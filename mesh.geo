// Const
supX = 0.2;
supY = 0.2;
airX = 5 * supX;
airY = 5 * supY;

supH =  0.2;
airH = supH;

// Supra
Point(1) = { -supX, -supY, 0.0, supH};
Point(2) = {  supX, -supY, 0.0, supH};
Point(3) = {  supX,  supY, 0.0, supH};
Point(4) = { -supX,  supY, 0.0, supH};

Line(1) = { 1, 2};
Line(2) = { 2, 3};
Line(3) = { 3, 4};
Line(4) = { 4, 1};

Line Loop(1) = { 1, 2, 3, 4};
Plane Surface(1) = {1};

// Air
Point(11) = { -airX, -airY, 0.0, airH};
Point(12) = {  airX, -airY, 0.0, airH};
Point(13) = {  airX,  airY, 0.0, airH};
Point(14) = { -airX,  airY, 0.0, airH};

Line(11) = { 11, 12};
Line(12) = { 12, 13};
Line(13) = { 13, 14};
Line(14) = { 14, 11};

Line Loop(2) = { 11, 12, 13, 14};
Plane Surface(2) = { 2, 1};

// Name mapping
Physical Line("1", 1001) = { 11};
Physical Line("2", 1002) = { 12};
Physical Line("3", 1003) = { 13};
Physical Line("4", 1004) = { 14};

Physical Surface("0", 100001) = { 1};
Physical Surface("1", 100002) = { 2};
