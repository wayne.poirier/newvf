import os
import sys

from time import time, sleep

import petsc4py
petsc4py.init(sys.argv)

from numba import njit
from mpi4py import MPI

COMM = MPI.COMM_WORLD
SIZE = COMM.Get_size()
RANK = COMM.Get_rank()

import gmshMesh as msh
import numpy as np
import geometry as geo
import matBuild as mb
import utils as ut
import petscUtils as lag

if RANK == 0:
    if os.path.exists('results'):
        for file in os.listdir('results'):
            os.remove('results/' + file)
        os.removedirs('results')
        os.mkdir('results')
    else:
        os.mkdir('results')
    files = ['nrmH', 'nrmHSup', 'nrmHDiam', 'nrmHSplit', 'nrmHSmart',
             'pointDiam', 'pointSplit', 'pointSmart', 'nrmHSupra', 'nrmJSupra']
    for f in files:
        if os.path.isfile(f):
            os.remove(f)

debugMat = False
viewPart = False
verbose  =  True
saveMesh = False
saveNrm  = False
################################################################################
#                                           Mesh extended                      #
################################################################################
meshFile = 'supra.msh'
meshFile = 'mesh.msh'
meshFile = 'circle.msh'
meshFile = 'order.msh'
meshFile = '2layer.msh'
meshFile = 'fil.msh'

# Read general mesh data
msh.gmshToHDF5(SIZE, RANK, COMM, meshFile)
cells, nodes, cellsGlobId, nodesGlobId,\
            nbCellsLoc, nbNodesLoc, edgesDom,\
            physCells, physEdgesDom = msh.readPartMesh('hdf5Mesh', RANK)
if verbose:
    print("Cells local (%d) sise %d and total size %d."
              %(RANK, nbCellsLoc, len(cells)))
    sys.stdout.flush()

# Build basic mesh info
geo.setLocalNodeId(cells, nodesGlobId, edgesDom)
nodesCellsId = geo.cellsAroundNodes(cells, len(nodes))
faces, nbInnerFaces, nbSharedFaces = geo.facesInfo(cells, nodes,
            edgesDom, physEdgesDom, nbCellsLoc, cellsGlobId)

# Build mesh info num methods
cellsCenter = geo.getCellsCenter(cells, nodes)
cellsVol = geo.getCellsVol(cells, nodes)
nodesFTags = geo.getNodesFacesTags(faces, nbInnerFaces + nbSharedFaces,
            len(nodes))
cellsFTags = geo.getCellsFacesTags(nodesFTags, nodesCellsId, len(cells))
facesNormal = geo.getFacesNormal2D(faces, nodes, cellsCenter)
leastSquareCoeff = geo.lastSquaresWeight(nodes, nodesCellsId, nbNodesLoc,
            cellsCenter, nbCellsLoc, faces)

# Diam info and splitted diam info
diamVol, diamNormal = geo.getInnerDiamond(faces, nodes, cellsCenter)
splitDVol, splitDNormal = geo.getSplitDiamond(faces, nodes,
            cellsCenter, nbInnerFaces+nbSharedFaces)

# Cells, Nodes and Faces numbering
allCellsSize, cellsCalcId = geo.globToLocNumbering(SIZE, RANK, COMM,
            cellsGlobId, nbCellsLoc)
allNodesSize, nodesCalcId = geo.globToLocNumbering(SIZE, RANK, COMM,
            nodesGlobId, nbNodesLoc)
nbCellsGlob = allCellsSize[SIZE]

# Compute the actual mapping (dim = 3 hard coded)
cellsMap = np.zeros((len(cells), 3), np.int32)
cellsMap[:,0] = 3*cellsCalcId[:]
cellsMap[:,1] = 3*cellsCalcId[:] +1
cellsMap[:,2] = 3*cellsCalcId[:] +2
cellsMap = np.reshape(cellsMap, 3*len(cells))

nodesMap = np.zeros((len(nodes), 3), np.int32)
nodesMap[:,0] = 3*nodesCalcId[:]
nodesMap[:,1] = 3*nodesCalcId[:] +1
nodesMap[:,2] = 3*nodesCalcId[:] +2
nodesMap = np.reshape(nodesMap, 3*len(nodes))

# facesMap = np.zeros((nbInnerFaces + nbSharedFaces, 3), np.int32)
# facesMap[:,0] = 3*facesCalcId[:]
# facesMap[:,1] = 3*facesCalcId[:] +1
# facesMap[:,2] = 3*facesCalcId[:] +2
# facesMap = np.reshape(facesMap, 3*(nbInnerFaces + nbSharedFaces))

### Get min h value
if verbose:
    hmi, hma = geo.minMaxFacesLength(COMM, facesNormal)
    if RANK == 0:
        print("-----------------------------------------")
        print("Min h value: %2.8f" %(hmi))
        print("Max h value: %2.8f" %(hma))

# Debug
if debugMat and False:
    def buildMapDebugMatrix(dim):
        matP  = lag.createMat((nbCellsLoc, nbCellsLoc), (dim, dim), dim)
        matPp = lag.createMat((nbCellsLoc, nbCellsLoc), (dim, dim), dim)

        row = np.zeros(dim * nbCellsLoc, np.int32)
        col = np.zeros(dim * nbCellsLoc, np.int32)
        val = np.zeros(dim * nbCellsLoc, np.float64)

        if dim == 1:
            for i in range(nbCellsLoc):
                matP.setValue( cellsGlobId[i], cellsCalcId[i], 1.)
                matPp.setValue(cellsCalcId[i], cellsGlobId[i], 1.)

            matP.assemblyBegin()
            matPp.assemblyBegin()
            matP.assemblyEnd()
            matPp.assemblyEnd()
        elif dim ==3:
            for i in range(nbCellsLoc):
                matP.setValue(3*cellsGlobId[i],    cellsMap[3*i],    1.)
                matP.setValue(3*cellsGlobId[i] +1, cellsMap[3*i +1], 1.)
                matP.setValue(3*cellsGlobId[i] +2, cellsMap[3*i +2], 1.)

                matPp.setValue(cellsMap[3*i],    3*cellsGlobId[i],    1.)
                matPp.setValue(cellsMap[3*i +1], 3*cellsGlobId[i] +1, 1.)
                matPp.setValue(cellsMap[3*i +2], 3*cellsGlobId[i] +2, 1.)

            matP.assemblyBegin()
            matPp.assemblyBegin()
            matP.assemblyEnd()
            matPp.assemblyEnd()
        else:
            raise ValueError('Invalid dim value')

        return matP, matPp
    matP, matPp = buildMapDebugMatrix(3)
    def buildMapDebugMatrixFaces():
        matP  = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)
        matPp = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

        row = np.zeros(3 * nbCellsLoc, np.int32)
        col = np.zeros(3 * nbCellsLoc, np.int32)
        val = np.zeros(3 * nbCellsLoc, np.float64)

        for i in range(nbInnerFaces):
            matP.setValue(3*facesGlobId[i],    facesMap[3*i],    1.)
            matP.setValue(3*facesGlobId[i] +1, facesMap[3*i +1], 1.)
            matP.setValue(3*facesGlobId[i] +2, facesMap[3*i +2], 1.)

            matPp.setValue(facesMap[3*i],    3*facesGlobId[i],    1.)
            matPp.setValue(facesMap[3*i +1], 3*facesGlobId[i] +1, 1.)
            matPp.setValue(facesMap[3*i +2], 3*facesGlobId[i] +2, 1.)

        for i in range(nbInnerFaces, nbInnerFaces + nbSharedFaces):
            if faces[i][5] == 1:
                matP.setValue(3*facesGlobId[i],    facesMap[3*i],    1.)
                matP.setValue(3*facesGlobId[i] +1, facesMap[3*i +1], 1.)
                matP.setValue(3*facesGlobId[i] +2, facesMap[3*i +2], 1.)

                matPp.setValue(facesMap[3*i],    3*facesGlobId[i],    1.)
                matPp.setValue(facesMap[3*i +1], 3*facesGlobId[i] +1, 1.)
                matPp.setValue(facesMap[3*i +2], 3*facesGlobId[i] +2, 1.)

        matP.assemblyBegin()
        matPp.assemblyBegin()
        matP.assemblyEnd()
        matPp.assemblyEnd()

        return matP, matPp
    matF, matFp = buildMapDebugMatrixFaces()
    if False:
        lag.fileViewPetscObj( matP,  'matP.out')
        lag.fileViewPetscObj(matPp, 'matPp.out')
        lag.fileViewPetscObj( matF,  'matF.out')
        lag.fileViewPetscObj(matFp, 'matFp.out')

################################################################################
#                                           Partitions                         #
################################################################################
if viewPart:
    debug = np.zeros(nbCellsLoc, np.float64)
    for i in range(nbCellsLoc):
        debug[i] = RANK
    msh.saveMeshEData('results/part', debug, cellsGlobId, 1, 0, RANK)

################################################################################
#                                           Helper                             #
################################################################################

def _funZ(p):
    return np.zeros(3)

# Static test functions
def _fun0(p):
    s = np.zeros(3)
    s[2] = np.exp(p[1])
    return s
def _rhs0(p):
    s = np.zeros(3)
    return s
def _rot0(p):
    s = np.zeros(3)
    y = p[1]
    s[0] = np.exp(y)
    return s
def _curlCurl0(p):
    s = np.zeros(3)
    s[2] = - np.exp(p[1])
    return s

def _fun1(p):
    s = np.zeros(3)
    x = p[0]
    s[2] = x * x
    return s
def _rhs1(p):
    s = np.zeros(3)
    x = p[0]
    s[2] = x * x - 2
    return s
def _rot1(p):
    s = np.zeros(3)
    x = p[0]
    s[1] = -2*x
    return s

def _fun2(p):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    s[2] = y*y * np.exp(x)
    return s
def _rhs2(p):
    s = np.zeros(3)
    x = p[0]
    s[2] = -2 * np.exp(x)
    return s
def _rot2(p):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    s[0] = 2 * y * np.exp(x)
    s[1] = - y*y * np.exp(x)
    return s

def _funBD1(p):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    r = x*x + y*y
    if r >= 0.25:
        s[2] = np.exp(r + 0.75)
    else:
        s[2] = np.exp(4 * r)
    return s
def _rhsBD1(p):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    r = x*x + y*y
    if r >= 0.25:
        s[2] = -r * np.exp(r + 0.75)
    else:
        s[2] = -4. * r * np.exp(4 * r)
    return s
def _funBD2(p):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    r = x*x + y*y
    if r >= 0.25:
        s[0] = np.exp(r + 0.75)
    else:
        s[0] = np.exp(4 * r)
    return s
def _rhsBD2(p):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    r = x*x + y*y
    if r >= 0.25:
        s[0] = -2 * y*y * np.exp(r + 0.75)
        s[1] =  2 * x*y * np.exp(r + 0.75)
    else:
        s[0] = -8. * y*y * np.exp(4 * r)
        s[1] =  8. * x*y * np.exp(4 * r)
    return s

def _fun3(p):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    s[1] = x
    s[2] = y
    return s
def _rot3(p):
    s = np.zeros(3)
    s[0] = 1.
    s[2] = 1.
    return s

def _fun4(p):
    s = np.zeros(3)
    x = p[0]
    s[2] = x*x
    return s
def _curl4(p):
    s = np.zeros(3)
    x = p[0]
    s[1] = -2*x
    return s
def _curlCurl4(p):
    s = np.zeros(3)
    s[2] = -2.
    return s

# Time test functions
def _tFun0(p, t):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    s[0] = t * np.exp(y)
    s[1] = np.cos(x*y)
    s[2] = x*x*x + t*y
    return s
def _tRhs0(p, t):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    cxy = np.cos(x*y)
    s[0] = (1-t) * np.exp(y) - x*y*cxy
    s[1] = y*y*cxy
    s[2] = y - 6*x
    return s

def _tFun1(p, t):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    s[2] = np.exp(y) * np.sin(t*x)
    return s
def _tRhs1(p, t):
    s = np.zeros(3)
    x = p[0]
    y = p[1]
    s[2] = np.exp(y) * (x * np.cos(t*x) + (t*t -1) * np.sin(t*x))
    return s

def _tFun2(p, t):
    s = np.zeros(3)
    r = p[0] * np.pi / 2
    # s[2] = np.cos(r) * np.sin(t)
    s[1] = np.sin(t/3)
    # s[2] = min(t/2, 1.)
    return s
def _tRhs2(p, t):
    s = np.zeros(3)
    return s

################################################################################
#                                           Generics on faces                  #
################################################################################
def ExtractCells(cellsTags, sz, tagsList):
    lst = []
    for i in range(sz):
        if cellsTags[i] in tagsList:
            lst.append(i)
    return np.array(lst)
def ExtractCellsChain(cellsLst, cellsTags, tagsList):
    lst = []
    for i in cellsLst:
        if cellsTags[i] in tagsList:
            lst.append(i)
    return np.array(lst)
def ExtractFaces(faces, nbOwd, nbShd, cellsTags, tagsList):
    sol = []
    lst = []
    for i in range(nbOwd):
        idL = faces[i][2]
        idR = faces[i][3]
        if idR == -1:
            continue
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    lst = []
    for i in range(nbOwd, nbOwd+nbShd):
        idL = faces[i][2]
        idR = faces[i][3]
        if idR == -1:
            continue
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    return sol
def ExtractFacesChain(facesLst, cellsTags, tagsList):
    sol = []
    lst = []
    for i in facesLst[0]:
        idL = faces[i][2]
        idR = faces[i][3]
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    lst = []
    for i in facesLst[1]:
        idL = faces[i][2]
        idR = faces[i][3]
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    return sol
def ExtractFacesBd(faces, nbOwd, nbShd):
    sol = []
    lst = []
    for i in range(nbOwd):
        if faces[i][3] == -1:
            lst.append(i)
    sol.append(np.array(lst))
    lst = []
    for i in range(nbOwd, nbOwd+nbShd):
        if faces[i][3] == -1:
            lst.append(i)
    sol.append(np.array(lst))
    return sol

# Extract faces examples
if False:
    # Faces group
    FBd      = ExtractFacesBd(faces, nbInnerFaces, nbSharedFaces)
    FBdPts   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, 1001), (1001, 1002), (1001, 1003), (1001, 1004),
                             (1002, 1001), (1002, 1002), (1002, 1003), (1002, 1004),
                             (1003, 1001), (1003, 1002), (1003, 1003), (1003, 1004),
                             (1004, 1001), (1004, 1002), (1004, 1003), (1004, 1004)])

    FInIn    = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags, [(-1, -1)])

    FInBdL   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, -1), (1002, -1), (1003, -1), (1004, -1)])
    FInBdR   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(-1, 1001), (-1, 1002), (-1, 1003), (-1, 1004)])

    FInnerCircle  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100001, 100001)])
    _FOuterCircle = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100002, 100002)])
    FOuterCircle  = ExtractFacesChain(_FOuterCircle, cellsFTags, [(-1, -1)])

    FInnerOuter = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100001, 100002)])
    FOuterInner = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100002, 100001)])

    # Vecs
    zero  = lag.createVec(nbCellsLoc, 1, 1)
    debug = lag.createVec(nbCellsLoc, 1, 1)

    ###     Part
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for i in range(nbCellsLoc):
        arrD[i] = RANK
    msh.saveMeshEData('results/part', arrD, cellsGlobId, 1, 0, RANK)


    ###     Border
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FBd[0]:
        A = faces[f][2]
        arrD[A] = 10 * RANK + 1.
    msh.saveMeshEData('results/Bd', arrD, cellsGlobId, 1, 0, RANK)

    ###     Pts on the border
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for lst in [FBdPts[0]]:
        for f in lst:
            A = faces[f][2]
            B = faces[f][3]
            arrD[A] = 10 * RANK + 1.
            arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/PtsBd', arrD, cellsGlobId, 1, 0, RANK)

    ###     InIn
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInIn[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/InIn', arrD, cellsGlobId, 1, 0, RANK)

    ###     In <-> Bd
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInBdR[0]:
        A = faces[f][2]
        arrD[A] = 10 * RANK + 1.
    for f in FInBdL[0]:
        B = faces[f][3]
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/InBd', arrD, cellsGlobId, 1, 0, RANK)

    ###    InnerCircle
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInnerCircle[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/InnerCircle', arrD, cellsGlobId, 1, 0, RANK)

    ###    OuterCircle
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FOuterCircle[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/OuterCircle', arrD, cellsGlobId, 1, 0, RANK)

    ###     Inner <-> Outer
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInnerOuter[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 2.
        arrD[B] = 10 * RANK + 1.
    for f in FOuterInner[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 2.
    msh.saveMeshEData('results/InOutOutIn', arrD, cellsGlobId, 1, 0, RANK)

################################################################################
#                                           Supra linearization                #
################################################################################
@njit
def supLin(row, col, coeff, rowId, colId, size,
              nA, nB, vC, vD, alpha,
              idL, idR, arrayGhost):
    v = 2 * vD
    # if idL > nbSup:
    #     assert(idR <= nbSup)
    #     idL = idR
    # if idR > nbSup:
    #     assert(idL <= nbSup)
    #     idR = idL

    curlA = np.array([[    0., -nA[2],  nA[1]],
                      [ nA[2],     0., -nA[0]],
                      [-nA[1],  nA[0],     0.]],
                     np.float64)
    curlB = np.array([[    0., -nB[2],  nB[1]],
                      [ nB[2],     0., -nB[0]],
                      [-nB[1],  nB[0],     0.]],
                     np.float64)

    JL0 = arrayGhost[3*idL]
    JL1 = arrayGhost[3*idL +1]
    JL2 = arrayGhost[3*idL +2]
    JR0 = arrayGhost[3*idR]
    JR1 = arrayGhost[3*idR +1]
    JR2 = arrayGhost[3*idR +2]

    JA0 = (JL0 + JR0) / (2. * Jc)
    JA1 = (JL1 + JR1) / (2. * Jc)
    JA2 = (JL2 + JR2) / (2. * Jc)

    nrmJ = JA0*JA0 + JA1*JA1 + JA2*JA2
    rho = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (1e-3 * Ec / (mu0 * Jc))
    tmp = (n-1) * (Ec / (Jc * mu0)) * np.power(nrmJ, (n-3)/2)
    jac = np.array([[rho + tmp*JA0*JA0, tmp*JA0*JA1, tmp*JA0*JA2],
                    [tmp*JA1*JA0, rho + tmp*JA1*JA1, tmp*JA1*JA2],
                    [tmp*JA2*JA0, tmp*JA2*JA1, rho + tmp*JA2*JA2]],
                   np.float64)

    op = curlA @ jac @ curlB

    for i in range(3):
        for j in range(3):
            row[size] = 3*rowId +i
            col[size] = 3*colId +j
            coeff[size] =  alpha * op[i,j] / v
            size += 1

    return 9
@njit
def supLinEps(row, col, coeff, rowId, colId, size,
              nA, nB, vC, vD, alpha,
              idL, idR, arrayGhost):
    v = 2 * vD
    eps    = 1e-6
    eps    = 0.
    # if idL > nbSup:
    #     assert(idR <= nbSup)
    #     idL = idR
    # if idR > nbSup:
    #     assert(idL <= nbSup)
    #     idR = idL

    curlA = np.array([[    0., -nA[2],  nA[1]],
                      [ nA[2],     0., -nA[0]],
                      [-nA[1],  nA[0],     0.]],
                     np.float64)
    curlB = np.array([[    0., -nB[2],  nB[1]],
                      [ nB[2],     0., -nB[0]],
                      [-nB[1],  nB[0],     0.]],
                     np.float64)

    JL0 = arrayGhost[3*idL]
    JL1 = arrayGhost[3*idL +1]
    JL2 = arrayGhost[3*idL +2]
    JR0 = arrayGhost[3*idR]
    JR1 = arrayGhost[3*idR +1]
    JR2 = arrayGhost[3*idR +2]

    JA0 = (JL0 + JR0) / (2. * Jc)
    JA1 = (JL1 + JR1) / (2. * Jc)
    JA2 = (JL2 + JR2) / (2. * Jc)

    nrmJ = JA0*JA0 + JA1*JA1 + JA2*JA2
    rho = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (1e-3 * Ec / (mu0 * Jc))
    tmp = (n-1) * (Ec / (Jc * mu0)) * np.power(nrmJ, (n-3)/2)
    jac = np.array([[rho + tmp*JA0*JA0, tmp*JA0*JA1, tmp*JA0*JA2],
                    [tmp*JA1*JA0, rho + tmp*JA1*JA1, tmp*JA1*JA2],
                    [tmp*JA2*JA0, tmp*JA2*JA1, rho + tmp*JA2*JA2]],
                   np.float64)

    op = curlA @ jac @ curlB

    for i in range(3):
        for j in range(3):
            row[size] = 3*rowId +i
            col[size] = 3*colId +j
            if i == j:
                coeff[size] =  alpha * (op[i,j] + eps) / v
            else:
                coeff[size] =  alpha * op[i,j] / v
            size += 1

    return 9

@njit
def supLin2(row, col, coeff, rowId, colId, size,
              nA, nB, vC, vD, alpha,
              idL, idR, arrayGhost):
    v = 2 * vD
    # if idL > nbSup:
    #     assert(idR <= nbSup)
    #     idL = idR
    # if idR > nbSup:
    #     assert(idL <= nbSup)
    #     idR = idL

    curlA = np.array([[    0., -nA[2],  nA[1]],
                      [ nA[2],     0., -nA[0]],
                      [-nA[1],  nA[0],     0.]],
                     np.float64)
    curlB = np.array([[    0., -nB[2],  nB[1]],
                      [ nB[2],     0., -nB[0]],
                      [-nB[1],  nB[0],     0.]],
                     np.float64)

    JL0 = arrayGhost[3*idL]
    JL1 = arrayGhost[3*idL +1]
    JL2 = arrayGhost[3*idL +2]
    JR0 = arrayGhost[3*idR]
    JR1 = arrayGhost[3*idR +1]
    JR2 = arrayGhost[3*idR +2]

    JA0 = (JL0 + JR0) / (2. * Jc)
    JA1 = (JL1 + JR1) / (2. * Jc)
    JA2 = (JL2 + JR2) / (2. * Jc)

    nrmJ = JA0*JA0 + JA1*JA1 + JA2*JA2
    tmp = (n-1) * (Ec / (Jc * mu0)) * np.power(nrmJ, (n-3)/2)
    jac = np.array([[tmp*JA0*JA0, tmp*JA0*JA1, tmp*JA0*JA2],
                    [tmp*JA1*JA0, tmp*JA1*JA1, tmp*JA1*JA2],
                    [tmp*JA2*JA0, tmp*JA2*JA1, tmp*JA2*JA2]],
                   np.float64)

    op = curlA @ jac @ curlB

    for i in range(3):
        for j in range(3):
            row[size] = 3*rowId +i
            col[size] = 3*colId +j
            coeff[size] =  alpha * op[i,j] / v
            size += 1

    return 9

################################################################################
#                                           Faces counting                     #
################################################################################
if True:
    START = time()

    ### Tags groups
    tAll = [-1, 1001, 1002, 1003, 1004]
    tIn = [-1]
    tBorder = [1001, 1002, 1003, 1004]
    cellsAll = [100001, 100002]
    cellsAir = [100002]
    cellsSup = [100001]

    ### Extract all needed faces
    CIn      = ExtractCells(cellsFTags, nbCellsLoc,      tIn)
    CBd      = ExtractCells(cellsFTags, nbCellsLoc,  tBorder)
    FSupAirL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsSup, cellsAir)])
    FSupAirR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsSup)])
    FSupSup  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsSup, cellsSup)])
    FSup = FSupSup
    _FIn     = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsAir)])
    FIn      = ExtractFacesChain(_FIn, cellsFTags, [(-1, -1)])
    _FInIn   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                            [(cellsAir, cellsAir), (cellsAir, cellsSup),
                             (cellsSup, cellsAir), (cellsSup, cellsSup)])
    FInIn    = ExtractFacesChain(_FInIn, cellsFTags, [(-1, -1)])
    FInBdL   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(-1, 1001), (-1, 1002), (-1, 1003), (-1, 1004)])
    FInBdR   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, -1), (1002, -1), (1003, -1), (1004, -1)])
    END = time()
    elapsedTime = COMM.allreduce(END - START, op=MPI.MAX)

    if False:
        sleep(RANK)
        print('----- %3.1d -----' %(RANK))
        print('Time to extract faces: %f' %(elapsedTime))
        print('Nb cells In: %d' %(len(CIn)))
        print('Nb cells Border: %d' %(len(CBd)))
        print('Nb faces In: %d' %(len(FInIn[0]) + len(FInIn[1])))
        print('Nb faces sup-air: %d' %(len(FSupAirL[0]) + len(FSupAirL[1]) + len(FSupAirR[0]) + len(FSupAirR[1])))
        print('Nb faces sup-sup: %d' %(len(FSupSup[0]) + len(FSupSup[1])))
        print('Nb faces air-air: %d' %(len(FIn[0]) + len(FIn[1])))
        print('Nb faces in-bd: %d' %(len(FInBdL[0]) + len(FInBdL[1]) + len(FInBdR[0]) + len(FInBdR[1])))
        sys.stdout.flush()

    # cellsId3: 3
    # edgesCurl: 6
    # curlCurl: 5
    # curlRhoCurl: 5
    # curlRhoCurlEps: 5

    # supLin: 9
    # supLinEps: 9
    # supLin2: 9

    # ExCells: 1
    # ExCellsEdges: 4 * nbMaxNeig
    # ExCellsEdgesLR: 2 * nbMaxNeig
    # ExDiamond: 4 + 4 * nbMaxNeig
    # ExDiamondLR: 2 + 2 * nbMaxNeig
    # ExSplitLR: 1 + 2 * nbMaxNeig
    szBuff = 10000000
    nbMaxNeig = 7
    if False: # Order
        szBuff = max([((4 + 4*nbMaxNeig) * 5) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Di-Domaine
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Lin diamond
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FIn[0]) + len(FIn[1])),
                      ((4 + 4 * nbMaxNeig) * 9) * (len(FSup[0]) + len(FSup[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),
                      ((2 + 2 * nbMaxNeig) * 9) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Smart Bi-Domaine
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FIn[0]) + len(FIn[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FSupSup[0]) + len(FSupSup[1])),
                      ((1 + 2 * nbMaxNeig) * 5) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Smart interfaces: Lin
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 * nbMaxNeig) * 6) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),

                      ((4 + 4 * nbMaxNeig) * 5) * (len(FIn[0]) + len(FIn[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      ((1 + 2 * nbMaxNeig) * 5) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),

                      ((4 + 4 * nbMaxNeig) * 9) * (len(FSup[0]) + len(FSup[1])),
                      ((1 + 2 * nbMaxNeig) * 9) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),

                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)

################################################################################
#                                           Faces                              #
################################################################################
if True:
    def getMPINeig(faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLst = -np.ones(mpiSize, np.int32)
        for i in range(nbInnerFaces, nbInnerFaces+nbSharedFaces):
            v = cellsMaps[faces[i][3]]
            j = 0
            while mpiCellsNb[j] < v:
                j += 1
            neigLst[j-1] = j-1
        return neigLst
    def getFacesNb(listOfFacesList, faces):
        nbOwnd       = 0
        nbToSyncOwnd = 0
        nbToSyncBrwd = 0
        for lstF in listOfFacesList:
            nbOwnd += len(lstF[0])
            for f in lstF[1]:
                if faces[f][5] == 1:
                    nbToSyncOwnd += 1
                else:
                    nbToSyncBrwd += 1
        return nbOwnd, nbToSyncOwnd, nbToSyncBrwd
    def getFaceNeig(listOfFacesList, faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLstOwnd = [[] for i in range(mpiSize)]
        neigLstBrwd = [[] for i in range(mpiSize)]
        for lstF in listOfFacesList:
            for f in lstF[1]:
                if f < nbInnerFaces:
                    continue

                v = cellsMaps[faces[f][3]]
                j = 0
                while mpiCellsNb[j] <= v:
                    j += 1
                if faces[f][5] == 1:
                    neigLstOwnd[j-1].append(f)
                else:
                    neigLstBrwd[j-1].append(f)

        return neigLstOwnd, neigLstBrwd

    neigLst = getMPINeig(faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)
    neigLstOwnd, neigLstBrwd = getFaceNeig([FSupAirL, FSupAirR], faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)

    nbFOwnd, nbFToSyncOwnd, nbFToSyncBrwd = getFacesNb([FSupAirL, FSupAirR], faces)

    nbFacesArray = np.zeros(SIZE+1, np.int32)
    nbFacesArray[RANK+1] = nbFOwnd + nbFToSyncOwnd
    nbFacesArray = COMM.allreduce(nbFacesArray, op=MPI.SUM)
    nbFacesArray = nbFacesArray.cumsum().astype(np.int32)

    facesCalcId = 1000000000 * np.ones(nbInnerFaces + nbSharedFaces, np.int32)
    def innerFacesCalcId(listOfFacesList, facesCalcId, nbFOwnd, nbFacesArray, rank):
        vId = 0

        for lstF in listOfFacesList:
            for f in lstF[0]:
                facesCalcId[f] = nbFacesArray[rank] + vId
                vId += 1

        assert(vId == nbFOwnd)
    def owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                        nbFacesArray, rank, mpiSize):
        vId = 0
        remapFOwnd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstOwnd[i]:
                facesCalcId[f] = nbFacesArray[rank] + nbFOwnd + vId
                vId += 1
                tmp = (facesCalcId[f], cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                remapFOwnd[i].append(tmp)

        assert(vId == nbFToSyncOwnd)
        return remapFOwnd
    def brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, mpiSize):
        oldFBrwd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstBrwd[i]:
                tmp = (f, cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                oldFBrwd[i].append(tmp)

        return oldFBrwd

    innerFacesCalcId([FSupAirL, FSupAirR], facesCalcId, nbFOwnd, nbFacesArray, RANK)
    remapFOwnd = owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                                 nbFacesArray, RANK, SIZE)
    oldFBrwd = brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, SIZE)

    def syncBrwWithRecieve(faces, facesCalcId, oldFBrwd, recievedF, mpiSize):
        tupleMap = []
        assert(len(oldFBrwd) == len(recievedF))
        for descF in oldFBrwd:
            j = 0
            while descF[1] != recievedF[j][2] and descF[2] != recievedF[j][1]:
                j += 1
            facesCalcId[descF[0]] = recievedF[j][0]

    for i in range(SIZE):
        if len(remapFOwnd[i]) != 0 or len(oldFBrwd[i]) != 0:
            if i < RANK:
                if len(remapFOwnd[i]) != 0:
                    COMM.send(remapFOwnd[i], dest=i)
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
            else:
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
                if len(remapFOwnd[i]) != 0:
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                    COMM.send(remapFOwnd[i], dest=i)

    facesMap = np.zeros((nbInnerFaces + nbSharedFaces, 3), np.int32)
    facesMap[:,0] = 3*facesCalcId[:]
    facesMap[:,1] = 3*facesCalcId[:] +1
    facesMap[:,2] = 3*facesCalcId[:] +2
    facesMap = np.reshape(facesMap, 3*(nbInnerFaces + nbSharedFaces))

    nbFacesLoc = nbFOwnd + nbFToSyncOwnd
    if verbose and RANK == 0:
        print('nbFacesLoc:', nbFacesLoc)

################################################################################
#                                           DEBUG                              #
################################################################################
if False:
    coeff = np.zeros(nbCellsLoc, np.float64)

    H = lag.createVec(nbCellsLoc, 1, 1)
    for f in FSupAirL[0]:
        idL = faces[f][2]
        idR = faces[f][3]

        coeff[idL] = 1.
        coeff[idR] = 0.5

    msh.saveMeshEData('results/SupAirL', coeff, cellsGlobId, 1, 0, RANK)

################################################################################
#                                           Testing                            #
################################################################################
sys.stdout.flush()

# Order: Static Bi-Domaine
if False:
    buildFun = mb.curlRhoCurlStrip
    buildFun = mb.curlRhoCurl

    # Space for coeff
    szBuff = 100000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    # Vec
    H     = lag.createVec(nbCellsLoc, 3, 3)
    rhsIn = lag.createVec(nbCellsLoc, 3, 3)
    rhsBd = lag.createVec(nbCellsLoc, 3, 3)
    solH  = lag.createVec(nbCellsLoc, 3, 3)
    zero  = lag.createVec(nbCellsLoc, 3, 3)

    # Ghost Vec
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    # Init Rho
    def computeRho(rho, nbCellsLoc, physCells, tags1, r1, r2):
        sz = rho.getSizes()[0]
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            if physCells[i] == tags1:
                arrRho[3*i] = r1
                arrRho[3*i+1] = r1
                arrRho[3*i+2] = r1
            else:
                arrRho[3*i] = r2
                arrRho[3*i+1] = r2
                arrRho[3*i+2] = r2
        rho.setArray(arrRho)

    if False:
        solFun = _fun0
        rhsFun = _rhs0
        computeRho(rho, nbCellsLoc, physCells, 100001, 1., 1.)
    elif False:
        solFun = _funBD1
        rhsFun = _rhsBD1
        computeRho(rho, nbCellsLoc, physCells, 100001, 1./16., 1./4.)
    else:
        solFun = _funBD2
        rhsFun = _rhsBD2
        computeRho(rho, nbCellsLoc, physCells, 100001, 1./8., 1./2.)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    # Mat
    matOP = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    # Build the matrix
    size = mb.ExDiamond(row, col, coeff, FInIn, faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, arrayRho)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, arrayRho)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsId3)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    ### All init
    sz = ut.cptVecTagsVal(coeff, row, solFun, cellsFTags, tBorder, cellsCenter,
                          cellsVol, nbCellsLoc, 3)
    lag.setValueVec(rhsBd, coeff, row, cellsMap, sz)
    sz = ut.cptVecTagsVal(coeff, row, rhsFun, cellsFTags, tIn, cellsCenter,
                          cellsVol, nbCellsLoc, 3)
    lag.setValueVec(rhsIn, coeff, row, cellsMap, sz)
    sz = ut.cptVecTagsVal(coeff, row, solFun, cellsFTags, tAll, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(solH, coeff, row, cellsMap, sz)

    ### Linear solver
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, 100)
    lag.solveLin(ksp, matOP, H, rhsBd + rhsIn)

    volLoc = cellsVol[:nbCellsLoc]
    nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zero), 3, 2, volLoc)
    errH = ut.diff(COMM, lag.getArray(H), lag.getArray(solH), 3, 2, volLoc)
    if verbose and RANK == 0:
        print('Nrm: %2.7f, Err: %2.7f' %(nrmH, errH))

    msh.saveMeshEData('results/H', lag.getArray(H),
                      cellsGlobId[:nbCellsLoc], 3, 0, RANK)
    msh.saveMeshEData('results/solH', lag.getArray(solH),
                      cellsGlobId[:nbCellsLoc], 3, 0, RANK)
    msh.saveMeshEData('results/errH', lag.getArray(H) - lag.getArray(solH),
                      cellsGlobId[:nbCellsLoc], 3, 0, RANK)
# Order: Smart Static Bi-Domaine
if False:
    buildFunEq = mb.curlRhoCurlEps

    buildFun   = mb.curlRhoCurlStrip
    buildFun   = mb.curlRhoCurl
    buildFunEq = mb.curlRhoCurlEps

    # Space for coeff
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    # Vec
    H     = lag.createVec(nbCellsLoc, 3, 3)
    F     = lag.createVec(nbFacesLoc, 3, 3)
    rhsIn = lag.createVec(nbCellsLoc, 3, 3)
    rhsBd = lag.createVec(nbCellsLoc, 3, 3)
    rhsF  = lag.createVec(nbFacesLoc, 3, 3)
    solH  = lag.createVec(nbCellsLoc, 3, 3)
    zero  = lag.createVec(nbCellsLoc, 3, 3)

    # Ghost Vec
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    # Init Rho
    def computeRho(rho, nbCellsLoc, physCells, tags1, r1, r2):
        sz = rho.getSizes()[0]
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            if physCells[i] == tags1:
                arrRho[3*i] = r1
                arrRho[3*i+1] = r1
                arrRho[3*i+2] = r1
            else:
                arrRho[3*i] = r2
                arrRho[3*i+1] = r2
                arrRho[3*i+2] = r2
        rho.setArray(arrRho)

    if False:
        solFun = _fun0
        rhsFun = _rhs0
        computeRho(rho, nbCellsLoc, physCells, 100001, 1., 1.)
    elif False:
        solFun = _funBD1
        rhsFun = _rhsBD1
        computeRho(rho, nbCellsLoc, physCells, 100001, 1./16., 1./4.)
    else:
        solFun = _funBD2
        rhsFun = _rhsBD2
        computeRho(rho, nbCellsLoc, physCells, 100001, 1./8., 1./2.)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    # Mat
    matOP1       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matOP2       = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matOP3       = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matOP4       = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    # Build the matrix
    size = mb.ExDiamond(row, col, coeff, FIn, faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamond(row, col, coeff, FSupSup, faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # ID
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap, cellsMap,
                         1, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap, cellsMap,
                         1., size, addRule=True)

    # Mat 2
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
    # Mat 3
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
    # Mat 4
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFunEq, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFunEq, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)

    ### All init
    sz = ut.cptVecTagsVal(coeff, row, solFun, cellsFTags, tBorder, cellsCenter,
                          cellsVol, nbCellsLoc, 3)
    lag.setValueVec(rhsBd, coeff, row, cellsMap, sz)
    sz = ut.cptVecTagsVal(coeff, row, rhsFun, cellsFTags, tIn, cellsCenter,
                          cellsVol, nbCellsLoc, 3)
    lag.setValueVec(rhsIn, coeff, row, cellsMap, sz)
    sz = ut.cptVecTagsVal(coeff, row, solFun, cellsFTags, tAll, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(solH, coeff, row, cellsMap, sz)

    ### Generics objects
    matOP = lag.createNest(2, [[matOP1, matOP2], [matOP3, matOP4]])
    rhsG  = lag.createNest(1, [rhsBd + rhsIn, rhsF])
    cptG  = lag.createNest(1, [H, F])

    ### Linear solver
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, 100)
    lag.solveLin(ksp, matOP, cptG, rhsG)

    volLoc = cellsVol[:nbCellsLoc]
    nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zero), 3, 2, volLoc)
    errH = ut.diff(COMM, lag.getArray(H), lag.getArray(solH), 3, 2, volLoc)
    if verbose and RANK == 0:
        print('Nrm: %2.7f, Err: %2.7f' %(nrmH, errH))

    msh.saveMeshEData('results/H', lag.getArray(H),
                      cellsGlobId[:nbCellsLoc], 3, 0, RANK)
    msh.saveMeshEData('results/solH', lag.getArray(solH),
                      cellsGlobId[:nbCellsLoc], 3, 0, RANK)
    msh.saveMeshEData('results/errH', lag.getArray(H) - lag.getArray(solH),
                      cellsGlobId[:nbCellsLoc], 3, 0, RANK)

# Time Bi-Domaine
if False:
    buildFun = mb.curlRhoCurlStrip
    buildFun = mb.curlRhoCurl

    t = 0.
    dt = 1e-3
    it = 0
    tEnd = 150*dt

    def funH0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[2] = 1
        return s
    def funH1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[0] = y
        return s

    def computeRho(rho, nbCellsLoc, JLoc, physCells, tagsAir, rAir, rSup):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            if physCells[i] in tagsAir:
                arrRho[3*i] = rAir
                arrRho[3*i+1] = rAir
                arrRho[3*i+2] = rAir
            else:
                arrRho[3*i] = rSup
                arrRho[3*i+1] = rSup
                arrRho[3*i+2] = rSup
        rho.setArray(arrRho)
        return rho

    ### Verif
    fun = funH0
    rhoAir = 1
    rhoSup = 1
    rhoAir = 100. * 1e1
    rhoSup =  0.5 * 1e1

    if verbose and RANK == 0:
        print("-----------------------------------------")

    ### Space for coeff
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vecs
    # Vec
    H     = lag.createVec(nbCellsLoc, 3, 3)
    J     = lag.createVec(nbCellsLoc, 3, 3)
    diriH = lag.createVec(nbCellsLoc, 3, 3)
    rhsH  = lag.createVec(nbCellsLoc, 3, 3)
    # Ghost vec and init
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    computeRho(rho, nbCellsLoc, rho, physCells, cellsAir, rhoAir, rhoSup)
    lag.syncGhostVec(rho)

    ### Mat
    # Mat
    matOP        = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    # Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    matRot.chop(1e-10)

    ### matOP
    # Curl Rho Curl
    size = mb.ExDiamond(row, col, coeff, FInIn, faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, lag.getGhostArray(rho))
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, lag.getGhostArray(rho))
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(matOP, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # Setup
    matOP.chop(1e-10)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriH, H)
    matRot.mult(H, J)

    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)

    ### Loop
    while t+dt < tEnd:
        t += dt
        it += 1

        lag.copy(diriH, rhsH)
        rhsH.axpby(1/dt, 1, volNotBorder*H)
        lag.solveLin(ksp, matOP, H, rhsH)

        matRot.mult(H, J)
        J = matNotBorder*J

        nbIt = ksp.getIterationNumber()
        if nbIt == maxIt or nbIt == 0:
            print('Error while solve, nbIt: ', nbIt)
            exit()
        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)

    if debugMat:
        lag.fileViewPetscObj(matP *  matOP * matPp,  'matOP.out')
        lag.fileViewPetscObj(matP * matRot * matPp, 'matRot.out')
# Lin diamond
if False:
    buildFun   = mb.curlRhoCurlEps
    buildFun = mb.curlRhoCurl
    linFun   = supLin
    jacFun   = supLin2

    t = 0.
    it = 0
    dt = 1e-3
    tEnd = 50*dt

    rhoAir = 1e0
    mu0 = 4*np.pi * 1e-4
    Ec = 1e-7
    Jc = 1e1
    n = 10

    # Static test functions
    def _H0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]
        s[2] = 1e0

        return s / mu0
    def _H1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]
        s[0] = 1e-2

        return s / mu0
    def computeRho(rho, nbCellsLoc, JLoc, physCells, tagsAir):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            if physCells[i] in tagsAir:
                arrRho[3*i] = rhoAir / mu0
                arrRho[3*i+1] = rhoAir / mu0
                arrRho[3*i+2] = rhoAir / mu0
            else:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                J2 = arr[3*i +2] / Jc
                nrmJ = J0*J0 + J1*J1 + J2*J2
                vRho = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))

                arrRho[3*i] = vRho
                arrRho[3*i+1] = vRho
                arrRho[3*i+2] = vRho
        rho.setArray(arrRho)
        return rho

    fun = _H0

    if verbose:
        if RANK == 0:
            print("-----------------------------------------")

    # Space for coeff
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vecs
    cptC     = lag.createVec(nbCellsLoc, 3, 3)
    memo     = lag.createVec(nbCellsLoc, 3, 3)
    zero     = lag.createVec(nbCellsLoc, 3, 3)
    tmpC     = lag.createVec(nbCellsLoc, 3, 3)
    rhsC     = lag.createVec(nbCellsLoc, 3, 3)
    diriC    = lag.createVec(nbCellsLoc, 3, 3)
    # Ghost vec
    ghostId  = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    cptRot   = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    rho      = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    ### Mat
    # Mat
    matOP        = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    memoOP       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matJac       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    # matNotBorder
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)
    # ksp = lag.getLinearSolver('fgmres', 'fieldsplit', 1e-8, 1e-4, 200)
    # ksp = lag.getLinearSolver('bcgs', 'asm', 1e-8, 1e-4, 200)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriC, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriC, cptC)

    ### Init memo
    computeRho(rho, nbCellsLoc, cptRot, physCells, cellsAir)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    size = mb.ExDiamond(row, col, coeff, FIn, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(memoOP, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    ### Init saveNrm
    if saveNrm:
        volLoc = cellsVol[:nbCellsLoc] * (physCells[:nbCellsLoc] == 100001)
        if RANK == 0:
            if os.path.exists('nrmHSupra'):
                os.remove('nrmHSupra')
            if os.path.exists('nrmJSupra'):
                os.remove('nrmJSupra')

            fNrmH = open('nrmHSupra', 'x')
            fNrmJ = open('nrmJSupra', 'x')
    ### Loop
    while t+dt < tEnd:
        lag.copy(cptC, tmpC)

        ### Save mesh
        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(cptC),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(matNotBorder * cptRot),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
        if saveNrm:
            nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zero), 3, 2, volLoc)
            nrmJ = ut.diff(COMM, lag.getArray(cptRot), lag.getArray(zero), 3, 2, volLoc)
            if RANK == 0:
                fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
                fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
        t += dt
        it += 1

        while True:
            matJac.zeroEntries()
            matOP.zeroEntries()

            lag.copy(tmpC, memo)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpC),
                              lag.getArray(zero), 3, 2, np.ones(nbCellsLoc))

            # Update Rot
            matRot.mult(tmpC, cptRot)
            lag.syncGhostVec(cptRot)
            arrayRot = lag.getGhostArray(cptRot)

            computeRho(rho, nbCellsLoc, cptRot, physCells, cellsAir)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)

            ### Update matOP
            # Jac
            size = mb.ExDiamond(row, col, coeff, FSup, faces,
                             facesNormal, diamNormal, diamVol, cellsVol,
                             nodesCellsId, leastSquareCoeff,
                             jacFun, arrayRot)
            lag.setValueMat(matJac, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FSupAirL, FSupAirR], faces,
                             facesNormal, diamNormal, diamVol, cellsVol,
                             nodesCellsId, leastSquareCoeff,
                             jacFun, arrayRot)
            lag.setValueMat(matJac, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

            # Curl Rho Curl
            size = mb.ExDiamond(row, col, coeff, FSup, faces,
                             facesNormal, diamNormal, diamVol, cellsVol,
                             nodesCellsId, leastSquareCoeff,
                             linFun, arrayRot)
            lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FSupAirL, FSupAirR], faces,
                             facesNormal, diamNormal, diamVol, cellsVol,
                             nodesCellsId, leastSquareCoeff,
                             linFun, arrayRot)
            lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FSupAirR, FSupAirL], faces,
                             facesNormal, diamNormal, diamVol, cellsVol,
                             nodesCellsId, leastSquareCoeff,
                             buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

            # Add memoOP values
            matOP.axpy(1., memoOP)

            # # Update pb
            lag.copy(diriC, rhsC)
            rhsC.axpby(1/dt, 1, volNotBorder*cptC)
            rhsC = rhsC + matJac*tmpC
            lag.solveLin(ksp, matOP, tmpC, rhsC)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memo),
                              lag.getArray(tmpC), 3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()

            if nrmDiff/nrmPred < 1e-3:
                if RANK == 0 and verbose:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
            if nbIt == 0:
                break
        if nbIt == 0:
            break
        lag.copy(tmpC, cptC)
    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(cptC),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(matNotBorder * cptRot),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
    if saveNrm:
        nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zero), 3, 2, volLoc)
        nrmJ = ut.diff(COMM, lag.getArray(cptRot), lag.getArray(zero), 3, 2, volLoc)
        if RANK == 0:
            fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
            fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
            fNrmH.close()
            fNrmJ.close()

# Time Smart bi-domaine
if False:
    buildFun = mb.curlRhoCurlStrip
    buildFun = mb.curlRhoCurl

    t = 0.
    dt = 1e-3
    it = 0
    tEnd = 150*dt

    def funH0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[2] = 1
        return s
    def funH1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[0] = y
        return s
    def computeRho(rho, nbCellsLoc, JLoc, physCells, tagsAir, rAir, rSup):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            if physCells[i] in tagsAir:
                arrRho[3*i] = rAir
                arrRho[3*i+1] = rAir
                arrRho[3*i+2] = rAir
            else:
                arrRho[3*i] = rSup
                arrRho[3*i+1] = rSup
                arrRho[3*i+2] = rSup
        rho.setArray(arrRho)
        return rho

    ### Verif
    fun = funH0
    rhoAir = 1
    rhoSup = 1
    rhoAir = 100. * 1e1
    rhoSup =  0.5 * 1e1

    if verbose and RANK == 0:
        print("-----------------------------------------")

    # Space for coeff
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vecs
    # Vec
    H     = lag.createVec(nbCellsLoc, 3, 3)
    J     = lag.createVec(nbCellsLoc, 3, 3)
    F     = lag.createVec(nbFacesLoc, 3, 3)
    diriH = lag.createVec(nbCellsLoc, 3, 3)
    rhsH  = lag.createVec(nbCellsLoc, 3, 3)
    rhsF  = lag.createVec(nbFacesLoc, 3, 3)
    zeros = lag.createVec(nbCellsLoc, 3, 3)
    # Ghost vec and init
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    computeRho(rho, nbCellsLoc, rho, physCells, cellsAir, rhoAir, rhoSup)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    cId = np.array([i for i in range(3*nbCellsLoc)], np.int32)
    lag.setValueVec(zeros, np.zeros(3*nbCellsLoc, np.float64), cId, cellsMap, 3*nbCellsLoc)

    ### Mat
    # Mat
    matOP1       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matOP2       = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matOP3       = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matOP4       = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    # Not Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    ### matOP
    # Mat 1
    # Curl Rho Curl
    size = mb.ExDiamond(row, col, coeff, FIn, faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     mb.curlRhoCurlStrip, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamond(row, col, coeff, FSupSup, faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap, cellsMap,
                         1., size, addRule=True)

    # Mat 2
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
    # Mat 3
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
    # Mat 4
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FSupAirL, FSupAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FSupAirR, FSupAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)
    # ksp = lag.getLinearSolver('fgmres', 'fieldsplit', 1e-8, 1e-4, 200)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # rhsF
    rhsF.setArray(np.zeros(3*nbFacesLoc, np.float64))
    # Init H and J
    lag.copy(diriH, H)
    matRot.mult(H, J)

    ### Generics objects
    matOP = lag.createNest(2, [[matOP1, matOP2], [matOP3, matOP4]])
    rhsG  = lag.createNest(1, [rhsH, rhsF])
    cptG  = lag.createNest(1, [H, F])

    ### Save mesh and nrmHSupra
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
    if saveNrm:
        volLoc = cellsVol[:nbCellsLoc] * (physCells[:nbCellsLoc] == 100001)
        nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zeros), 3, 2, volLoc)
        nrmJ = ut.diff(COMM, lag.getArray(J), lag.getArray(zeros), 3, 2, volLoc)
        if RANK == 0:
            if os.path.exists('nrmHSupra'):
                os.remove('nrmHSupra')
            if os.path.exists('nrmJSupra'):
                os.remove('nrmJSupra')

            fNrmH = open('nrmHSupra', 'x')
            fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
            fNrmJ = open('nrmJSupra', 'x')
            fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")

    ### Loop
    while t+dt < tEnd:
        t += dt
        it += 1

        lag.copy(diriH, rhsH)
        rhsH.axpby(1/dt, 1, volNotBorder*H)
        lag.solveLin(ksp, matOP, cptG, rhsG)
        matRot.mult(H, J)

        nbIt = ksp.getIterationNumber()
        if nbIt == maxIt or nbIt == 0:
            print('Error while solve, nbIt: ', nbIt)
            exit()
        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
        if saveNrm:
            nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zeros), 3, 2, volLoc)
            nrmJ = ut.diff(COMM, lag.getArray(J), lag.getArray(zeros), 3, 2, volLoc)
            if RANK == 0:
                fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
                fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")

    if saveNrm and RANK == 0:
        fNrmH.close()
        fNrmJ.close()

    if debugMat:
        lag.fileViewPetscObj(matP *  matOP1 * matPp,  'matOP1.out')
        lag.fileViewPetscObj(matP *  matOP2 * matFp,  'matOP2.out')
        lag.fileViewPetscObj(matF *  matOP3 * matPp,  'matOP3.out')
        lag.fileViewPetscObj(matF *  matOP4 * matFp,  'matOP4.out')
# Smart interfaces: Lin
if False:
    buildFunEq = mb.curlRhoCurlEps
    buildFun   = mb.curlRhoCurlStrip
    buildFun = mb.curlRhoCurl
    linFun   = supLin
    jacFun   = supLin2

    t = 0.
    it = 0
    dt = 1e-3
    tEnd = 50*dt

    rhoAir = 1e0
    mu0 = 4*np.pi * 1e-4
    Ec = 1e-7
    Jc = 1e1
    n = 10

    # Static test functions
    def _H0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]
        s[2] = 1e0

        return s / mu0
    def _H1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]
        s[0] = 1e-2

        return s / mu0
    def computeRho(rho, nbCellsLoc, JLoc, physCells, tagsAir):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            if physCells[i] in tagsAir:
                arrRho[3*i] = rhoAir / mu0
                arrRho[3*i+1] = rhoAir / mu0
                arrRho[3*i+2] = rhoAir / mu0
            else:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                J2 = arr[3*i +2] / Jc
                nrmJ = J0*J0 + J1*J1 + J2*J2
                vRho = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))

                arrRho[3*i] = vRho
                arrRho[3*i+1] = vRho
                arrRho[3*i+2] = vRho
        rho.setArray(arrRho)
        return rho

    fun = _H0

    if verbose and RANK == 0:
        print("-----------------------------------------")
        sys.stdout.flush()


    # Space for coeff
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vecs
    cptC   = lag.createVec(nbCellsLoc, 3, 3)
    memo   = lag.createVec(nbCellsLoc, 3, 3)
    zeroC  = lag.createVec(nbCellsLoc, 3, 3)
    tmpC   = lag.createVec(nbCellsLoc, 3, 3)
    rhsC   = lag.createVec(nbCellsLoc, 3, 3)
    diriC  = lag.createVec(nbCellsLoc, 3, 3)

    rhsF   = lag.createVec(nbFacesLoc, 3, 3)
    tmpF   = lag.createVec(nbFacesLoc, 3, 3)
    cptF   = lag.createVec(nbFacesLoc, 3, 3)
    zeroF  = lag.createVec(nbFacesLoc, 3, 3)

    # Ghost vec
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    cptRot = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    rho    = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    ### Mat
    # Mat
    matOP1       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matOP2       = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matOP3       = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matOP4       = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)
    memoOP1      = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    matRot      = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    matJac1      = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matJac2      = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matJac3      = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matJac4      = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    # matNotBorder
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriC, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriC, cptC)
    lag.copy(zeroF, cptF)

    ### Memo
    computeRho(rho, nbCellsLoc, cptRot, physCells, cellsAir)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    ### memoOP1
    size = mb.ExDiamond(row, col, coeff, FIn, faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, arrayRho)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                     facesNormal, diamNormal, diamVol, cellsVol,
                     nodesCellsId, leastSquareCoeff,
                     buildFun, arrayRho)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    ### Init saveNrm
    if saveNrm:
        volLoc = cellsVol[:nbCellsLoc] * (physCells[:nbCellsLoc] == 100001)
        if RANK == 0:
            if os.path.exists('nrmHSupra'):
                os.remove('nrmHSupra')
            if os.path.exists('nrmJSupra'):
                os.remove('nrmJSupra')

            fNrmH = open('nrmHSupra', 'x')
            fNrmJ = open('nrmJSupra', 'x')
    ### Loop
    while t+dt < tEnd:
        lag.copy(cptC, tmpC)
        lag.copy(cptF, tmpF)

        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(cptC),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(cptRot),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
        if saveNrm:
            nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zeroC), 3, 2, volLoc)
            nrmJ = ut.diff(COMM, lag.getArray(cptRot), lag.getArray(zeroC), 3, 2, volLoc)
            if RANK == 0:
                fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
                fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
        t += dt
        it += 1

        while True:
            matOP1.zeroEntries()
            matOP2.zeroEntries()
            matOP3.zeroEntries()
            matOP4.zeroEntries()

            matJac1.zeroEntries()
            matJac2.zeroEntries()
            matJac3.zeroEntries()
            matJac4.zeroEntries()

            lag.copy(tmpC, memo)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpC),
                              lag.getArray(zeroC), 3, 2, np.ones(nbCellsLoc))

            # Update Rot and Rho
            matRot.mult(cptC, cptRot)
            lag.syncGhostVec(cptRot)
            arrayRot = lag.getGhostArray(cptRot)

            computeRho(rho, nbCellsLoc, cptRot, physCells, cellsAir)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)


            ### Update matOP
            # Jac
            size = mb.ExDiamond(row, col, coeff, FSup, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac2, row, col, coeff,
                            cellsMap, facesMap,
                            size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac3, row, col, coeff,
                            facesMap, cellsMap,
                            size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac4, row, col, coeff,
                            facesMap, facesMap,
                            size, addRule=True)

            # Curl Rho Curl
            size = mb.ExDiamond(row, col, coeff, FSup, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            # Add memoOP1 values
            matOP1.axpy(1., memoOP1)

            # Mat 2
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)

            # Mat 3
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)

            # Mat 4
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)

            # # Update pb
            matOp = lag.createNest(2, [[matOP1, matOP2], [matOP3, matOP4]])

            lag.copy(diriC, rhsC)
            rhsC.axpby(1/dt, 1, volNotBorder*cptC)
            rhsC = rhsC + matJac1*tmpC + matJac2*tmpF
            rhsF = matJac3*tmpC + matJac4*tmpF

            # Build nest vector
            rhsG = lag.createNest(1, [rhsC, rhsF])
            cptG = lag.createNest(1, [tmpC, tmpF])

            lag.solveLin(ksp, matOp, cptG, rhsG)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memo),
                              lag.getArray(tmpC), 3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()

            if nrmDiff/nrmPred < 1e-3:
                if verbose and RANK == 0:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
            if nbIt == 0:
                break

        if nbIt == 0:
            break
        lag.copy(tmpC, cptC)
        lag.copy(tmpF, cptF)

    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(cptC),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(cptRot),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
    if saveNrm:
        nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zeroC), 3, 2, volLoc)
        nrmJ = ut.diff(COMM, lag.getArray(cptRot), lag.getArray(zeroC), 3, 2, volLoc)
        if RANK == 0:
            fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
            fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
            fNrmH.close()
            fNrmJ.close()

################################################################################
#                                           Testing triD                       #
################################################################################
sys.stdout.flush()

# Faces list
if True:
    tAll = [-1, 1001, 1002, 1003, 1004]
    tIn = [-1]
    tBorder = [1001, 1002, 1003, 1004]
    cellsAll = [100001, 100002, 100003]
    cellsFil = [100001]
    cellsWir = [100002]
    cellsAir = [100003]

    ### Extract all needed faces
    CIn      = ExtractCells(cellsFTags, nbCellsLoc,      tIn)
    CBd      = ExtractCells(cellsFTags, nbCellsLoc,  tBorder)

    _FIn     = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsAir)])
    FIn      = ExtractFacesChain(_FIn, cellsFTags, [(-1, -1)])
    _FInIn   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                            [(cellsAir, cellsAir), (cellsAir, cellsWir),
                             (cellsWir, cellsAir), (cellsWir, cellsWir),
                             (cellsWir, cellsFil), (cellsFil, cellsWir),
                             (cellsFil, cellsFil)])
    FInIn    = ExtractFacesChain(_FInIn, cellsFTags, [(-1, -1)])
    FInBdL   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(-1, 1001), (-1, 1002), (-1, 1003), (-1, 1004)])
    FInBdR   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, -1), (1002, -1), (1003, -1), (1004, -1)])

    FFilWirL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsFil, cellsWir)])
    FFilWirR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsWir, cellsFil)])
    FFilFil  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsFil, cellsFil)])

    FWirAirL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsWir, cellsAir)])
    FWirAirR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsWir)])
    FWirWir  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsWir, cellsWir)])

    FAirAir  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsAir)])

# Faces numbering
if True:
    ### Debug
    FAllL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                         [(cellsFil, cellsWir), (cellsWir, cellsAir)])
    FAllR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                         [(cellsWir, cellsFil), (cellsAir, cellsWir)])

    def getMPINeig(faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLst = -np.ones(mpiSize, np.int32)
        for i in range(nbInnerFaces, nbInnerFaces+nbSharedFaces):
            v = cellsMaps[faces[i][3]]
            j = 0
            while mpiCellsNb[j] < v:
                j += 1
            neigLst[j-1] = j-1
        return neigLst
    def getFacesNb(listOfFacesList, faces):
        nbOwnd       = 0
        nbToSyncOwnd = 0
        nbToSyncBrwd = 0
        for lstF in listOfFacesList:
            nbOwnd += len(lstF[0])
            for f in lstF[1]:
                if faces[f][5] == 1:
                    nbToSyncOwnd += 1
                else:
                    nbToSyncBrwd += 1
        return nbOwnd, nbToSyncOwnd, nbToSyncBrwd
    def getFaceNeig(listOfFacesList, faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLstOwnd = [[] for i in range(mpiSize)]
        neigLstBrwd = [[] for i in range(mpiSize)]
        for lstF in listOfFacesList:
            for f in lstF[1]:
                if f < nbInnerFaces:
                    continue

                v = cellsMaps[faces[f][3]]
                j = 0
                while mpiCellsNb[j] <= v:
                    j += 1
                if faces[f][5] == 1:
                    neigLstOwnd[j-1].append(f)
                else:
                    neigLstBrwd[j-1].append(f)

        return neigLstOwnd, neigLstBrwd

    neigLst = getMPINeig(faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)
    neigLstOwnd, neigLstBrwd = getFaceNeig([FAllL, FAllR], faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)

    nbFOwnd, nbFToSyncOwnd, nbFToSyncBrwd = getFacesNb([FAllL, FAllR], faces)

    nbFacesArray = np.zeros(SIZE+1, np.int32)
    nbFacesArray[RANK+1] = nbFOwnd + nbFToSyncOwnd
    nbFacesArray = COMM.allreduce(nbFacesArray, op=MPI.SUM)
    nbFacesArray = nbFacesArray.cumsum().astype(np.int32)

    facesCalcId = 1000000000 * np.ones(nbInnerFaces + nbSharedFaces, np.int32)
    def innerFacesCalcId(listOfFacesList, facesCalcId, nbFOwnd, nbFacesArray, rank):
        vId = 0

        for lstF in listOfFacesList:
            for f in lstF[0]:
                facesCalcId[f] = nbFacesArray[rank] + vId
                vId += 1

        assert(vId == nbFOwnd)
    def owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                        nbFacesArray, rank, mpiSize):
        vId = 0
        remapFOwnd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstOwnd[i]:
                facesCalcId[f] = nbFacesArray[rank] + nbFOwnd + vId
                vId += 1
                tmp = (facesCalcId[f], cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                remapFOwnd[i].append(tmp)

        assert(vId == nbFToSyncOwnd)
        return remapFOwnd
    def brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, mpiSize):
        oldFBrwd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstBrwd[i]:
                tmp = (f, cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                oldFBrwd[i].append(tmp)

        return oldFBrwd

    innerFacesCalcId([FAllL, FAllR], facesCalcId, nbFOwnd, nbFacesArray, RANK)
    remapFOwnd = owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                                 nbFacesArray, RANK, SIZE)
    oldFBrwd = brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, SIZE)

    def syncBrwWithRecieve(faces, facesCalcId, oldFBrwd, recievedF, mpiSize):
        tupleMap = []
        assert(len(oldFBrwd) == len(recievedF))
        for descF in oldFBrwd:
            j = 0
            while descF[1] != recievedF[j][2] and descF[2] != recievedF[j][1]:
                j += 1
            facesCalcId[descF[0]] = recievedF[j][0]

    for i in range(SIZE):
        if len(remapFOwnd[i]) != 0 or len(oldFBrwd[i]) != 0:
            if i < RANK:
                if len(remapFOwnd[i]) != 0:
                    COMM.send(remapFOwnd[i], dest=i)
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
            else:
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
                if len(remapFOwnd[i]) != 0:
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                    COMM.send(remapFOwnd[i], dest=i)

    facesMap = np.zeros((nbInnerFaces + nbSharedFaces, 3), np.int32)
    facesMap[:,0] = 3*facesCalcId[:]
    facesMap[:,1] = 3*facesCalcId[:] +1
    facesMap[:,2] = 3*facesCalcId[:] +2
    facesMap = np.reshape(facesMap, 3*(nbInnerFaces + nbSharedFaces))

    nbFacesLoc = nbFOwnd + nbFToSyncOwnd
    if verbose and RANK == 0:
        print('nbFacesLoc:', nbFacesLoc)

# Tri-Domain diamond
if False:
    buildFun = mb.curlRhoCurl

    t = 0.
    dt = 1e-3
    it = 0
    tEnd = 150*dt

    def funH0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[2] = 1
        return s
    def funH1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[0] = -y
        s[1] =  x
        return s
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            v = rhoDict[physCells[i]]
            arrRho[3*i]   = v
            arrRho[3*i+1] = v
            arrRho[3*i+2] = v
        rho.setArray(arrRho)
        return rho

    ### Verif
    fun = funH0
    rhoFil =   0.1 * 1e-3
    rhoWir =   50. * 1e-3
    rhoAir = 1000. * 1e-3
    rhoRules = {
            cellsFil[0]: rhoFil,
            cellsWir[0]: rhoWir,
            cellsAir[0]: rhoAir,
            }

    if verbose and RANK == 0:
        print("-----------------------------------------")

    # # Space for coeff
    szBuff = 10000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vecs
    # Vec
    H     = lag.createVec(nbCellsLoc, 3, 3)
    J     = lag.createVec(nbCellsLoc, 3, 3)
    diriH = lag.createVec(nbCellsLoc, 3, 3)
    rhsH  = lag.createVec(nbCellsLoc, 3, 3)
    zero  = lag.createVec(nbCellsLoc, 3, 3)
    # Ghost vec and init
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    computeRho(rho, nbCellsLoc, rho, physCells, rhoRules)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    ### Mat
    # Mat
    matOP        = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    # Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)

    ### matOP
    # Curl Rho Curl
    size = mb.ExDiamond(row, col, coeff, FInIn, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(matOP, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # Setup

    ### Linear solver
    maxIt = 500
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)
    # ksp = lag.getLinearSolver('bcgs', 'asm', 1e-8, 1e-4, maxIt)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriH, H)
    matRot.mult(H, J)

    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
    ### Init saveNrm
    if saveNrm:
        volLoc = cellsVol[:nbCellsLoc] * (physCells[:nbCellsLoc] == 100001)
        if RANK == 0:
            if os.path.exists('nrmHSupra'):
                os.remove('nrmHSupra')
            if os.path.exists('nrmJSupra'):
                os.remove('nrmJSupra')

            fNrmH = open('nrmHSupra', 'x')
            fNrmJ = open('nrmJSupra', 'x')

    if verbose and RANK == 0:
        print('Start computing...')
        sys.stdout.flush()
    ### Loop
    while t+dt < tEnd:
        if saveNrm:
            nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zero), 3, 2, volLoc)
            nrmJ = ut.diff(COMM, lag.getArray(J), lag.getArray(zero), 3, 2, volLoc)
            if RANK == 0:
                fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
                fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
        t += dt
        it += 1

        lag.copy(diriH, rhsH)
        rhsH.axpby(1/dt, 1, volNotBorder*H)
        lag.solveLin(ksp, matOP, H, rhsH)

        matRot.mult(H, J)
        J = matNotBorder*J

        nbIt = ksp.getIterationNumber()
        if nbIt == maxIt or nbIt == 0:
            print('Error while solve, nbIt: ', nbIt)
            exit()
        if verbose and RANK == 0:
            print('Iteration number: ', nbIt)
            sys.stdout.flush()

        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)

    if debugMat:
        lag.fileViewPetscObj(matP *  matOP * matPp,  'matOP.out')
        lag.fileViewPetscObj(matP * matRot * matPp, 'matRot.out')
    if saveNrm:
        nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zero), 3, 2, volLoc)
        nrmJ = ut.diff(COMM, lag.getArray(J), lag.getArray(zero), 3, 2, volLoc)
        if RANK == 0:
            fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
            fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
            fNrmH.close()
            fNrmJ.close()
# Tri-domain Smart
if False:
    buildFun = mb.curlRhoCurl

    t = 0.
    dt = 1e-3
    it = 0
    tEnd = 150*dt

    def funH0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[2] = 1
        return s
    def funH1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[0] = -y
        s[1] =  x
        return s
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            v = rhoDict[physCells[i]]
            arrRho[3*i]   = v
            arrRho[3*i+1] = v
            arrRho[3*i+2] = v
        rho.setArray(arrRho)
        return rho

    ### Verif
    fun = funH0
    rhoFil =   0.1 * 1e-3
    rhoWir =   50. * 1e-3
    rhoAir = 1000. * 1e-3
    rhoRules = {
            cellsFil[0]: rhoFil,
            cellsWir[0]: rhoWir,
            cellsAir[0]: rhoAir,
            }

    if verbose and RANK == 0:
        print("-----------------------------------------")
    if verbose and RANK == 0:
        print("-----------------------------------------")

    # Space for coeff
    szBuff = 1000000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vecs
    # Vec
    H     = lag.createVec(nbCellsLoc, 3, 3)
    J     = lag.createVec(nbCellsLoc, 3, 3)
    F     = lag.createVec(nbFacesLoc, 3, 3)
    diriH = lag.createVec(nbCellsLoc, 3, 3)
    rhsH  = lag.createVec(nbCellsLoc, 3, 3)
    rhsF  = lag.createVec(nbFacesLoc, 3, 3)
    zeros = lag.createVec(nbCellsLoc, 3, 3)
    # Ghost vec and init
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    computeRho(rho, nbCellsLoc, rho, physCells, rhoRules)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    cId = np.array([i for i in range(3*nbCellsLoc)], np.int32)
    lag.setValueVec(zeros, np.zeros(3*nbCellsLoc, np.float64), cId, cellsMap, 3*nbCellsLoc)

    ### Mat
    # Mat
    matOP1       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matOP2       = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matOP3       = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matOP4       = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    # Not Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    ### matOP
    # Mat 1
    # Curl Rho Curl
    size = mb.ExDiamond(row, col, coeff, FIn, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamond(row, col, coeff, FWirWir, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamond(row, col, coeff, FFilFil, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FFilWirL, FFilWirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FFilWirR, FFilWirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap, cellsMap,
                         1., size, addRule=True)

    # Mat 2
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FFilWirL, FFilWirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FFilWirR, FFilWirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)

    # Mat 3
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FFilWirL, FFilWirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FFilWirR, FFilWirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)

    # Mat 4
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FFilWirL, FFilWirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FFilWirR, FFilWirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-12, 1e-12, maxIt)
    # ksp = lag.getLinearSolver('fgmres', 'fieldsplit', 1e-8, 1e-4, 200)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # rhsF
    rhsF.setArray(np.zeros(3*nbFacesLoc, np.float64))
    # Init H and J
    lag.copy(diriH, H)
    matRot.mult(H, J)

    ### Generics objects
    matOP = lag.createNest(2, [[matOP1, matOP2], [matOP3, matOP4]])
    rhsG  = lag.createNest(1, [rhsH, rhsF])
    cptG  = lag.createNest(1, [H, F])

    ### Save mesh and nrmHSupra
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
    if saveNrm:
        volLoc = cellsVol[:nbCellsLoc] * (physCells[:nbCellsLoc] == 100001)
        nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zeros), 3, 2, volLoc)
        nrmJ = ut.diff(COMM, lag.getArray(J), lag.getArray(zeros), 3, 2, volLoc)
        if RANK == 0:
            if os.path.exists('nrmHSupra'):
                os.remove('nrmHSupra')
            if os.path.exists('nrmJSupra'):
                os.remove('nrmJSupra')

            fNrmH = open('nrmHSupra', 'x')
            fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
            fNrmJ = open('nrmJSupra', 'x')
            fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")

    ### Loop
    while t+dt < tEnd:
        t += dt
        it += 1

        lag.copy(diriH, rhsH)
        rhsH.axpby(1/dt, 1, volNotBorder*H)
        lag.solveLin(ksp, matOP, cptG, rhsG)
        matRot.mult(H, J)

        nbIt = ksp.getIterationNumber()
        if nbIt == maxIt or nbIt == 0:
            print('Error while solve, nbIt: ', nbIt)
            exit()
        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
        if saveNrm:
            nrmH = ut.diff(COMM, lag.getArray(H), lag.getArray(zeros), 3, 2, volLoc)
            nrmJ = ut.diff(COMM, lag.getArray(J), lag.getArray(zeros), 3, 2, volLoc)
            if RANK == 0:
                fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
                fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")

    if saveNrm and RANK == 0:
        fNrmH.close()
        fNrmJ.close()

    if debugMat:
        lag.fileViewPetscObj(matOP1,  'matOP1.out')
        lag.fileViewPetscObj(matOP2,  'matOP2.out')
        lag.fileViewPetscObj(matOP3,  'matOP3.out')
        lag.fileViewPetscObj(matOP4,  'matOP4.out')

# Lin Diamond
if False:
    buildFun = mb.curlRhoCurl
    linFun   = supLin
    jacFun   = supLin2

    t = 0.
    it = 0
    dt = 5e-4
    tEnd = 100*dt

    mu0 = 4*np.pi * 1e-4
    Ec = 1e-7
    Jc = 1e1
    n = 10

    # Static test functions
    def funH0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[2] = 1e-3
        return s / mu0
    def funH1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[0] =  -y / 1e2
        s[1] =   x / 1e2
        return s / mu0
    # Static test functions
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            rule = rhoDict[physCells[i]]
            if rule[0]:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                J2 = arr[3*i +2] / Jc
                nrmJ = J0*J0 + J1*J1 + J2*J2
                vRho = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))

                arrRho[3*i]   = vRho
                arrRho[3*i+1] = vRho
                arrRho[3*i+2] = vRho
            else:
                v = rule[1]
                arrRho[3*i]   = v
                arrRho[3*i+1] = v
                arrRho[3*i+2] = v
        rho.setArray(arrRho)
        return rho

    ### Verif
    fun = funH0
    rhoRules = {
            cellsFil[0]: ( True,    0),
            cellsWir[0]: (False, 1e-2),
            cellsAir[0]: (False,  1e0),
            }

    if verbose:
        if RANK == 0:
            print("-----------------------------------------")

    # Space for coeff
    szBuff = 10000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vecs
    cptC     = lag.createVec(nbCellsLoc, 3, 3)
    memo     = lag.createVec(nbCellsLoc, 3, 3)
    zero     = lag.createVec(nbCellsLoc, 3, 3)
    tmpC     = lag.createVec(nbCellsLoc, 3, 3)
    rhsC     = lag.createVec(nbCellsLoc, 3, 3)
    diriC    = lag.createVec(nbCellsLoc, 3, 3)
    # Ghost vec
    ghostId  = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    cptRot   = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    rho      = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    ### Mat
    # Mat
    matOP        = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    memoOP       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matJac       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    # matNotBorder
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
                           facesNormal, cellsVol, nodesCellsId,
                           leastSquareCoeff, mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
                             facesNormal, cellsVol, nodesCellsId,
                             leastSquareCoeff, mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)
    # ksp = lag.getLinearSolver('fgmres', 'fieldsplit', 1e-8, 1e-4, 200)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriC, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriC, cptC)

    ### Init memo
    computeRho(rho, nbCellsLoc, cptRot, physCells, rhoRules)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    size = mb.ExDiamond(row, col, coeff, FIn, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamond(row, col, coeff, FWirWir, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamondLR(row, col, coeff, [FWirAirL, FWirAirR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamondLR(row, col, coeff, [FWirAirR, FWirAirL], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(memoOP, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(memoOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    ### Init saveNrm
    if saveNrm:
        volLoc = cellsVol[:nbCellsLoc] * (physCells[:nbCellsLoc] == 100001)
        if RANK == 0:
            if os.path.exists('nrmHSupra'):
                os.remove('nrmHSupra')
            if os.path.exists('nrmJSupra'):
                os.remove('nrmJSupra')

            fNrmH = open('nrmHSupra', 'x')
            fNrmJ = open('nrmJSupra', 'x')
    ### Loop
    while t+dt < tEnd:
        lag.copy(cptC, tmpC)

        ### Save mesh
        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(cptC),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(matNotBorder * cptRot),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
        if saveNrm:
            nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zero), 3, 2, volLoc)
            nrmJ = ut.diff(COMM, lag.getArray(cptRot), lag.getArray(zero), 3, 2, volLoc)
            if RANK == 0:
                fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
                fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
        t += dt
        it += 1

        while True:
            matJac.zeroEntries()
            matOP.zeroEntries()

            lag.copy(tmpC, memo)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpC),
                              lag.getArray(zero), 3, 2, np.ones(nbCellsLoc))

            # Update Rot
            matRot.mult(tmpC, cptRot)
            lag.syncGhostVec(cptRot)
            arrayRot = lag.getGhostArray(cptRot)

            computeRho(rho, nbCellsLoc, cptRot, physCells, rhoRules)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)

            ### Update matOP
            # Jac
            size = mb.ExDiamond(row, col, coeff, FFilFil, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FFilWirL, FFilWirR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  jacFun, arrayRot)
            lag.setValueMat(matJac, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

            # Curl Rho Curl
            size = mb.ExDiamond(row, col, coeff, FFilFil, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FFilWirL, FFilWirR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  linFun, arrayRot)
            lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FFilWirR, FFilWirL], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap, cellsMap, size, addRule=True)


            # Add memoOP values
            matOP.axpy(1., memoOP)

            # # Update pb
            lag.copy(diriC, rhsC)
            rhsC.axpby(1/dt, 1, volNotBorder*cptC)
            rhsC = rhsC + matJac*tmpC
            lag.solveLin(ksp, matOP, tmpC, rhsC)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memo),
                              lag.getArray(tmpC), 3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()

            if nrmDiff/nrmPred < 1e-3:
                if RANK == 0 and verbose:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
            if nbIt == 0:
                break
        if nbIt == 0:
            break
        lag.copy(tmpC, cptC)
    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(cptC),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(cptRot),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
    if saveNrm:
        nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zero), 3, 2, volLoc)
        nrmJ = ut.diff(COMM, lag.getArray(matNotBorder * cptRot), lag.getArray(zero), 3, 2, volLoc)
        if RANK == 0:
            fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
            fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
            fNrmH.close()
            fNrmJ.close()
# Lin Split
if False:
    buildFun = mb.curlRhoCurl
    linFun   = supLin
    jacFun   = supLin2

    t = 0.
    it = 0
    dt = 2e-4
    tEnd = 150*dt

    rhoAir = 1e0
    mu0 = 4*np.pi * 1e-4
    Ec = 1e-7
    Jc = 1e1
    n = 10

    # Static test functions
    def funH0(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[2] = 1e-3
        return s / mu0
    def funH1(p):
        s = np.zeros(3)
        x = p[0]
        y = p[1]
        z = p[2]

        s[0] =  -y / 1e2
        s[1] =   x / 1e2
        return s / mu0
    # Static test functions
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            rule = rhoDict[physCells[i]]
            if rule[0]:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                J2 = arr[3*i +2] / Jc
                nrmJ = J0*J0 + J1*J1 + J2*J2
                vRho = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))

                arrRho[3*i]   = vRho
                arrRho[3*i+1] = vRho
                arrRho[3*i+2] = vRho
            else:
                v = rule[1]
                arrRho[3*i]   = v
                arrRho[3*i+1] = v
                arrRho[3*i+2] = v
        rho.setArray(arrRho)
        return rho

    ### Verif
    fun = funH0
    rhoRules = {
            cellsFil[0]: ( True,    0),
            cellsWir[0]: (False, 1e-2),
            cellsAir[0]: (False,  1e0),
            }

    if verbose and RANK == 0:
        print("-----------------------------------------")
        sys.stdout.flush()


    # Space for coeff
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vecs
    cptC   = lag.createVec(nbCellsLoc, 3, 3)
    memo   = lag.createVec(nbCellsLoc, 3, 3)
    zeroC  = lag.createVec(nbCellsLoc, 3, 3)
    tmpC   = lag.createVec(nbCellsLoc, 3, 3)
    rhsC   = lag.createVec(nbCellsLoc, 3, 3)
    diriC  = lag.createVec(nbCellsLoc, 3, 3)

    rhsF   = lag.createVec(nbFacesLoc, 3, 3)
    tmpF   = lag.createVec(nbFacesLoc, 3, 3)
    cptF   = lag.createVec(nbFacesLoc, 3, 3)
    zeroF  = lag.createVec(nbFacesLoc, 3, 3)

    # Ghost vec
    ghostId = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    cptRot = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)
    rho    = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    ### Mat
    # Mat
    matOP1       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matOP2       = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matOP3       = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matOP4       = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    memoOP1      = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    memoOP2      = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    memoOP3      = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    memoOP4      = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    matJac1      = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matJac2      = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matJac3      = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matJac4      = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    # matNotBorder
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # Rot CIn
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
              facesNormal, cellsVol, nodesCellsId, leastSquareCoeff,
              mb.edgesCurl)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder, cellsCenter,
                          [1.], nbCellsLoc, 3)
    lag.setValueVec(diriC, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriC, cptC)
    lag.copy(zeroF, cptF)

    ### Init memo
    computeRho(rho, nbCellsLoc, cptRot, physCells, rhoRules)
    lag.syncGhostVec(rho)
    arrayRho = lag.getGhostArray(rho)

    ### memoOP1
    size = mb.ExDiamond(row, col, coeff, FIn, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExDiamond(row, col, coeff, FWirWir, faces,
                        facesNormal, diamNormal, diamVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                          facesNormal, diamNormal, diamVol, cellsVol,
                          nodesCellsId, leastSquareCoeff,
                          buildFun, arrayRho)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 1,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    # ID dt
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, mb.cellsId3)
    lag.setScaleValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap,
                         1./dt, size, addRule=True)
    # ID Bd
    size = mb.ExCells(row, col, coeff, CBd, cellsVol, mb.cellsOneOnDiag)
    lag.setValueMat(memoOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

    ### memoOP2
    size = mb.ExSplitLR(row, col, coeff, 2.,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 2,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)

    ### memoOP3
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 3,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)

    ### memoOP4
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FWirAirL, FWirAirR], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
    size = mb.ExSplitLR(row, col, coeff, 4,
                        [FWirAirR, FWirAirL], faces,
                        facesNormal, diamNormal, splitDNormal,
                        diamVol, splitDVol, cellsVol,
                        nodesCellsId, leastSquareCoeff,
                        buildFun, arrayRho)
    lag.setValueMat(memoOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)

    ### Init saveNrm
    if saveNrm:
        volLoc = cellsVol[:nbCellsLoc] * (physCells[:nbCellsLoc] == 100001)
        if RANK == 0:
            if os.path.exists('nrmHSupra'):
                os.remove('nrmHSupra')
            if os.path.exists('nrmJSupra'):
                os.remove('nrmJSupra')

            fNrmH = open('nrmHSupra', 'x')
            fNrmJ = open('nrmJSupra', 'x')
    ### Loop
    while t+dt < tEnd:
        lag.copy(cptC, tmpC)
        lag.copy(cptF, tmpF)

        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(cptC),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(cptRot),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
        if saveNrm:
            nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zeroC), 3, 2, volLoc)
            nrmJ = ut.diff(COMM, lag.getArray(cptRot), lag.getArray(zeroC), 3, 2, volLoc)
            if RANK == 0:
                fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
                fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
        t += dt
        it += 1

        while True:
            matOP1.zeroEntries()
            matOP2.zeroEntries()
            matOP3.zeroEntries()
            matOP4.zeroEntries()

            matJac1.zeroEntries()
            matJac2.zeroEntries()
            matJac3.zeroEntries()
            matJac4.zeroEntries()

            lag.copy(tmpC, memo)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpC),
                              lag.getArray(zeroC), 3, 2, np.ones(nbCellsLoc))

            # Update Rot and Rho
            matRot.mult(cptC, cptRot)
            lag.syncGhostVec(cptRot)
            arrayRot = lag.getGhostArray(cptRot)

            computeRho(rho, nbCellsLoc, cptRot, physCells, rhoRules)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)


            ### Update matOP
            # Jac
            size = mb.ExDiamond(row, col, coeff, FFilFil, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac2, row, col, coeff,
                            cellsMap, facesMap,
                            size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac3, row, col, coeff,
                            facesMap, cellsMap,
                            size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                jacFun, arrayRot)
            lag.setValueMat(matJac4, row, col, coeff,
                            facesMap, facesMap,
                            size, addRule=True)

            # Curl Rho Curl
            size = mb.ExDiamond(row, col, coeff, FFilFil, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap, cellsMap, size, addRule=True)
            # Add memoOP1 values
            matOP1.axpy(1., memoOP1)

            # Mat 2
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap, facesMap, size, addRule=True)
            matOP2.axpy(1., memoOP2)

            # Mat 3
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap, cellsMap, size, addRule=True)
            matOP3.axpy(1., memoOP3)

            # Mat 4
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                linFun, arrayRot)
            lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap, facesMap, size, addRule=True)
            matOP4.axpy(1., memoOP4)

            # # Update pb
            matOp = lag.createNest(2, [[matOP1, matOP2], [matOP3, matOP4]])

            lag.copy(diriC, rhsC)
            rhsC.axpby(1/dt, 1, volNotBorder*cptC)
            rhsC = rhsC + matJac1*tmpC + matJac2*tmpF
            rhsF = matJac3*tmpC + matJac4*tmpF

            # Build nest vector
            rhsG = lag.createNest(1, [rhsC, rhsF])
            cptG = lag.createNest(1, [tmpC, tmpF])

            lag.solveLin(ksp, matOp, cptG, rhsG)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memo),
                              lag.getArray(tmpC), 3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()

            if nrmDiff/nrmPred < 1e-3:
                if verbose and RANK == 0:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
            if nbIt == 0:
                break

        if nbIt == 0:
            break
        lag.copy(tmpC, cptC)
        lag.copy(tmpF, cptF)

    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(cptC),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(cptRot),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
    if saveNrm:
        nrmH = ut.diff(COMM, lag.getArray(cptC), lag.getArray(zeroC), 3, 2, volLoc)
        nrmJ = ut.diff(COMM, lag.getArray(cptRot), lag.getArray(zeroC), 3, 2, volLoc)
        if RANK == 0:
            fNrmH.write(str(t) + ' ' + str(nrmH) + "\n")
            fNrmJ.write(str(t) + ' ' + str(nrmJ) + "\n")
            fNrmH.close()
            fNrmJ.close()
