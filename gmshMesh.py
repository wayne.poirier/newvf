################################################################################
#                                                Mesh reading and partitioning #
################################################################################

# Give a gmsh file and save it under the wanted hdf5 format.
# It also set firstId to write element under the gmsh ElementData format.
def gmshToHDF5(SIZE, RANK, COMM, fileName):
    if RANK == 0:
        nodes, domEdges, cells, physDomEdges, \
                physCells = readGmsh(fileName)
        # Save hdf5 with MPI constraint
        if SIZE > 1:
            epart, npart = makePartition(SIZE, cells, len(nodes))
            savePartMesh('hdf5Mesh', cells, nodes, domEdges,
                            physCells, physDomEdges, epart, npart, SIZE)
        else:
            savePartMesh('hdf5Mesh', cells, nodes, domEdges,
                            physCells, physDomEdges, None, None, SIZE)
        firstId = len(domEdges)+1
    else:
        firstId = 0

    # Set firstId (need MPI sharing).
    firstId = COMM.bcast(firstId, root=0)
    setFirstId(firstId)

# Read gmsh file format.
def readGmsh(fileName):
    import meshio
    mesh = meshio.read(fileName)

    nodes = mesh.points
    domEdges = mesh.cells['line']
    cells = mesh.cells['triangle']

    physDomEdges = mesh.cell_data['line']['gmsh:physical']
    physCells = mesh.cell_data['triangle']['gmsh:physical']

    return nodes, domEdges, cells, physDomEdges, physCells

# Make the metis partition.
def makePartition(sz, cells, nbNodes):
    from mgmetis import metis
    from mgmetis.enums import OPTION

    option = metis.get_default_options()
    option[OPTION.NUMBERING] = 0
    option[OPTION.DBGLVL] = 2
    option[OPTION.MINCONN] = 1
    option[OPTION.CONTIG] = 1
    option[OPTION.OBJTYPE] = 1

    objval, epart, npart = \
            metis.part_mesh_dual(sz, cells, nv=nbNodes, opts=option)
    return epart, npart

# We read hdf5 inside the given directory.
def readPartMesh(dirName, mpiRk):
    import os
    import h5py

    fileName = os.path.join(dirName, 'mesh' + str(mpiRk) + '.hdf5')
    with h5py.File(fileName, 'r') as f:
        cells = f['cells'][:]
        nodes = f['nodes'][:]

        cellsGlobId = f['cellsGlobId'][:]
        nodesGlobId = f['nodesGlobId'][:]

        nbCellsLoc = f['nbCellsLoc'][0]
        nbNodesLoc = f['nbNodesLoc'][0]

        edgesDom = f['edgesDom'][:]
        physCells = f['physCells'][:]
        physEdgesDom = f['physEdgesDom'][:]
    f.close()

    return cells, nodes, cellsGlobId, nodesGlobId,\
            nbCellsLoc, nbNodesLoc, edgesDom, physCells, physEdgesDom

# Save mesh data for mpiSz process. Each of them have their own HDF5 file.
def savePartMesh(dirName, cells, nodes, edgesDom, physCells, physEdgesDom,
                 epart, npart, mpiSz):

    import os
    import h5py
    import numpy as np

    print('Saving partition files ...')
    print('Number of cells: ', len(cells))
    print('Number of nodes: ', len(nodes))

    # Need to add global id for cells and nodes to each process.
    cellsGlobId = np.array([i for i in range(len(cells))], dtype=np.int32)
    nodesGlobId = np.array([i for i in range(len(nodes))], dtype=np.int32)

    if not os.path.exists(dirName):
        os.mkdir(dirName)
    if mpiSz == 1:
        fileName = os.path.join(dirName, 'mesh0.hdf5')
        with h5py.File(fileName, 'w') as f:
            f.create_dataset('cells', data=cells)
            f.create_dataset('nodes', data=nodes)

            f.create_dataset('cellsGlobId', data=cellsGlobId)
            f.create_dataset('nodesGlobId', data=nodesGlobId)

            f.create_dataset('nbCellsLoc', data=[len(cells)])
            f.create_dataset('nbNodesLoc', data=[len(nodes)])

            f.create_dataset('edgesDom', data=edgesDom)
            f.create_dataset('physCells', data=physCells)
            f.create_dataset('physEdgesDom', data=physEdgesDom)

    else:
        # Build neighborhood infomation
        neiCells, neiNodes = addNeiInfo(cells, nodes, epart, npart,
                        cellsGlobId, nodesGlobId, mpiSz)

        # Share domain edges if needed.
        edgesDomId = [[] for i in range(mpiSz)]
        for i in range(len(edgesDom)):
            n1 = npart[edgesDom[i][0]]
            n2 = npart[edgesDom[i][1]]
            edgesDomId[n1].append(i)
            edgesDomId[n2].append(i)
            for j in range(mpiSz):
                if edgesDom[i][0] in neiNodes[j]:
                    edgesDomId[j].append(i)
                if edgesDom[i][1] in neiNodes[j]:
                    edgesDomId[j].append(i)
        # Remove duplicate
        for i in range(mpiSz):
            edgesDomId[i].sort()
            tmp = []
            for j in range(1, len(edgesDomId[i])):
                if edgesDomId[i][j-1] == edgesDomId[i][j]:
                    tmp.append(edgesDomId[i][j])
            edgesDomId[i] = tmp

        # Save for each mpi process
        for mpiRk in range(mpiSz):
            fileName = os.path.join(dirName, 'mesh' + str(mpiRk) + '.hdf5')
            with h5py.File(fileName, 'w') as f:
                # Basic partitioned data
                pCells = cells[epart==mpiRk]
                pNodes = nodes[npart==mpiRk]
                pNbCells = [len(pCells)]
                pNbNodes = [len(pNodes)]
                pCellsId = cellsGlobId[epart==mpiRk]
                pNodesId = nodesGlobId[npart==mpiRk]

                # Neighbors data
                nCells = cells[neiCells[mpiRk]]
                nNodes = nodes[neiNodes[mpiRk]]

                # Fusion of the two previous list
                allCells = np.append(pCells, nCells, axis=0)
                allNodes = np.append(pNodes, nNodes, axis=0)
                allCellsId = np.append(pCellsId, neiCells[mpiRk])
                allNodesId = np.append(pNodesId, neiNodes[mpiRk])

                # Saving
                f.create_dataset('cells', data=allCells)
                f.create_dataset('nodes', data=allNodes)

                f.create_dataset('cellsGlobId', data=allCellsId)
                f.create_dataset('nodesGlobId', data=allNodesId)

                f.create_dataset('nbCellsLoc', data=pNbCells)
                f.create_dataset('nbNodesLoc', data=pNbNodes)

                f.create_dataset('edgesDom', data=edgesDom[edgesDomId[mpiRk]])
                f.create_dataset('physCells', data=physCells[allCellsId])
                f.create_dataset('physEdgesDom', data=physEdgesDom[edgesDomId[mpiRk]])

    f.close()

# Compute neighbouring data for sharing cells and nodes.
def addNeiInfo(cells, nodes, cpart, npart, cellsGlobId, nodesGlobId, mpiSz):
    # SharedNodes are nodes on the partition separation threshold.
    # NeiCells and NeiNodes contain to a given id the partition id for witch
    # we need to share Cells[id] and Nodes[id].
    sharedNodes = [[] for i in range(len(nodes))]
    neiCells = [[] for i in range(mpiSz)]
    neiNodes = [[] for i in range(mpiSz)]

    # Get all nodes on the partition threshold
    for i in range(len(cells)):
        tmp = cpart[i]
        for j in range(len(cells[i])):
            if tmp != npart[cells[i][j]]:
                sharedNodes[cells[i][j]].append(tmp)

    # Add cells and node to neighbors list
    for i in range(len(cells)):
        neighbors = False
        for j in range(len(cells[i])):
            if sharedNodes[cells[i][j]] != []:
                neighbors = True
        if neighbors:
            neiPart = []
            for j in range(len(cells[i])):
                neiPart += sharedNodes[cells[i][j]]
                neiPart.append(npart[cells[i][j]])
            neiPart.sort()
            neiCells[neiPart[0]].append(i)
            for k in range(len(cells[i])):
                neiNodes[neiPart[0]].append(cells[i][k])
            for k in range(1, len(neiPart)):
                if neiPart[k-1] != neiPart[k]:
                    neiCells[neiPart[k]].append(i)
                    for l in range(len(cells[i])):
                        neiNodes[neiPart[k]].append(cells[i][l])

    # Sort and clean duplicates
    def sortAndClean(array, part, mpiRk):
        array.sort()
        sz = len(array)
        i = 1

        while i < sz:
            if part[array[i]] == mpiRk:
                array.remove(array[i])
                sz -= 1
            elif array[i-1] == array[i]:
                array.remove(array[i])
                sz -= 1
            else:
                i += 1
        if part[array[0]] == mpiRk:
            array.remove(array[0])
        return array
    for i in range(mpiSz):
        neiCells[i] = sortAndClean(neiCells[i], cpart, i)
        neiNodes[i] = sortAndClean(neiNodes[i], npart, i)

    return neiCells, neiNodes

################################################################################
#                                                Save value under mesh format  #
################################################################################

firstId = -1

# Set the first id of the $ElementData field in gmsh file format.
def setFirstId(nb):
    global firstId
    firstId = nb

# Save array under gmsh file format for $ElementData field.
def saveMeshEData(filename, array, arrayId, dim, step, part):
    assert(int(len(array)/dim) <= len(arrayId))

    filename = filename + "-" + ('0' * (5-len(str(step)))) + str(step) \
            + '_' + ('0' * (3-len(str(part)))) + str(part) + '.msh'
    f = open(filename, "w")
    f.write("$MeshFormat\n4.1 0 8\n$EndMeshFormat\n")
    f.write("$ElementData\n")
    f.write("1 \"Data\"\n1\n0.0\n4\n")
    f.write(str(step) + "\n")
    f.write(str(dim)  + "\n")
    f.write(str(int(len(array)/dim)) + "\n")
    f.write(str(part) + "\n")

    for i in range(int(len(array)/dim)):
        f.write(str(int(firstId + arrayId[i])))
        for j in range(dim):
            f.write(" " + str(array[dim*i +j]))
        f.write("\n")

    f.write("$EndElementData")
    f.close()

# Save array under gmsh file format for $NodeData field.
def saveMeshNData(filename, array, arrayId, dim, step, part):
    assert(int(len(array)/dim) <= len(arrayId))

    filename = filename + "-" + ('0' * (5-len(str(step)))) + str(step) \
            + '_' + ('0' * (3-len(str(part)))) + str(part) + '.msh'
    f = open(filename, "w")
    f.write("$MeshFormat\n4.1 0 8\n$EndMeshFormat\n")
    f.write("$NodeData\n")
    f.write("1 \"Data\"\n1\n0.0\n4\n")
    f.write(str(step) + "\n")
    f.write(str(dim)  + "\n")
    f.write(str(int(len(array)/dim)) + "\n")
    f.write(str(part) + "\n")

    for i in range(int(len(array)/dim)):
        f.write(str(int(1 + arrayId[i])))
        for j in range(dim):
            f.write(" " + str(array[dim*i +j]))
        f.write("\n")

    f.write("$EndNodeData")
    f.close()
