import numpy as np

from mpi4py import MPI

################################################################################
#                                           Basic mesh building                #
################################################################################

# For all nodeId read from hdf5, change the global id to a local one.
def setLocalNodeId(cells, nodesGlobId, edges):
    lookup = []
    sz = len(nodesGlobId)
    for i in range(sz):
        lookup.append((nodesGlobId[i], i))
    lookup.sort()

    # Return lookup[i][1] when lookup[i][0] is provided (binary search).
    def getId(lookup, idx, sz):
        begin = 0
        end = sz-1
        while begin < end:
            mid = int((begin + end)/2)
            if lookup[mid][0] == idx:
                return lookup[mid][1]
            elif lookup[mid][0] < idx:
                begin = mid+1
            else:
                end = mid
        assert(begin == end)
        return lookup[begin][1]

    for i in range(len(cells)):
        tmp = cells[i]
        for j in range(len(tmp)):
            tmp[j] = getId(lookup, tmp[j], sz)

    for i in range(len(edges)):
        edges[i][0] = getId(lookup, edges[i][0], sz)
        edges[i][1] = getId(lookup, edges[i][1], sz)

# nodesCellsId[i][j] if the j-th cells with nodes[i] as nodes.
def cellsAroundNodes(cells, nbCells):
    nodesCellsId = [[] for i in range(nbCells)]
    for i in range(len(cells)):
        for j in range(len(cells[i])):
            nodesCellsId[cells[i][j]].append(i)
    return nodesCellsId

# Build facesData array for each faces:
# facesData description:
#  - id nA, id nB, id cL, id cR, phys, sens
#  - rule id nA < id nB
#         id cL < id cR
#  - sens of original global id (for faces normales)
def facesInfo(cells, nodes, edges, physEdges, nbCellsLoc, cellsGlobId):
    facesData = []
    facesSorted = []

    # Add all faces encountered (even duplicates)
    for i in range(len(cells)):
        n0 = cells[i][0]
        n1 = cells[i][1]
        n2 = cells[i][2]

        if n0 < n1:
            facesData.append([n0, n1, i])
        else:
            facesData.append([n1, n0, i])
        if n0 < n2:
            facesData.append([n0, n2, i])
        else:
            facesData.append([n2, n0, i])
        if n1 < n2:
            facesData.append([n1, n2, i])
        else:
            facesData.append([n2, n1, i])
    facesData.sort()

    # Merge duplicate to create face data
    sz = len(facesData)
    i = 1
    while i < sz:
        nA = facesData[i-1][0]
        nB = facesData[i-1][1]
        if nA == facesData[i][0] and nB == facesData[i][1]:
            facesSorted.append([nA, nB, facesData[i-1][2], facesData[i][2], -1, 1])
            i += 2
        else:
            facesSorted.append([nA, nB, facesData[i-1][2], -1, -1, 1])
            i += 1

    # Last facesData case
    nA = facesData[sz-1][0]
    nB = facesData[sz-1][1]
    if not(nA == facesSorted[-1][0] and nB == facesSorted[-1][1]):
        facesSorted.append([nA, nB, facesData[sz-1][2], -1, -1, 1])

    # Return edges index from nA and nB (binary search)
    def getId(faces, nA, nB, sz):
        begin = 0
        end = sz-1
        while begin < end:
            mid = int((begin + end)/2)
            if faces[mid][0] == nA and faces[mid][1] == nB:
                return mid
            elif faces[mid][0] < nA:
                begin = mid+1
            elif faces[mid][0] == nA and faces[mid][1] < nB:
                begin = mid+1
            else:
                end = mid
        assert(begin == end)
        return begin
    # Add edges tag from domian border
    for i in range(len(edges)):
        if edges[i][0] < edges[i][1]:
            nA = edges[i][0]
            nB = edges[i][1]
        else:
            nA = edges[i][1]
            nB = edges[i][0]

        pos = getId(facesSorted, nA, nB, len(facesSorted))
        facesSorted[pos][4] = physEdges[i]

    faces, nbFacesInner, nbFacesShared = sortFaces(facesSorted, nbCellsLoc)

    # Correct sens with global id
    # Not for innerfaces ?
    for i in range(nbFacesInner, len(faces)):
        if faces[i][3] >= nbCellsLoc:
            glob2 = cellsGlobId[faces[i][2]]
            glob3 = cellsGlobId[faces[i][3]]
            if glob3 < glob2:
                faces[i][5] = -1

    return faces, nbFacesInner, nbFacesShared

# Sort the faces to have in the following order:
#  - innerfaces (Left and Right cells are owned)
#  - shared faces (Left owned, Right borrowed)
#  - Borrowed faces (Left and Right borrowed)
# We also put domain edges at the begin of each categorie.
def sortFaces(faces, nbCellsLoc):
    innerBorder = []
    innerFaces = []
    sharedFaces = []
    borrowBorder = []
    borrowFaces = []
    sortedFaces = []

    for i in range(len(faces)):
        if faces[i][4] != -1:
            if faces[i][2] < nbCellsLoc:
                innerBorder.append(faces[i])
            else:
                borrowBorder.append(faces[i])
        else:
            if faces[i][2] >= nbCellsLoc:
                borrowFaces.append(faces[i])
            elif faces[i][3] >= nbCellsLoc:
                sharedFaces.append(faces[i])
            else:
                innerFaces.append(faces[i])

    for lst in [ innerBorder, innerFaces, sharedFaces,
                borrowBorder, borrowFaces]:
        for i in range(len(lst)):
            sortedFaces.append(lst[i])

    return sortedFaces, len(innerBorder) + len(innerFaces), len(sharedFaces)

################################################################################
#                                           Compute value for NM scheme        #
################################################################################

# Compute cells center (array).
def getCellsCenter(cells, nodes):
    sz = len(cells)
    center = np.zeros((sz, 3), dtype=np.float64)
    for i in range(sz):
        for j in range(len(cells[i])):
            center[i] += nodes[cells[i][j]]
        center[i] /= 3

    return center

# Compute cells volume (array).
def getCellsVol(cells, nodes):
    sz = len(cells)
    vol = np.zeros(sz, dtype=np.float64)

    for i in range(sz):
        x0 = nodes[cells[i][0]][0]
        x1 = nodes[cells[i][1]][0]
        x2 = nodes[cells[i][2]][0]

        y0 = nodes[cells[i][0]][1]
        y1 = nodes[cells[i][1]][1]
        y2 = nodes[cells[i][2]][1]

        vol[i] = 1./2. * abs((x0-x1)*(y0-y2) - (x0-x2)*(y0-y1))
    return vol

# Nodes get the tags of their faces (maximum ones).
def getNodesFacesTags(faces, id1stBorrowFaces, nbNodes):
    sz = len(faces)
    tags = -np.ones(nbNodes, dtype=np.int32)

    for i in range(sz):
        if faces[i][4] != -1:
            tags[faces[i][0]] = faces[i][4]
            tags[faces[i][1]] = faces[i][4]
        else:
            break
    return tags

# Cells get the tags of the nodes around them (maximum ones).
def getCellsFacesTags(nodesFTags, nodesCellsId, nbCells):
    sz = len(nodesFTags)
    tags = -np.ones(nbCells, dtype=np.int32)

    for i in range(sz):
        if nodesFTags[i] != -1:
            for j in range(len(nodesCellsId[i])):
                tags[nodesCellsId[i][j]] = nodesFTags[i]

    return tags

# Compute faces normal (np.array, shape (nb faces, 3)).
# Face normal aren't normalized.
# They are set outward from faces[2] using the vector:
#   - faces Center - cells Center
def getFacesNormal2D(faces, nodes, cellsCenter):
    sz = len(faces)
    normal = np.zeros((sz, 3), dtype=np.float64)
    fCenter = np.zeros(3, dtype=np.float64)
    outward = np.zeros(3, dtype=np.float64)
    tmp = np.zeros(3, dtype=np.float64)

    for i in range(sz):
        tmp[0] =  nodes[faces[i][0]][1] - nodes[faces[i][1]][1]
        tmp[1] = -nodes[faces[i][0]][0] + nodes[faces[i][1]][0]

        fCenter[:] = (nodes[faces[i][0]][:] + nodes[faces[i][1]][:]) / 2.
        outward[:] = cellsCenter[faces[i][2]][:] - fCenter[:]
        if faces[i][5] == -1:
            outward = -outward

        if (outward[0] * tmp[0] + outward[1] * tmp[1]) > 0:
            normal[i][:] = -1 * tmp[:]
        else:
            normal[i][:] = tmp[:]
    return normal

# Compyte least square weight.
def lastSquaresWeight(nodes, nodesCellsId, nbNodesLoc, cellsCenter,
                      nbCellsLoc, faces):
    sz = len(nodes)
    Ixx = np.zeros(sz)
    Iyy = np.zeros(sz)
    Ixy = np.zeros(sz)
    Rx = np.zeros(sz)
    Ry = np.zeros(sz)
    nb = np.zeros(sz, dtype=np.int8)

    alpha = [l.copy() for l in nodesCellsId]
    for i in range(len(alpha)):
        for j in range(len(alpha[i])):
            alpha[i][j] = 0.

    # if nodes id < nbNodesLoc, it is an owned nodes. Thus the surrounding
    # cells are known.
    for i in range(nbNodesLoc):
        for j in range(len(nodesCellsId[i])):
            rx = cellsCenter[nodesCellsId[i][j]][0] - nodes[i][0]
            ry = cellsCenter[nodesCellsId[i][j]][1] - nodes[i][1]
            Ixx[i] += (rx * rx)
            Iyy[i] += (ry * ry)
            Ixy[i] += (rx * ry)
            Rx[i] += rx
            Ry[i] += ry
            nb[i] += 1

    # if nodes >= nbNodesLoc, we need to check if the node isn't on the
    # partition limits. Because we need those weight.
    # Just need to check if one cells id  is below nbCellsLoc.
    for i in range(nbNodesLoc, sz):
        isLimits = False
        for j in range(len(nodesCellsId[i])):
            if nodesCellsId[i][j] < nbCellsLoc:
                isLimits = True
        if isLimits:
            for j in range(len(nodesCellsId[i])):
                rx = cellsCenter[nodesCellsId[i][j]][0] - nodes[i][0]
                ry = cellsCenter[nodesCellsId[i][j]][1] - nodes[i][1]
                Ixx[i] += (rx * rx)
                Iyy[i] += (ry * ry)
                Ixy[i] += (rx * ry)
                Rx[i] += rx
                Ry[i] += ry
                nb[i] += 1

    # Remove nodes from border from the least square.
    for i in range(len(faces)):
        if faces[i][4] != -1:
            nb[faces[i][0]] = 0
            nb[faces[i][1]] = 0

    # Compute the weight
    for i in range(sz):
        if nb[i] != 0:
            D = Ixx[i]*Iyy[i] - Ixy[i]*Ixy[i]
            lambdaX = (Ixy[i]*Ry[i] - Iyy[i]*Rx[i]) / D
            lambdaY = (Ixy[i]*Rx[i] - Ixx[i]*Ry[i]) / D
            for j in range(len(nodesCellsId[i])):
                rx = cellsCenter[nodesCellsId[i][j]][0] - nodes[i][0]
                ry = cellsCenter[nodesCellsId[i][j]][1] - nodes[i][1]
                coeff = (1 + lambdaX*rx + lambdaY*ry)\
                        / (nb[i] + lambdaX*Rx[i] + lambdaY*Ry[i])
                alpha[i][j] = coeff

    return alpha

# Compute min and max normal L2 norm.
# Use facesNormal for simplicity.
def minMaxFacesLength(COMM, facesNormal):
    x = facesNormal[0][0]
    y = facesNormal[0][1]
    z = facesNormal[0][2]
    nrm = np.sqrt(x**2 + y**2 + z**2)

    hma = nrm
    hmi = nrm
    for i in range(len(facesNormal)):
        x = facesNormal[i][0]
        y = facesNormal[i][1]
        z = facesNormal[i][2]

        nrm = np.sqrt(x**2 + y**2 + z**2)
        if nrm < hmi:
            hmi = nrm
        if nrm > hma:
            hma = nrm

    hmi = COMM.allreduce(hmi, op=MPI.MIN)
    hma = COMM.allreduce(hma, op=MPI.MAX)
    return hmi, hma

################################################################################
#                                           Numbering                          #
################################################################################

# Give a MPI aware local numbering from a global one.
# Return 2 array:
#   - lstLocSizes is of size SIZE
#   - lstLocSizes[RANK] is the fist id used by a given process
#   - locId[i] is the new numbering of the i-th object
def globToLocNumbering(SIZE, RANK, COMM, globId, nbLocObj):
    # Get list of all local size.
    lstLocSizes = np.zeros(SIZE+1, np.int32)
    lstLocSizes[RANK+1] = nbLocObj
    lstLocSizes = COMM.allreduce(lstLocSizes, op=MPI.SUM)
    lstLocSizes = lstLocSizes.cumsum().astype(np.int32)

    # Share in a vector the local position to sync them
    syncId = np.zeros(lstLocSizes[-1], np.int32)
    for i in range(nbLocObj):
        syncId[globId[i]] = lstLocSizes[RANK]+i
    syncId = COMM.allreduce(syncId, op=MPI.SUM)
    locId = syncId[globId]

    return lstLocSizes, locId

# Return:
#   - the number of faces owned by the MPI process.
#   - the number of shared faces owned by the current MPI process.
def facesNbOwnershipShared(SIZE, RANK, COMM, faces, cellsGlobId,
                           nbInnerFaces, nbSharedFaces):
    nbShared = 0
    nbOwned = nbInnerFaces
    for i in range(nbInnerFaces, nbInnerFaces+nbSharedFaces):
        nL = cellsGlobId[faces[i][2]]
        nR = cellsGlobId[faces[i][3]]
        if nL > nR:
            nbOwned += 1
            nbShared += 1
    return nbOwned, nbShared

# Reduce all owned size in an array.
def facesSizesArray(SIZE, RANK, COMM, nbOwned):
    ownedSizes = np.zeros(SIZE+1, dtype=np.int32)
    ownedSizes[RANK+1] = nbOwned
    ownedSizes =  COMM.allreduce(ownedSizes, op=MPI.SUM)
    ownedSizes = ownedSizes.cumsum()
    return ownedSizes

# Setup faces numbering for MPI usage.
def facesLocFacesNum(SIZE, RANK, COMM, faces, cellsGlobId, ownedSizes,
            nbFacesToSync, nbInnerFaces, nbSharedFaces):
    # Sync all sizes
    nbSyncArray = np.zeros(SIZE+1, dtype=np.int32)
    nbSyncArray[RANK+1] = nbFacesToSync
    nbSyncArray =  COMM.allreduce(nbSyncArray, op=MPI.SUM)
    nbSyncArray = nbSyncArray.cumsum()

    # Shared nodes of shared faces
    pos = 0
    facesComm = np.zeros((nbSyncArray[-1], 3), dtype=np.int32)
    for i in range(nbInnerFaces, nbInnerFaces+nbSharedFaces):
        nL = cellsGlobId[faces[i][2]]
        nR = cellsGlobId[faces[i][3]]
        if nL > nR:
            facesComm[nbSyncArray[RANK]+pos][0] = \
                    ownedSizes[RANK] + nbInnerFaces + pos
            facesComm[nbSyncArray[RANK]+pos][1] = nL
            facesComm[nbSyncArray[RANK]+pos][2] = nR
            pos += 1
    facesComm = COMM.allreduce(facesComm, op=MPI.SUM)

    # Set a new id for faces
    pos = 0
    facesCalcId = -np.ones(nbInnerFaces + nbSharedFaces, dtype=np.int32)
    for i in range(nbInnerFaces):
        facesCalcId[i] = pos + ownedSizes[RANK]
        pos += 1
    # Get shared id on shared faces
    for i in range(nbSharedFaces):
        nL = cellsGlobId[faces[i + nbInnerFaces][2]]
        nR = cellsGlobId[faces[i + nbInnerFaces][3]]
        if nL > nR:
            assert(facesCalcId[i+nbInnerFaces] == -1)
            facesCalcId[i+nbInnerFaces] = pos + ownedSizes[RANK]
            pos += 1
        else:
            for j in range(len(facesComm)):
                if facesComm[j][1] == nR and facesComm[j][2] == nL:
                    assert(facesCalcId[i+nbInnerFaces] == -1)
                    facesCalcId[i+nbInnerFaces] = facesComm[j][0]
    return facesCalcId

# Compute faces numbering to be consistent between different MPI executions.
def facesGlobFacesNum(SIZE, RANK, COMM,
            faces, nodesGlobId, cellsGlobId, ownedSizes,
            nbInnerFaces, nbSharedFaces):

    # In an array, put global nA and nB for all faces
    pos = 0
    shareAll = np.zeros((ownedSizes[SIZE], 2))
    for i in range(nbInnerFaces):
        shareAll[pos + ownedSizes[RANK]][0] = nodesGlobId[faces[i][0]]
        shareAll[pos + ownedSizes[RANK]][1] = nodesGlobId[faces[i][1]]
        pos += 1
    for i in range(nbInnerFaces, nbInnerFaces + nbSharedFaces):
        nL = cellsGlobId[faces[i][2]]
        nR = cellsGlobId[faces[i][3]]
        if nL > nR:
            shareAll[pos + ownedSizes[RANK]][0] = nodesGlobId[faces[i][0]]
            shareAll[pos + ownedSizes[RANK]][1] = nodesGlobId[faces[i][1]]
            pos += 1
    shareAll = COMM.allreduce(shareAll, op=MPI.SUM)

    # Use tuple to sort them and shared them in a new array
    sortedAll = np.zeros((ownedSizes[SIZE], 2))
    if RANK == 0:
        fNodes = []
        for i in range(ownedSizes[SIZE]):
            nA = shareAll[i][0]
            nB = shareAll[i][1]
            if nA < nB:
                fNodes.append((nA, nB))
            else:
                fNodes.append((nB, nA))
        fNodes.sort()

        for i in range(ownedSizes[SIZE]):
            sortedAll[i][0] = fNodes[i][0]
            sortedAll[i][1] = fNodes[i][1]
    sortedAll = COMM.allreduce(sortedAll, op=MPI.SUM)

    # Transfer of values form sortedAll to facesGlobId
    def findId(A, B):
        if A > B:
            A, B = B, A
        for i in range(len(sortedAll)):
            if sortedAll[i][0] == A and sortedAll[i][1] == B:
                return i
        assert(False)
    facesGlobId = -np.ones(len(faces))
    for i in range(len(faces)):
        A = nodesGlobId[faces[i][0]]
        B = nodesGlobId[faces[i][1]]
        fId = findId(A, B)
        facesGlobId[i] = fId
    return facesGlobId

################################################################################
#                                           Diamond                            #
################################################################################

# Compute volume and normal used in diamond scheme
# Orientation:
#   - X: R-L
#   - Y: B-A
# Should follow global synced orientation when needed.
#
# vol[i]: diamond volume of the faces i
# normals[i]: diamond normal of the faces i (array of size 3)
def getInnerDiamond(faces, nodes, cellsCenter):
    sz = len(faces)
    vol = np.zeros(sz, dtype=np.float64)
    normal = np.zeros((sz, 3), dtype=np.float64)
    vecs = np.zeros((2, 3), dtype=np.float64)
    tmp = np.zeros(3, dtype=np.float64)

    for i in range(sz):
        if faces[i][3] != -1:
            vecs[0] = nodes[faces[i][1]] - nodes[faces[i][0]]
            vecs[1] = cellsCenter[faces[i][3]] - cellsCenter[faces[i][2]]

            # Sync normals direction with shared faces
            if faces[i][5] == -1:
                vecs[1] = -vecs[1]

            # Check the right hand rule.
            tmpV = (vecs[1][0] * vecs[0][1] - vecs[1][1] * vecs[0][0]) / 2.
            tmp[0] = -vecs[1][1]
            tmp[1] =  vecs[1][0]
            tmp[2] = 0.0

            if tmpV < 0.0:
                vol[i] = -tmpV
                normal[i][:] = -tmp[:]
            else:
                vol[i] = tmpV
                normal[i][:] = tmp[:]
        else:
            fCenter = (nodes[faces[i][1]] + nodes[faces[i][0]]) / 2

            vecs[0] = nodes[faces[i][1]] - nodes[faces[i][0]]
            vecs[1] = fCenter - cellsCenter[faces[i][2]]

            # Sync normals direction with shared faces
            if faces[i][5] == -1:
                vecs[1] = -vecs[1]

            # Check the right hand rule.
            tmpV = (vecs[1][0] * vecs[0][1] - vecs[1][1] * vecs[0][0]) / 2.
            tmp[0] = -vecs[1][1]
            tmp[1] =  vecs[1][0]
            tmp[2] = 0.0

            if tmpV < 0.0:
                vol[i] = -tmpV
                normal[i][:] = -tmp[:]
            else:
                vol[i] = tmpV
                normal[i][:] = tmp[:]

    return vol, normal

################################################################################
#                                           Split diamond                      #
################################################################################

# Compute volume and normal used in splitted diamond scheme
# Orientation:
#   - X: R-F and F-L
#   - Y: B-A
# Should follow global synced orientation when needed.
#
# vol[i][0]: left half diamond volume of the faces i
# vol[i][1]: right half diamond volume of the faces i
#
# normals[i][0]: left half diamond normal of the faces i (array of size 3)
# normals[i][1]: right half diamond normal of the faces i (array of size 3)
def getSplitDiamond(faces, nodes, cellsCenter, nbDiamToCompute):
    vol = np.zeros((nbDiamToCompute, 2), dtype=np.float64)
    normals = np.zeros((nbDiamToCompute, 2, 3), dtype=np.float64)

    faceCenter = np.zeros(3, dtype=np.float64)
    vecs = np.zeros((2, 3), dtype=np.float64)
    BA = np.zeros(3, dtype=np.float64)
    nrmL = np.zeros(3, dtype=np.float64)
    nrmR = np.zeros(3, dtype=np.float64)

    for i in range(nbDiamToCompute):
        if faces[i][4] == -1:
            idA = faces[i][0]
            idB = faces[i][1]
            faceCenter[:] = (nodes[idA][:] + nodes[idB][:]) / 2
            BA[:] = nodes[idB][:] - nodes[idA]

            # Sync normals direction with shared faces
            if faces[i][5] == 1:
                vecs[0][:] = faceCenter[:] - cellsCenter[faces[i][2]][:]
                vecs[1][:] = cellsCenter[faces[i][3]][:] - faceCenter[:]
            else:
                vecs[0][:] = - faceCenter[:] + cellsCenter[faces[i][2]][:]
                vecs[1][:] = - cellsCenter[faces[i][3]][:] + faceCenter[:]

            # Check the right hand rule.
            volL = (vecs[0][0] * BA[1] - vecs[0][1] * BA[0]) / 2
            volR = (vecs[1][0] * BA[1] - vecs[1][1] * BA[0]) / 2

            nrmL[0] = -vecs[0][1]
            nrmL[1] =  vecs[0][0]
            nrmR[0] = -vecs[1][1]
            nrmR[1] =  vecs[1][0]

            if volL < 0.0:
                vol[i][0] = - volL
                vol[i][1] = - volR
                normals[i][0][:] = -nrmL[:]
                normals[i][1][:] = -nrmR[:]
            else:
                vol[i][0] = volL
                vol[i][1] = volR
                normals[i][0][:] = nrmL[:]
                normals[i][1][:] = nrmR[:]

    return vol, normals
