import numpy as np
from numba import jit, njit

# Get the matrix allowing to pass from cells to nodes.
def getCellsToNodesCoeff(row, col, coeff, nodesCellsId, nbNodesLoc,
                         weight, cellsCalcId, nodesCalcId):
    size = 0
    for i in range(nbNodesLoc):
        for j in range(len(nodesCellsId[i])):
            if weight[i][j] != 0.:
                idC = cellsCalcId[nodesCellsId[i][j]]
                idN = nodesCalcId[i]
                for k in range(3):
                    row[size] = 3*idN +k
                    col[size] = 3*idC +k
                    coeff[size] = weight[i][j]
                    size += 1
    return size

################################################################################
#                                           Generics matrix                    #
################################################################################

# SubFunction to write 1. * vol on the diagonal.
@njit
def cellsId3(row, col, coeff, size, pos, vol):
    pos = 3*pos
    for l in range(3):
        row[size] = pos+l
        col[size] = pos+l
        coeff[size] = vol
        size += 1
    return 3

# SubFunction to write 1. on the diagonal.
@njit
def cellsOneOnDiag(row, col, coeff, size, pos, vol):
    pos = 3*pos
    for l in range(3):
        row[size] = pos+l
        col[size] = pos+l
        coeff[size] = 1.
        size += 1
    return 3

# SubFunction to compute the curl.
# 3 by 3 matrix for curl coeff
#         0 -a3  a2
# a x =  a3   0 -a1
#       -a2  a1   0
@njit
def edgesCurl(row, col, coeff, rowId, colId, size, n, v, alpha):
    row[size] = 3*rowId
    col[size] = 3*colId +1
    coeff[size] = -alpha * n[2] / v
    size += 1
    row[size] = 3*rowId
    col[size] = 3*colId +2
    coeff[size] = alpha * n[1] / v
    size += 1

    row[size] = 3*rowId +1
    col[size] = 3*colId
    coeff[size] = alpha * n[2] / v
    size += 1
    row[size] = 3*rowId +1
    col[size] = 3*colId +2
    coeff[size] = -alpha * n[0] / v
    size += 1

    row[size] = 3*rowId +2
    col[size] = 3*colId
    coeff[size] = -alpha * n[1] / v
    size += 1
    row[size] = 3*rowId +2
    col[size] = 3*colId +1
    coeff[size] = alpha * n[0] / v
    size += 1

    return 6

### Curl matrix
#     a1
# a = a2
#     a3
#         0 -a3  a2                      0   0  a2
# a x =  a3   0 -a1   donc en 2D on a:   0   0 -a1
#       -a2  a1   0                    -a2  a1   0
#                        -a2b2  a2b1          0
# (a x b) x en 2D donne:  a1b2 -a1b1          0
#                            0     0 -a2b2-a1b1
# 3 by 3 matrix for curl curl coeff in 2D (z = 0)
@njit
def curlCurl(row, col, coeff, rowId, colId, size,
              nA, nB, vC, vD, alpha,
              useless1, useless2, useless3):

    v = 2 * vD
    row[size] = 3*rowId
    col[size] = 3*colId
    coeff[size] = -alpha * nA[1] * nB[1] / v
    size += 1
    row[size] = 3*rowId
    col[size] = 3*colId +1
    coeff[size] =  alpha * nA[1] * nB[0] / v
    size += 1
    row[size] = 3*rowId +1
    col[size] = 3*colId
    coeff[size] =  alpha * nA[0] * nB[1] / v
    size += 1
    row[size] = 3*rowId +1
    col[size] = 3*colId +1
    coeff[size] = -alpha * nA[0] * nB[0] / v
    size += 1
    row[size] = 3*rowId +2
    col[size] = 3*colId +2
    coeff[size] = -alpha * (nA[0] * nB[0] + nA[1] * nB[1]) / v
    size += 1

    return 5

# 3 by 3 matrix for curl curl coeff in 2D (z = 0)
@njit
def curlRhoCurl(row, col, coeff, rowId, colId, size,
              nA, nB, vC, vD, alpha,
              useless1, useless2, useless3):

    rhoAir = (useless3[3*useless1] + useless3[3*useless2])/2

    v = 2 * vD
    row[size] = 3*rowId
    col[size] = 3*colId
    coeff[size] = -rhoAir * alpha * nA[1] * nB[1] / v
    size += 1
    row[size] = 3*rowId
    col[size] = 3*colId +1
    coeff[size] =  rhoAir * alpha * nA[1] * nB[0] / v
    size += 1
    row[size] = 3*rowId +1
    col[size] = 3*colId
    coeff[size] =  rhoAir * alpha * nA[0] * nB[1] / v
    size += 1
    row[size] = 3*rowId +1
    col[size] = 3*colId +1
    coeff[size] = -rhoAir * alpha * nA[0] * nB[0] / v
    size += 1
    row[size] = 3*rowId +2
    col[size] = 3*colId +2
    coeff[size] = -rhoAir * alpha * (nA[0] * nB[0] + nA[1] * nB[1]) / v
    size += 1

    return 5

# 3 by 3 matrix for curl curl coeff in 2D (z = 0)
# Cant @njit because of the min list
def curlRhoCurlEps(row, col, coeff, rowId, colId, size,
                   nA, nB, vC, vD, alpha,
                   useless1, useless2, useless3):

    rhoAir = (useless3[3*useless1] + useless3[3*useless2])/2
    minFact = np.min(np.abs([
        nA[1] * nB[1],
        nA[1] * nB[0],
        nA[0] * nB[1],
        nA[0] * nB[0],
        nA[0] * nB[0] + nA[1] * nB[1]
        ]))
    eps = 1e-5 * minFact

    v = 2 * vD
    row[size] = 3*rowId
    col[size] = 3*colId
    coeff[size] = -rhoAir * alpha * (nA[1] * nB[1] + eps) / v
    size += 1
    row[size] = 3*rowId
    col[size] = 3*colId +1
    coeff[size] =  rhoAir * alpha * nA[1] * nB[0] / v
    size += 1
    row[size] = 3*rowId +1
    col[size] = 3*colId
    coeff[size] =  rhoAir * alpha * nA[0] * nB[1] / v
    size += 1
    row[size] = 3*rowId +1
    col[size] = 3*colId +1
    coeff[size] = -rhoAir * alpha * (nA[0] * nB[0] + eps) / v
    size += 1
    row[size] = 3*rowId +2
    col[size] = 3*colId +2
    coeff[size] = -rhoAir * alpha * (nA[0] * nB[0] + nA[1] * nB[1] + eps) / v
    size += 1

    # print("RowId: %d, ColId: %d, RhoAir: %.15f" %(rowId, colId, rhoAir))
    # print("Val: ", coeff[size-5:size])

    return 5

# Stripped version of curlRhoCurl
@njit
def curlRhoCurlStrip(row, col, coeff, rowId, colId, size,
                     nA, nB, vC, vD, alpha,
                     p1, p2, wght):

    w = (wght[3*p1] + wght[3*p2])/2
    v = 2 * vD

    row[size] = 3*rowId +2
    col[size] = 3*colId +2
    coeff[size] = -w * alpha * (nA[0] * nB[0] + nA[1] * nB[1]) / v
    size += 1

    row[size] = 3*rowId
    col[size] = 3*colId
    coeff[size] = 1.
    size += 1

    row[size] = 3*rowId +1
    col[size] = 3*colId +1
    coeff[size] = 1.
    size += 1
    return 3

# Apply fun on cells
def ExCells(row, col, coeff, idCells, vCells, fun):
    size = 0
    for i in idCells:
        vC = vCells[i]
        size += fun(row, col, coeff, size, i, vC)
    return size

# Apply fun on cells edges
def ExCellsEdges(row, col, coeff, idFaces, faces,
              nFaces, vCells, nodesCellsId, lsAlpha,
              fun):
    size = 0

    # Faces owned
    for idF in idFaces[0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        vL = vCells[L]
        vR = vCells[R]

        for j in range(len(lsAlpha[A])):
            idC = nodesCellsId[A][j]
            alpha = lsAlpha[A][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL,  alpha / 2)
            size += fun(row, col, coeff,
                        R, idC, size, nF, vR, -alpha / 2)
        for j in range(len(lsAlpha[B])):
            idC = nodesCellsId[B][j]
            alpha = lsAlpha[B][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL, alpha / 2)
            size += fun(row, col, coeff,
                        R, idC, size, nF, vR, -alpha / 2)

    # Faces shared
    for idF in idFaces[1]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        vL = vCells[L]

        if f[5] == -1:
            sig = -1.
        else:
            sig =  1.

        for j in range(len(lsAlpha[A])):
            idC = nodesCellsId[A][j]
            alpha = lsAlpha[A][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL, sig*alpha / 2)
        for j in range(len(lsAlpha[B])):
            idC = nodesCellsId[B][j]
            alpha = lsAlpha[B][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL, sig*alpha / 2)

    return size

# Apply fun on cells edges
def ExCellsEdgesLR(row, col, coeff, idFaces, faces,
              nFaces, vCells, nodesCellsId, lsAlpha,
              fun):
    size = 0

    # Owned Left
    for idF in idFaces[0][0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        vL = vCells[L]
        vR = vCells[R]

        for j in range(len(lsAlpha[A])):
            idC = nodesCellsId[A][j]
            alpha = lsAlpha[A][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL,  alpha / 2)
        for j in range(len(lsAlpha[B])):
            idC = nodesCellsId[B][j]
            alpha = lsAlpha[B][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL, alpha / 2)
    # Shared Left
    for idF in idFaces[0][1]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        vL = vCells[L]

        if f[5] == -1:
            sig = -1.
        else:
            sig =  1.

        for j in range(len(lsAlpha[A])):
            idC = nodesCellsId[A][j]
            alpha = lsAlpha[A][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL, sig*alpha / 2)
        for j in range(len(lsAlpha[B])):
            idC = nodesCellsId[B][j]
            alpha = lsAlpha[B][j]
            size += fun(row, col, coeff,
                        L, idC, size, nF, vL, sig*alpha / 2)

    # Owned Right
    for idF in idFaces[1][0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        vL = vCells[L]
        vR = vCells[R]

        for j in range(len(lsAlpha[A])):
            idC = nodesCellsId[A][j]
            alpha = lsAlpha[A][j]
            size += fun(row, col, coeff,
                        R, idC, size, nF, vR, -alpha / 2)
        for j in range(len(lsAlpha[B])):
            idC = nodesCellsId[B][j]
            alpha = lsAlpha[B][j]
            size += fun(row, col, coeff,
                        R, idC, size, nF, vR, -alpha / 2)

    # No shared Right
    return size

# Apply fun on diamond
def ExDiamond(row, col, coeff, idFaces, faces,
              nFaces, nDiam, vDiam, vCells,
              nodesCellsId, lsAlpha,
              fun, funArgs):
    size = 0

    # Faces owned
    for idF in idFaces[0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        nD = nDiam[idF]
        vD = vDiam[idF]
        vL = vCells[L]
        vR = vCells[R]

        size += fun(row, col, coeff, L, R, size,
                    nF, nF, vL, vD,  1., L, R, funArgs)
        size += fun(row, col, coeff, L, L, size,
                    nF, nF, vL, vD, -1., L, R, funArgs)

        size += fun(row, col, coeff, R, R, size,
                    nF, nF, vR, vD, -1., L, R, funArgs)
        size += fun(row, col, coeff, R, L, size,
                    nF, nF, vR, vD,  1., L, R, funArgs)

        for j in range(len(lsAlpha[A])):
            idN = nodesCellsId[A][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD, -lsAlpha[A][j],
                        L, R, funArgs)
            size += fun(row, col, coeff, R, idN, size,
                        nF, nD, vR, vD,  lsAlpha[A][j],
                        L, R, funArgs)

        for j in range(len(lsAlpha[B])):
            idN = nodesCellsId[B][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD,  lsAlpha[B][j],
                        L, R, funArgs)
            size += fun(row, col, coeff, R, idN, size,
                        nF, nD, vR, vD, -lsAlpha[B][j],
                        L, R, funArgs)

    # Faces shared
    for idF in idFaces[1]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        nD = nDiam[idF]
        vD = vDiam[idF]
        vL = vCells[L]

        if f[5] == -1:
            sig = -1.
        else:
            sig =  1.

        size += fun(row, col, coeff, L, R, size,
                    nF, nF, vL, vD,  1., L, R, funArgs)
        size += fun(row, col, coeff, L, L, size,
                    nF, nF, vL, vD, -1., L, R, funArgs)

        for j in range(len(lsAlpha[A])):
            idN = nodesCellsId[A][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD, - sig*lsAlpha[A][j],
                        L, R, funArgs)

        for j in range(len(lsAlpha[B])):
            idN = nodesCellsId[B][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD,   sig*lsAlpha[B][j],
                        L, R, funArgs)

    return size

# Apply fun on diamond
def ExDiamondLR(row, col, coeff, idFaces, faces,
              nFaces, nDiam, vDiam, vCells,
              nodesCellsId, lsAlpha,
              fun, funArgs):
    size = 0

    # Owned Left
    for idF in idFaces[0][0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        nD = nDiam[idF]
        vD = vDiam[idF]
        vL = vCells[L]
        vR = vCells[R]

        size += fun(row, col, coeff, L, R, size,
                    nF, nF, vL, vD,  1., L, R, funArgs)
        size += fun(row, col, coeff, L, L, size,
                    nF, nF, vL, vD, -1., L, R, funArgs)

        for j in range(len(lsAlpha[A])):
            idN = nodesCellsId[A][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD, -lsAlpha[A][j],
                        L, R, funArgs)
        for j in range(len(lsAlpha[B])):
            idN = nodesCellsId[B][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD,  lsAlpha[B][j],
                        L, R, funArgs)

    # Shared Left
    for idF in idFaces[0][1]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        nD = nDiam[idF]
        vD = vDiam[idF]
        vL = vCells[L]
        vR = vCells[R]

        if f[5] == -1:
            sig = -1.
        else:
            sig =  1.

        size += fun(row, col, coeff, L, R, size,
                    nF, nF, vL, vD,  1., L, R, funArgs)
        size += fun(row, col, coeff, L, L, size,
                    nF, nF, vL, vD, -1., L, R, funArgs)

        for j in range(len(lsAlpha[A])):
            idN = nodesCellsId[A][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD, - sig*lsAlpha[A][j],
                        L, R, funArgs)
        for j in range(len(lsAlpha[B])):
            idN = nodesCellsId[B][j]
            size += fun(row, col, coeff, L, idN, size,
                        nF, nD, vL, vD,   sig*lsAlpha[B][j],
                        L, R, funArgs)

    # Owned Right
    for idF in idFaces[1][0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        nF = nFaces[idF]
        nD = nDiam[idF]
        vD = vDiam[idF]
        vL = vCells[L]
        vR = vCells[R]

        size += fun(row, col, coeff, R, R, size,
                    nF, nF, vR, vD, -1., L, R, funArgs)
        size += fun(row, col, coeff, R, L, size,
                    nF, nF, vR, vD,  1., L, R, funArgs)

        for j in range(len(lsAlpha[A])):
            idN = nodesCellsId[A][j]
            size += fun(row, col, coeff, R, idN, size,
                        nF, nD, vR, vD,  lsAlpha[A][j],
                        L, R, funArgs)
        for j in range(len(lsAlpha[B])):
            idN = nodesCellsId[B][j]
            size += fun(row, col, coeff, R, idN, size,
                        nF, nD, vR, vD, -lsAlpha[B][j],
                        L, R, funArgs)

    # No shared Right
    return size

# Apply fun on diamond
# 4 matrix will be build tagged with [[1, 2], [3, 4]].
def ExSplitLR(row, col, coeff, tag, idFaces, faces,
              nFaces, nDiam, nSptDiam,
              vDiam, vSptDiam, vCells,
              nodesCellsId, lsAlpha,
              fun, funArgs):
    size = 0

    # Owned Left
    for idF in idFaces[0][0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        F = idF

        nF = nFaces[idF]
        nD = nDiam[idF]
        nDL = nSptDiam[idF][0]
        nDR = nSptDiam[idF][1]

        vD = vDiam[idF]
        vL = vCells[L]
        vR = vCells[R]
        vDL = vSptDiam[idF][0]
        vDR = vSptDiam[idF][1]

        if tag == 2:
            size += fun(row, col, coeff, L, F, size,
                        nF, nF, vL, vDL,  1.,
                        L, L, funArgs)
        if tag == 4:
            size += fun(row, col, coeff, F, F, size,
                        nF, nF, 1., vDL,  1.,
                        L, L, funArgs)
        if tag == 1:
            size += fun(row, col, coeff, L, L, size,
                        nF, nF, vL, vDL, -1.,
                        L, L, funArgs)
            for j in range(len(lsAlpha[A])):
                idN = nodesCellsId[A][j]
                size += fun(row, col, coeff, L, idN, size,
                            nF, nDL, vL, vDL, -lsAlpha[A][j],
                            L, L, funArgs)
            for j in range(len(lsAlpha[B])):
                idN = nodesCellsId[B][j]
                size += fun(row, col, coeff, L, idN, size,
                            nF, nDL, vL, vDL,  lsAlpha[B][j],
                            L, L, funArgs)
        if tag == 3:
            size += fun(row, col, coeff, F, L, size,
                        nF, nF, 1., vDL, -1.,
                        L, L, funArgs)
            for j in range(len(lsAlpha[A])):
                idN = nodesCellsId[A][j]
                size += fun(row, col, coeff, F, idN, size,
                            nF, nDL, 1., vDL, -lsAlpha[A][j],
                            L, L, funArgs)
            for j in range(len(lsAlpha[B])):
                idN = nodesCellsId[B][j]
                size += fun(row, col, coeff, F, idN, size,
                            nF, nDL, 1., vDL,  lsAlpha[B][j],
                            L, L, funArgs)

    # Shared Left
    for idF in idFaces[0][1]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        F = idF

        nF = nFaces[idF]
        nD = nDiam[idF]
        nDL = nSptDiam[idF][0]

        vD = vDiam[idF]
        vL = vCells[L]
        vDL = vSptDiam[idF][0]

        if f[5] == -1:
            sig = -1.
        else:
            sig =  1.

        if tag == 2:
            size += fun(row, col, coeff, L, F, size,
                        nF, nF, vL, vDL,  1.,
                        L, L, funArgs)
        if tag == 4:
            size += fun(row, col, coeff, F, F, size,
                        nF, nF, 1., vDL, 1.,
                        L, L, funArgs)
        if tag == 1:
            size += fun(row, col, coeff, L, L, size,
                        nF, nF, vL, vDL, -1.,
                        L, L, funArgs)
            for j in range(len(lsAlpha[A])):
                idN = nodesCellsId[A][j]
                size += fun(row, col, coeff, L, idN, size,
                            nF, nDL, vL, vDL, - sig * lsAlpha[A][j],
                            L, L, funArgs)
            for j in range(len(lsAlpha[B])):
                idN = nodesCellsId[B][j]
                size += fun(row, col, coeff, L, idN, size,
                            nF, nDL, vL, vDL,  sig * lsAlpha[B][j],
                            L, L, funArgs)
        if tag == 3:
            size += fun(row, col, coeff, F, L, size,
                        nF, nF, 1., vDL, -1.,
                        L, L, funArgs)
            for j in range(len(lsAlpha[A])):
                idN = nodesCellsId[A][j]
                size += fun(row, col, coeff, F, idN, size,
                            nF, nDL, 1., vDL, -sig * lsAlpha[A][j],
                            L, L, funArgs)
            for j in range(len(lsAlpha[B])):
                idN = nodesCellsId[B][j]
                size += fun(row, col, coeff, F, idN, size,
                            nF, nDL, 1., vDL,  sig * lsAlpha[B][j],
                            L, L, funArgs)

    # Owned Right
    for idF in idFaces[1][0]:
        f = faces[idF]
        A = f[0]
        B = f[1]
        L = f[2]
        R = f[3]
        F = idF

        nF = nFaces[idF]
        nD = nDiam[idF]
        nDL = nSptDiam[idF][0]
        nDR = nSptDiam[idF][1]

        vD = vDiam[idF]
        vL = vCells[L]
        vR = vCells[R]
        vDL = vSptDiam[idF][0]
        vDR = vSptDiam[idF][1]

        if tag == 2:
            size += fun(row, col, coeff, R, F, size,
                        nF, nF, vR, vDR,  1.,
                        R, R, funArgs)
        if tag == 4:
            size += fun(row, col, coeff, F, F, size,
                        nF, nF, 1., vDR,  1.,
                        R, R, funArgs)
        if tag == 1:
            size += fun(row, col, coeff, R, R, size,
                        nF, nF, vR, vDR, -1.,
                        R, R, funArgs)
            for j in range(len(lsAlpha[A])):
                idN = nodesCellsId[A][j]
                size += fun(row, col, coeff, R, idN, size,
                            nF, nDR, vR, vDR,  lsAlpha[A][j],
                            R, R, funArgs)
            for j in range(len(lsAlpha[B])):
                idN = nodesCellsId[B][j]
                size += fun(row, col, coeff, R, idN, size,
                            nF, nDR, vR, vDR, -lsAlpha[B][j],
                            R, R, funArgs)
        if tag == 3:
            size += fun(row, col, coeff, F, R, size,
                        nF, nF, 1., vDR, -1.,
                        R, R, funArgs)
            for j in range(len(lsAlpha[A])):
                idN = nodesCellsId[A][j]
                size += fun(row, col, coeff, F, idN, size,
                            nF, nDR, 1., vDR,  lsAlpha[A][j],
                            R, R, funArgs)
            for j in range(len(lsAlpha[B])):
                idN = nodesCellsId[B][j]
                size += fun(row, col, coeff, F, idN, size,
                            nF, nDR, 1., vDR, -lsAlpha[B][j],
                            R, R, funArgs)

    # No shared Right
    return size
