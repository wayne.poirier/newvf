import sys
import gmsh
from math import cos, sin, pi

unit = 10.
scl  =  0.5

rIn  = 0.1  * unit
hIn  = 0.05 * unit * scl

rOut = 1.   * unit
hOut = 0.4  * unit * scl

def addCircle(ctrPts, ctrTags, radius, mshSz):
    pts = []
    crv = []

    pts.append(gmsh.model.geo.addPoint( radius + ctrPts[0],           ctrPts[1], 0., mshSz))
    pts.append(gmsh.model.geo.addPoint(          ctrPts[0], -radius + ctrPts[1], 0., mshSz))
    pts.append(gmsh.model.geo.addPoint(-radius + ctrPts[0],           ctrPts[1], 0., mshSz))
    pts.append(gmsh.model.geo.addPoint(          ctrPts[0],  radius + ctrPts[1], 0., mshSz))

    crv.append(gmsh.model.geo.addCircleArc(pts[0], ctrTags, pts[1]))
    crv.append(gmsh.model.geo.addCircleArc(pts[1], ctrTags, pts[2]))
    crv.append(gmsh.model.geo.addCircleArc(pts[2], ctrTags, pts[3]))
    crv.append(gmsh.model.geo.addCircleArc(pts[0], ctrTags, pts[3]))

    clc = gmsh.model.geo.addCurveLoop([crv[0], crv[1], crv[2], -crv[3]])

    return (pts, crv, clc)

################################################################################
#                                                Mesh Gen                      #
################################################################################
gmsh.initialize()
gmsh.model.add("circle")

# Mesh
p0 = gmsh.model.geo.addPoint( 0.,  0., 0., hIn)
cIn  = addCircle((0., 0.), p0,  rIn,  hIn)
cOut = addCircle((0., 0.), p0, rOut, hOut)

sIn  = gmsh.model.geo.addPlaneSurface(         [cIn[2]])
sOut = gmsh.model.geo.addPlaneSurface([cOut[2], cIn[2]])

# Physical
gmsh.model.geo.addPhysicalGroup(1, [cOut[1][0]], 1001)
gmsh.model.geo.addPhysicalGroup(1, [cOut[1][1]], 1002)
gmsh.model.geo.addPhysicalGroup(1, [cOut[1][2]], 1003)
gmsh.model.geo.addPhysicalGroup(1, [cOut[1][3]], 1004)
gmsh.model.geo.addPhysicalGroup(2,  [sIn], 100001)
gmsh.model.geo.addPhysicalGroup(2, [sOut], 100002)

gmsh.model.geo.synchronize()
gmsh.model.mesh.generate(2)
gmsh.write("circle.msh")

if '-nopopup' not in sys.argv:
    gmsh.fltk.run()

gmsh.finalize()
