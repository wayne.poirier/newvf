from petsc4py import PETSc

# Write Vec content in a file
def fileViewPetscObj(pObj, fileName):
    pObj.view(PETSc.Viewer().createASCII(fileName))

################################################################################
#                                           Vector interface                   #
################################################################################

# Create an MPI Vec with a local size, a scale for it and a blockSize.
def createVec(sz, scale, blockSize):
    sizes = (scale * sz, PETSc.DECIDE)

    # Init Mpi vec with size and blockSize
    v = PETSc.Vec().create()
    v.setType('mpi')
    v.setBlockSize(blockSize)
    v.setSizes(sizes)
    v.setFromOptions()
    return v

# Set the value of a Vec v for a list of values (val) and position (pos).
# addRule is a Boolean to add value at the same position.
# Also apply a mapping on row id.
def setValueVec(v, val, pos, rowMapping, sz, addRule=False):
    assert(len(pos) >= sz)
    assert(len(val) >= sz)
    for i in range(sz):
        v.setValue(rowMapping[pos[i]], val[i], addv=addRule)
    v.assemblyBegin()
    v.assemblyEnd()

# Set the value of a Vec v for a list of values (val) and position (pos)
# with a scale constant.
# addRule is a Boolean to add value at the same position.
# Also apply a mapping on row id.
def setScaleValueVec(v, val, pos, rowMapping, scale,
                     sz, addRule=False):
    assert(len(pos) >= sz)
    assert(len(val) >= sz)
    for i in range(sz):
        v.setValue(rowMapping[pos[i]], scale * val[i], addv=addRule)
    v.assemblyBegin()
    v.assemblyEnd()

# Set the value of a Vec v for a list of weighted values (val)
# and position (pos).
# addRule is a Boolean to add value at the same position.
# Also apply a mapping on row id.
def setWeightedValueVec(v, val, pos, rowMapping, weighted,
                        sz, addRule=False):
    assert(len(pos) >= sz)
    assert(len(val) >= sz)
    for i in range(sz):
        v.setValue(rowMapping[pos[i]], param[i] * val[i], addv=addRule)
    v.assemblyBegin()
    v.assemblyEnd()

# Return the vector value
def getArray(v):
    return v.getArray()

# Create an MPI Vec with a local size and Ghost value.
# It use scale for the size and a blockSize for vector structure.
def createGhostVec(sz, scale, blockSize, ghostArray):
    sizes = (scale * sz, PETSc.DECIDE)

    # Init Mpi vec with size and blockSize
    v = PETSc.Vec().create()
    v.setType('mpi')
    v.setBlockSize(blockSize)
    v.setSizes(sizes)
    v.setMPIGhost(ghostArray)
    v.setFromOptions()
    return v

# Sync a vector with Ghost setup.
def syncGhostVec(v):
    v.ghostUpdate()

def copy(src, dst):
    src.copy(dst)

# Return the array with ghost value at the end.
def getGhostArray(v):
    with v.localForm() as lf:
        array = lf.array
    return array

################################################################################
#                                           Matrix interface                   #
################################################################################

# Create an MPI Mat with a local sizes, a scale for the sizes and a blockSize.
# sz = (nb local row, nb local col)
# scale = (row scale, col scale)
def createMat(sz, scale, blockSize):
    size0 = (scale[0] * sz[0], PETSc.DECIDE)
    size1 = (scale[1] * sz[1], PETSc.DECIDE)

    # Init Mpi vec with size and blockSize
    m = PETSc.Mat().create()
    m.setType('mpiaij')
    m.setBlockSize(blockSize)
    m.setSizes((size0, size1))
    m.setFromOptions()
    return m

# Set the value of a Mat v for a list of values (val) and position
# (posI and posJ).
# addRule is a Boolean to add value at the same position.
# Also apply a mapping on row and col id.
def setValueMat(m, posI, posJ, val, rowMapping, colMapping, sz, addRule=False):
    assert(len(posI) >= sz)
    assert(len(posJ) >= sz)
    assert(len(val) >= sz)
    for i in range(sz):
        m.setValue(rowMapping[posI[i]], colMapping[posJ[i]], val[i], addv=addRule)
    m.assemblyBegin()
    m.assemblyEnd()

# Set the value of a Mat v for a list of values (val) to scale and position
# (posI and posJ).
# addRule is a Boolean to add value at the same position.
# Also apply a mapping on row and col id.
def setScaleValueMat(m, posI, posJ, val, rowMapping, colMapping,
                     scale, sz, addRule=False):
    assert(len(posI) >= sz)
    assert(len(posJ) >= sz)
    assert(len(val) >= sz)
    for i in range(sz):
        m.setValue(rowMapping[posI[i]], colMapping[posJ[i]],
                   scale * val[i], addv=addRule)
    m.assemblyBegin()
    m.assemblyEnd()

# Set the value of a Mat v for a list of weighted values (val) and position
# (posI and posJ).
# addRule is a Boolean to add value at the same position.
# Also apply a mapping on row and col id.
def setWeightedValueMat(m, posI, posJ, val, rowMapping, colMapping,
                        weight, sz, addRule=False):
    assert(len(posI) >= sz)
    assert(len(posJ) >= sz)
    assert(len(val) >= sz)
    for i in range(sz):
        m.setValue(rowMapping[posI[i]], colMapping[posJ[i]],
                   weight * val[i], addv=addRule)
    m.assemblyBegin()
    m.assemblyEnd()

# Set the value of a Mat v for a list of values (val) and position
# (posI and posJ).
# addRule is a Boolean to add value at the same position.
def setValueMatTags(m, posI, posJ, val, tags, sz, tagsId, addRule=False):
    assert(len(posI) >= sz)
    assert(len(posJ) >= sz)
    assert(len(val) >= sz)
    for i in range(sz):
        if tags[i] in tagsId:
            m.setValue(posI[i], posJ[i], val[i], addv=addRule)
    m.assemblyBegin()
    m.assemblyEnd()

# Add coeff to the matrix m, (row, col, val) must be under CSR format.
def setValueMatCSR(m, row, col, val, addRule=False):
    m.setValuesCSR(row, col, val, addv=addRule)
    m.assemblyBegin()
    m.assemblyEnd()

################################################################################
#                                           KSP interface                      #
################################################################################

# Create and return the linear solver.
def getLinearSolver(solverType, pcType, rTol, aTol, maxIt):
    ksp = PETSc.KSP().create()
    ksp.setType(solverType)
    ksp.setTolerances(rtol=rTol, atol=aTol, max_it=maxIt)
    pc = ksp.getPC()
    pc.setType(pcType)
    ksp.setFromOptions()
    return ksp

# Linear solver for Ax = b
def solveLin(solver, A, x, b):
    solver.setOperators(A)
    solver.solve(b, x)

################################################################################
#                                           Nested interface                   #
################################################################################

# Create an nested Matrix or Vector.
# objDim = 1 for vector and 2 for matrix.
def createNest(objDim, content):
    if objDim == 1:
        return PETSc.Vec().createNest(content)
    elif objDim == 2:
        return PETSc.Mat().createNest(content)
    else:
        raise ValueError('ObjDim incorrect 1 or 2')

# From a nested petsc object (Matrix/Vector) return the list of the components.
def getNestedElmt(petscObj, objDim):
    if objDim == 1:
        return petscObj.getNestSubVecs()
    elif objDim == 2:
        return petscObj.getNestSubMatrix()
    else:
        raise ValueError('ObjDim incorrect 1 or 2')
