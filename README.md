# newVF

Finite Volume library, work in progress.

## Config
We use python3, the following libraries need to be install manually:
- 'meshio<4'
- 'numpy<2' because of 'meshio<4'
- 'mgmetis'

Others can be bundle with PETSc with '--download-..' flags:
- 'petsc4py' the python binding
- an mpi compiler and it's python binding (could be removed if wanted)
- cblas and lapack
