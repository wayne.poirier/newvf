import pyximport
pyximport.install(language_level=3)

import os
import numpy as np
from mpi4py import MPI

import utilsFast as ut

################################################################################
#                                           Save matrix shape                  #
################################################################################

# In mat.pbm the header file
# In _mat + 'RANK' + .pbm the content of a given process
# Need to merge all file in order:
#     cat mat.pbm _mat* > img.pbm
# should merge them.
# Should not be used with large matrices, ouput file are large.
def viewRow3x3(m, nbCellsLoc, cellsCalcId, nbRow, nbCol, RANK):
    if not os.path.exists('img'):
        os.mkdir('img')

    if RANK == 0:
        hd = 'img/mat.pbm'
        f = open(hd, 'w')
        f.write('P1\n' + str(nbRow) + ' ' + str(nbCol) + '\n')
        f.close()

    if RANK < 10:
        ftmp = 'img/_mat00' + str(RANK) + '.pbm'
    elif RANK < 100:
        ftmp = 'img/_mat0' + str(RANK) + '.pbm'
    else:
        ftmp = 'img/_mat' + str(RANK) + '.pbm'
    frk = open(ftmp, 'w')
    for i in range(nbCellsLoc):
        line = ['0' for i in range(nbCol+1)]
        line[-1] = '\n'

        k = 3*cellsCalcId[i]
        for l in range(3):
            index = m.getRow(k+l)[0]
            for j in index:
                line[int(j/3)] = '1'
        frk.write(' '.join(line))
    frk.close()

################################################################################
#                                           Errors                             #
################################################################################

# Norm of the difference of 2 numpy array, work with MPI.
def diff(COMM, arrA, arrB, dim, nrm, weight=None):
    assert(len(arrA) == len(arrB))
    assert(dim > 0)
    assert(nrm > 0)

    size = int(arrA.size / dim)
    dff = np.abs(arrA - arrB)
    if dim != 1:
        dff = np.reshape(dff, (size, dim))
    if nrm != 1:
        dff = np.power(dff, nrm)
    if dim != 1:
        dff = np.sum(dff, axis=1)
    if type(weight) == np.ndarray:
        assert(len(weight) == size)
        dff = dff * weight
    else:
        assert(type(weight) == type(None))
    dff = np.sum(dff)
    dff = COMM.allreduce(dff, op=MPI.SUM)
    if nrm != 1:
        dff = np.power(dff, 1/nrm)
    return dff

# Return cells-wise difference for a given nrm.
def cellsDiff(COMM, arrA, arrB, dim, nrm):
    assert(len(arrA) == len(arrB))
    assert(dim > 0)
    assert(nrm > 0)

    size = int(arrA.size / dim)
    dff = np.abs(arrA - arrB)
    if dim != 1:
        dff = np.reshape(dff, (size, dim))
    if nrm != 1:
        dff = np.power(dff, nrm)
    if dim != 1:
        dff = np.sum(dff, axis=1)
    if nrm != 1:
        dff = np.power(dff, 1/nrm)
    return dff

################################################################################
#                                           Cpt vector val                     #
################################################################################

# Compute vector value and store it in value and position array.
def cptVecVal(val, pos, fun, points, nbPtsLoc, dim, time=(False, 0)):
    assert(len(val) == len(pos))
    assert(len(val) >= dim*nbPtsLoc)

    if time[0]:
        _fun = lambda p: fun(p, time[1])
    else:
        _fun = fun

    for i in range(nbPtsLoc):
        v = _fun(points[i])
        k = dim*i
        for j in range(dim):
            val[dim*i +j] = v[j]
            pos[dim*i +j] = k+j

    return dim*nbPtsLoc

# Compute vector for a given list of tags value and store it in value
# and position array.
def cptVecTagsVal(val, pos, fun, cellsFTags, tags, cellsCenter,
                  cellsVol, nbCellsLoc, dim, time=(False, 0)):
    nb = 0

    if time[0]:
        _fun = lambda p: fun(p, time[1])
    else:
        _fun = fun

    for i in range(nbCellsLoc):
        if cellsFTags[i] in tags:
            v = _fun(cellsCenter[i])
            k = dim*i
            if len(cellsVol) != 1:
                vol = cellsVol[i]
            else:
                vol = 1.
            for j in range(dim):
                val[nb] = vol * v[j]
                pos[nb] = k+j
                nb += 1

    return nb
################################################################################
#                                           IJ -> CSR                          #
################################################################################

# From a AIJ matrix coefficient format, return row and col array of the
# corresonding CSR format.
# It also return a zeroed array for CSR values.
def IJtoCSRZeros(posI, posJ, rowMap, colMap, sz, nbLineLoc):
    # Count occurence in a list of list
    lst = [[] for i in range(nbLineLoc)]
    for i in range(sz):
        pI = posI[i]
        pJ = posJ[i]
        if not pJ in lst[pI]:
            lst[pI].append(pJ)

    # Build row
    cumsum = 0
    row = np.zeros(nbLineLoc+1, np.int32)
    for i in range(nbLineLoc):
        cumsum += len(lst[i])
        row[i+1] = cumsum

    # Build col
    col = np.zeros(row[-1], np.int32)
    for i in range(nbLineLoc):
        idCol = lst[i]
        for j in range(len(idCol)):
            idCol[j] = colMap[idCol[j]]
        idCol.sort()
        offset = row[i]
        for j in range(len(idCol)):
            col[offset +j] = idCol[j]

    val = np.zeros(row[-1], np.float64)
    return row, col, val

# Add coeff of an AIJ matrix coefficient to and CSR one.
# DO NOT replace the value, add them.
def coeffIJtoCSR(posI, posJ, coeff, sz, rowCsr, colCsr, valCsr,
                 rowMap, colMap, rowOffset):
    assert(len(posI) >= sz)
    assert(len(posJ) >= sz)
    assert(len(coeff) >= sz)
    ut.coeffIJtoCSR(posI, posJ, coeff, rowCsr, colCsr, valCsr,
                 rowMap, colMap, sz, rowOffset)

# Add coeff of an AIJ matrix coefficient to and CSR one.
# DO NOT replace the value, add them.
# Also scale the values.
def coeffIJtoCSRScaled(posI, posJ, coeff, scale, sz, rowCsr, colCsr, valCsr,
                       rowMap, colMap, rowOffset):
    assert(len(posI) >= sz)
    assert(len(posJ) >= sz)
    assert(len(coeff) >= sz)
    ut.coeffIJtoCSRScaled(posI, posJ, coeff, scale, rowCsr, colCsr, valCsr,
                 rowMap, colMap, sz, rowOffset)

# Add coeff of an AIJ matrix coefficient to and CSR one.
# DO NOT replace the value, add them.
# Also weight the values.
def coeffIJtoCSRWeighted(posI, posJ, coeff, weight, scale, sz,
                         rowCsr, colCsr, valCsr,
                         rowMap, colMap, rowOffset):
    assert(len(posI) >= sz)
    assert(len(posJ) >= sz)
    assert(len(coeff) >= sz)
    assert(len(weight) == len(rowMap))
    ut.coeffIJtoCSRWeighted(posI, posJ, coeff, weight, scale,
                          rowCsr, colCsr, valCsr,
                          rowMap, colMap, sz, rowOffset)

# Return an approximated CSR skeleton for the diamond scheme.
def approxDiamondSchemeCSR(cellsToNodes, nodesToCells, cellsCalcId,
                           nbCellsLoc):
    row = np.zeros(nbCellsLoc+1, np.int32)
    colList = []

    for i in range(nbCellsLoc):
        colTmp = []
        assert(len(cellsToNodes[i]) == 3)
        for j in range(3):
            nodes = cellsToNodes[i][j]
            for k in range(len(nodesToCells[nodes])):
                cellsTmp = cellsCalcId[nodesToCells[nodes][k]]
                if not(cellsTmp in colTmp):
                    colTmp.append(cellsTmp)
        colTmp.sort()
        row[i+1] = row[i] + len(colTmp)
        for j in range(len(colTmp)):
            colList.append(colTmp[j])

    col = np.array(colList, np.int32)
    return row, col

# Scale up a base CSR
# From A[i, j] = b, b scalar, we scale to allow b matrix.
def scaleUpCSR(rowB, colB, dimRow, dimCol, rowScale, colScale):
    # Scale up row
    rowS = np.zeros(dimRow * len(rowB) - (dimRow -1), np.int32)
    ut.scaleUpCSRRow(rowB, rowS, dimRow, np.array(rowScale, np.int32))

    # colScale under numpy format
    maxLen = 0
    for c in colScale:
        maxLen = max(maxLen, len(c))
    npColScale = np.zeros((dimRow, maxLen+1), np.int32)

    for i in range(dimRow):
        npColScale[i][0] = len(colScale[i])+1
        for j in range(len(colScale[i])):
            npColScale[i][j+1] = colScale[i][j]

    # Scale column
    colS = np.zeros(rowS[-1], np.int32)
    ut.scaleUpCSRCol(rowB, colB, rowS, colS, npColScale, dimRow, dimCol)

    # Allocate array for value
    valS = np.zeros(rowS[-1], np.float64)

    return rowS, colS, valS

################################################################################
#                                           Ghost                              #
################################################################################

# From all id, return the list of foreign id scalled if needed.
def getForeignId(idArray, lastLocId, scale):
    array = scale * idArray[lastLocId:]
    sz = len(array)
    arrayScale = np.zeros((sz, scale), np.int32)
    for i in range(scale):
        arrayScale[:,i] = array +i
    arrayScale = np.reshape(arrayScale, sz*scale)
    return arrayScale
