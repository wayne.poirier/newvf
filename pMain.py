import os
import sys

from time import time, sleep

import petsc4py
petsc4py.init(sys.argv)

from numba import njit
from mpi4py import MPI

COMM = MPI.COMM_WORLD
SIZE = COMM.Get_size()
RANK = COMM.Get_rank()

import gmshMesh as msh
import numpy as np
import geometry as geo
import matBuild as mb
import utils as ut
import petscUtils as lag

if RANK == 0:
    if os.path.exists('results'):
        for file in os.listdir('results'):
            os.remove('results/' + file)
        os.removedirs('results')
        os.mkdir('results')
    else:
        os.mkdir('results')
    files = ['nrmH', 'nrmHSup', 'nrmHDiam', 'nrmHSplit', 'nrmHSmart',
             'pointDiam', 'pointSplit', 'pointSmart', 'nrmHSupra', 'nrmJSupra']
    for f in files:
        if os.path.isfile(f):
            os.remove(f)

debugMat = False
viewPart = False
verbose  =  True
saveMesh = False
saveNrm  = False
################################################################################
#                                           Mesh extended                      #
################################################################################
meshFile = 'order.msh'
meshFile = '2layer.msh'
meshFile = 'fil.msh'

# Read general mesh data
msh.gmshToHDF5(SIZE, RANK, COMM, meshFile)
cells, nodes, cellsGlobId, nodesGlobId,\
            nbCellsLoc, nbNodesLoc, edgesDom,\
            physCells, physEdgesDom = msh.readPartMesh('hdf5Mesh', RANK)
if verbose:
    print("Cells local (%d) sise %d and total size %d."
              %(RANK, nbCellsLoc, len(cells)))
    sys.stdout.flush()

# Build basic mesh info
geo.setLocalNodeId(cells, nodesGlobId, edgesDom)
nodesCellsId = geo.cellsAroundNodes(cells, len(nodes))
faces, nbInnerFaces, nbSharedFaces = geo.facesInfo(cells, nodes,
            edgesDom, physEdgesDom, nbCellsLoc, cellsGlobId)

# Build mesh info num methods
cellsCenter = geo.getCellsCenter(cells, nodes)
cellsVol = geo.getCellsVol(cells, nodes)
nodesFTags = geo.getNodesFacesTags(faces, nbInnerFaces + nbSharedFaces,
            len(nodes))
cellsFTags = geo.getCellsFacesTags(nodesFTags, nodesCellsId, len(cells))
facesNormal = geo.getFacesNormal2D(faces, nodes, cellsCenter)
leastSquareCoeff = geo.lastSquaresWeight(nodes, nodesCellsId, nbNodesLoc,
            cellsCenter, nbCellsLoc, faces)

# Diam info and splitted diam info
diamVol, diamNormal = geo.getInnerDiamond(faces, nodes, cellsCenter)
splitDVol, splitDNormal = geo.getSplitDiamond(faces, nodes,
            cellsCenter, nbInnerFaces+nbSharedFaces)

# Cells, Nodes and Faces numbering
allCellsSize, cellsCalcId = geo.globToLocNumbering(SIZE, RANK, COMM,
            cellsGlobId, nbCellsLoc)
allNodesSize, nodesCalcId = geo.globToLocNumbering(SIZE, RANK, COMM,
            nodesGlobId, nbNodesLoc)
nbCellsGlob = allCellsSize[SIZE]

# Compute the actual mapping (dim = 3 hard coded)
cellsMap = np.zeros((len(cells), 3), np.int32)
cellsMap[:,0] = 3*cellsCalcId[:]
cellsMap[:,1] = 3*cellsCalcId[:] +1
cellsMap[:,2] = 3*cellsCalcId[:] +2
cellsMap = np.reshape(cellsMap, 3*len(cells))

nodesMap = np.zeros((len(nodes), 3), np.int32)
nodesMap[:,0] = 3*nodesCalcId[:]
nodesMap[:,1] = 3*nodesCalcId[:] +1
nodesMap[:,2] = 3*nodesCalcId[:] +2
nodesMap = np.reshape(nodesMap, 3*len(nodes))

# facesMap = np.zeros((nbInnerFaces + nbSharedFaces, 3), np.int32)
# facesMap[:,0] = 3*facesCalcId[:]
# facesMap[:,1] = 3*facesCalcId[:] +1
# facesMap[:,2] = 3*facesCalcId[:] +2
# facesMap = np.reshape(facesMap, 3*(nbInnerFaces + nbSharedFaces))

### Get min h value
if verbose:
    hmi, hma = geo.minMaxFacesLength(COMM, facesNormal)
    if RANK == 0:
        print("-----------------------------------------")
        print("Min h value: %2.8f" %(hmi))
        print("Max h value: %2.8f" %(hma))

# Debug
if debugMat and False:
    def buildMapDebugMatrix(dim):
        matP  = lag.createMat((nbCellsLoc, nbCellsLoc), (dim, dim), dim)
        matPp = lag.createMat((nbCellsLoc, nbCellsLoc), (dim, dim), dim)

        row = np.zeros(dim * nbCellsLoc, np.int32)
        col = np.zeros(dim * nbCellsLoc, np.int32)
        val = np.zeros(dim * nbCellsLoc, np.float64)

        if dim == 1:
            for i in range(nbCellsLoc):
                matP.setValue( cellsGlobId[i], cellsCalcId[i], 1.)
                matPp.setValue(cellsCalcId[i], cellsGlobId[i], 1.)

            matP.assemblyBegin()
            matPp.assemblyBegin()
            matP.assemblyEnd()
            matPp.assemblyEnd()
        elif dim ==3:
            for i in range(nbCellsLoc):
                matP.setValue(3*cellsGlobId[i],    cellsMap[3*i],    1.)
                matP.setValue(3*cellsGlobId[i] +1, cellsMap[3*i +1], 1.)
                matP.setValue(3*cellsGlobId[i] +2, cellsMap[3*i +2], 1.)

                matPp.setValue(cellsMap[3*i],    3*cellsGlobId[i],    1.)
                matPp.setValue(cellsMap[3*i +1], 3*cellsGlobId[i] +1, 1.)
                matPp.setValue(cellsMap[3*i +2], 3*cellsGlobId[i] +2, 1.)

            matP.assemblyBegin()
            matPp.assemblyBegin()
            matP.assemblyEnd()
            matPp.assemblyEnd()
        else:
            raise ValueError('Invalid dim value')

        return matP, matPp
    matP, matPp = buildMapDebugMatrix(3)
    def buildMapDebugMatrixFaces():
        matP  = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)
        matPp = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

        row = np.zeros(3 * nbCellsLoc, np.int32)
        col = np.zeros(3 * nbCellsLoc, np.int32)
        val = np.zeros(3 * nbCellsLoc, np.float64)

        for i in range(nbInnerFaces):
            matP.setValue(3*facesGlobId[i],    facesMap[3*i],    1.)
            matP.setValue(3*facesGlobId[i] +1, facesMap[3*i +1], 1.)
            matP.setValue(3*facesGlobId[i] +2, facesMap[3*i +2], 1.)

            matPp.setValue(facesMap[3*i],    3*facesGlobId[i],    1.)
            matPp.setValue(facesMap[3*i +1], 3*facesGlobId[i] +1, 1.)
            matPp.setValue(facesMap[3*i +2], 3*facesGlobId[i] +2, 1.)

        for i in range(nbInnerFaces, nbInnerFaces + nbSharedFaces):
            if faces[i][5] == 1:
                matP.setValue(3*facesGlobId[i],    facesMap[3*i],    1.)
                matP.setValue(3*facesGlobId[i] +1, facesMap[3*i +1], 1.)
                matP.setValue(3*facesGlobId[i] +2, facesMap[3*i +2], 1.)

                matPp.setValue(facesMap[3*i],    3*facesGlobId[i],    1.)
                matPp.setValue(facesMap[3*i +1], 3*facesGlobId[i] +1, 1.)
                matPp.setValue(facesMap[3*i +2], 3*facesGlobId[i] +2, 1.)

        matP.assemblyBegin()
        matPp.assemblyBegin()
        matP.assemblyEnd()
        matPp.assemblyEnd()

        return matP, matPp
    matF, matFp = buildMapDebugMatrixFaces()
    if False:
        lag.fileViewPetscObj( matP,  'matP.out')
        lag.fileViewPetscObj(matPp, 'matPp.out')
        lag.fileViewPetscObj( matF,  'matF.out')
        lag.fileViewPetscObj(matFp, 'matFp.out')

################################################################################
#                                           Partitions                         #
################################################################################
if viewPart:
    debug = np.zeros(nbCellsLoc, np.float64)
    for i in range(nbCellsLoc):
        debug[i] = RANK
    msh.saveMeshEData('results/part', debug, cellsGlobId, 1, 0, RANK)

################################################################################
#                                           Generics on faces                  #
################################################################################
def ExtractCells(cellsTags, sz, tagsList):
    lst = []
    for i in range(sz):
        if cellsTags[i] in tagsList:
            lst.append(i)
    return np.array(lst)
def ExtractCellsChain(cellsLst, cellsTags, tagsList):
    lst = []
    for i in cellsLst:
        if cellsTags[i] in tagsList:
            lst.append(i)
    return np.array(lst)
def ExtractFaces(faces, nbOwd, nbShd, cellsTags, tagsList):
    sol = []
    lst = []
    for i in range(nbOwd):
        idL = faces[i][2]
        idR = faces[i][3]
        if idR == -1:
            continue
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    lst = []
    for i in range(nbOwd, nbOwd+nbShd):
        idL = faces[i][2]
        idR = faces[i][3]
        if idR == -1:
            continue
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    return sol
def ExtractFacesChain(facesLst, cellsTags, tagsList):
    sol = []
    lst = []
    for i in facesLst[0]:
        idL = faces[i][2]
        idR = faces[i][3]
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    lst = []
    for i in facesLst[1]:
        idL = faces[i][2]
        idR = faces[i][3]
        tmp = (cellsTags[idL], cellsTags[idR])
        if tmp in tagsList:
            lst.append(i)
    sol.append(np.array(lst))
    return sol
def ExtractFacesBd(faces, nbOwd, nbShd):
    sol = []
    lst = []
    for i in range(nbOwd):
        if faces[i][3] == -1:
            lst.append(i)
    sol.append(np.array(lst))
    lst = []
    for i in range(nbOwd, nbOwd+nbShd):
        if faces[i][3] == -1:
            lst.append(i)
    sol.append(np.array(lst))
    return sol

# Extract faces examples
if False:
    # Faces group
    FBd      = ExtractFacesBd(faces, nbInnerFaces, nbSharedFaces)
    FBdPts   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, 1001), (1001, 1002), (1001, 1003), (1001, 1004),
                             (1002, 1001), (1002, 1002), (1002, 1003), (1002, 1004),
                             (1003, 1001), (1003, 1002), (1003, 1003), (1003, 1004),
                             (1004, 1001), (1004, 1002), (1004, 1003), (1004, 1004)])

    FInIn    = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags, [(-1, -1)])

    FInBdL   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, -1), (1002, -1), (1003, -1), (1004, -1)])
    FInBdR   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(-1, 1001), (-1, 1002), (-1, 1003), (-1, 1004)])

    FInnerCircle  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100001, 100001)])
    _FOuterCircle = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100002, 100002)])
    FOuterCircle  = ExtractFacesChain(_FOuterCircle, cellsFTags, [(-1, -1)])

    FInnerOuter = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100001, 100002)])
    FOuterInner = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(100002, 100001)])

    # Vecs
    zero  = lag.createVec(nbCellsLoc, 1, 1)
    debug = lag.createVec(nbCellsLoc, 1, 1)

    ###     Part
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for i in range(nbCellsLoc):
        arrD[i] = RANK
    msh.saveMeshEData('results/part', arrD, cellsGlobId, 1, 0, RANK)


    ###     Border
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FBd[0]:
        A = faces[f][2]
        arrD[A] = 10 * RANK + 1.
    msh.saveMeshEData('results/Bd', arrD, cellsGlobId, 1, 0, RANK)

    ###     Pts on the border
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for lst in [FBdPts[0]]:
        for f in lst:
            A = faces[f][2]
            B = faces[f][3]
            arrD[A] = 10 * RANK + 1.
            arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/PtsBd', arrD, cellsGlobId, 1, 0, RANK)

    ###     InIn
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInIn[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/InIn', arrD, cellsGlobId, 1, 0, RANK)

    ###     In <-> Bd
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInBdR[0]:
        A = faces[f][2]
        arrD[A] = 10 * RANK + 1.
    for f in FInBdL[0]:
        B = faces[f][3]
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/InBd', arrD, cellsGlobId, 1, 0, RANK)

    ###    InnerCircle
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInnerCircle[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/InnerCircle', arrD, cellsGlobId, 1, 0, RANK)

    ###    OuterCircle
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FOuterCircle[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 1.
    msh.saveMeshEData('results/OuterCircle', arrD, cellsGlobId, 1, 0, RANK)

    ###     Inner <-> Outer
    lag.copy(zero, debug)
    arrD = lag.getArray(debug)
    for f in FInnerOuter[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 2.
        arrD[B] = 10 * RANK + 1.
    for f in FOuterInner[0]:
        A = faces[f][2]
        B = faces[f][3]
        arrD[A] = 10 * RANK + 1.
        arrD[B] = 10 * RANK + 2.
    msh.saveMeshEData('results/InOutOutIn', arrD, cellsGlobId, 1, 0, RANK)

################################################################################
#                                           Faces counting                     #
################################################################################
if True:
    START = time()

    ### Tags groups
    tAll = [-1, 1001, 1002, 1003, 1004]
    tIn = [-1]
    tBorder = [1001, 1002, 1003, 1004]
    cellsAll = [100001, 100002]
    cellsAir = [100002]
    cellsSup = [100001]

    ### Extract all needed faces
    CIn      = ExtractCells(cellsFTags, nbCellsLoc,      tIn)
    CBd      = ExtractCells(cellsFTags, nbCellsLoc,  tBorder)
    FSupAirL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsSup, cellsAir)])
    FSupAirR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsSup)])
    FSupSup  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsSup, cellsSup)])
    FSup = FSupSup
    _FIn     = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsAir)])
    FIn      = ExtractFacesChain(_FIn, cellsFTags, [(-1, -1)])
    _FInIn   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                            [(cellsAir, cellsAir), (cellsAir, cellsSup),
                             (cellsSup, cellsAir), (cellsSup, cellsSup)])
    FInIn    = ExtractFacesChain(_FInIn, cellsFTags, [(-1, -1)])
    FInBdL   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(-1, 1001), (-1, 1002), (-1, 1003), (-1, 1004)])
    FInBdR   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, -1), (1002, -1), (1003, -1), (1004, -1)])
    END = time()
    elapsedTime = COMM.allreduce(END - START, op=MPI.MAX)

    if False:
        sleep(RANK)
        print('----- %3.1d -----' %(RANK))
        print('Time to extract faces: %f' %(elapsedTime))
        print('Nb cells In: %d' %(len(CIn)))
        print('Nb cells Border: %d' %(len(CBd)))
        print('Nb faces In: %d' %(len(FInIn[0]) + len(FInIn[1])))
        print('Nb faces sup-air: %d' %(len(FSupAirL[0]) + len(FSupAirL[1]) + len(FSupAirR[0]) + len(FSupAirR[1])))
        print('Nb faces sup-sup: %d' %(len(FSupSup[0]) + len(FSupSup[1])))
        print('Nb faces air-air: %d' %(len(FIn[0]) + len(FIn[1])))
        print('Nb faces in-bd: %d' %(len(FInBdL[0]) + len(FInBdL[1]) + len(FInBdR[0]) + len(FInBdR[1])))
        sys.stdout.flush()

    # cellsId3: 3
    # edgesCurl: 6
    # curlCurl: 5
    # curlRhoCurl: 5
    # curlRhoCurlEps: 5

    # supLin: 9
    # supLinEps: 9
    # supLin2: 9

    # ExCells: 1
    # ExCellsEdges: 4 * nbMaxNeig
    # ExCellsEdgesLR: 2 * nbMaxNeig
    # ExDiamond: 4 + 4 * nbMaxNeig
    # ExDiamondLR: 2 + 2 * nbMaxNeig
    # ExSplitLR: 1 + 2 * nbMaxNeig
    szBuff = 10000000
    nbMaxNeig = 7
    if False: # Order
        szBuff = max([((4 + 4*nbMaxNeig) * 5) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Di-Domaine
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Lin diamond
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FIn[0]) + len(FIn[1])),
                      ((4 + 4 * nbMaxNeig) * 9) * (len(FSup[0]) + len(FSup[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),
                      ((2 + 2 * nbMaxNeig) * 9) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Smart Bi-Domaine
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FIn[0]) + len(FIn[1])),
                      ((4 + 4 * nbMaxNeig) * 5) * (len(FSupSup[0]) + len(FSupSup[1])),
                      ((1 + 2 * nbMaxNeig) * 5) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)
    if False: # Smart interfaces: Lin
        szBuff = max([(4 * nbMaxNeig * 6) * (len(FInIn[0]) + len(FInIn[1])),
                      ((2 * nbMaxNeig) * 6) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),

                      ((4 + 4 * nbMaxNeig) * 5) * (len(FIn[0]) + len(FIn[1])),
                      ((2 + 2 * nbMaxNeig) * 5) * (len(FInBdL[0]) + len(FInBdL[1]) +
                                                   len(FInBdR[0]) + len(FInBdR[1])),
                      ((1 + 2 * nbMaxNeig) * 5) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),

                      ((4 + 4 * nbMaxNeig) * 9) * (len(FSup[0]) + len(FSup[1])),
                      ((1 + 2 * nbMaxNeig) * 9) * (len(FSupAirL[0]) + len(FSupAirL[1]) +
                                                   len(FSupAirR[0]) + len(FSupAirR[1])),

                      (1 * 3) * (len(CBd)),
                      (1 * 3) * (len(CIn)),
                      3 * nbCellsLoc,
                      3 * nbNodesLoc])
        if True:
            print('szBuff', szBuff)

################################################################################
#                                           Faces Bi-Domain                    #
################################################################################
if True:
    def getMPINeig(faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLst = -np.ones(mpiSize, np.int32)
        for i in range(nbInnerFaces, nbInnerFaces+nbSharedFaces):
            v = cellsMaps[faces[i][3]]
            j = 0
            while mpiCellsNb[j] < v:
                j += 1
            neigLst[j-1] = j-1
        return neigLst
    def getFacesNb(listOfFacesList, faces):
        nbOwnd       = 0
        nbToSyncOwnd = 0
        nbToSyncBrwd = 0
        for lstF in listOfFacesList:
            nbOwnd += len(lstF[0])
            for f in lstF[1]:
                if faces[f][5] == 1:
                    nbToSyncOwnd += 1
                else:
                    nbToSyncBrwd += 1
        return nbOwnd, nbToSyncOwnd, nbToSyncBrwd
    def getFaceNeig(listOfFacesList, faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLstOwnd = [[] for i in range(mpiSize)]
        neigLstBrwd = [[] for i in range(mpiSize)]
        for lstF in listOfFacesList:
            for f in lstF[1]:
                if f < nbInnerFaces:
                    continue

                v = cellsMaps[faces[f][3]]
                j = 0
                while mpiCellsNb[j] <= v:
                    j += 1
                if faces[f][5] == 1:
                    neigLstOwnd[j-1].append(f)
                else:
                    neigLstBrwd[j-1].append(f)

        return neigLstOwnd, neigLstBrwd

    neigLst = getMPINeig(faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)
    neigLstOwnd, neigLstBrwd = getFaceNeig([FSupAirL, FSupAirR], faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)

    nbFOwnd, nbFToSyncOwnd, nbFToSyncBrwd = getFacesNb([FSupAirL, FSupAirR], faces)

    nbFacesArray = np.zeros(SIZE+1, np.int32)
    nbFacesArray[RANK+1] = nbFOwnd + nbFToSyncOwnd
    nbFacesArray = COMM.allreduce(nbFacesArray, op=MPI.SUM)
    nbFacesArray = nbFacesArray.cumsum().astype(np.int32)

    facesCalcId = 1000000000 * np.ones(nbInnerFaces + nbSharedFaces, np.int32)
    def innerFacesCalcId(listOfFacesList, facesCalcId, nbFOwnd, nbFacesArray, rank):
        vId = 0

        for lstF in listOfFacesList:
            for f in lstF[0]:
                facesCalcId[f] = nbFacesArray[rank] + vId
                vId += 1

        assert(vId == nbFOwnd)
    def owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                        nbFacesArray, rank, mpiSize):
        vId = 0
        remapFOwnd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstOwnd[i]:
                facesCalcId[f] = nbFacesArray[rank] + nbFOwnd + vId
                vId += 1
                tmp = (facesCalcId[f], cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                remapFOwnd[i].append(tmp)

        assert(vId == nbFToSyncOwnd)
        return remapFOwnd
    def brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, mpiSize):
        oldFBrwd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstBrwd[i]:
                tmp = (f, cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                oldFBrwd[i].append(tmp)

        return oldFBrwd

    innerFacesCalcId([FSupAirL, FSupAirR], facesCalcId, nbFOwnd, nbFacesArray, RANK)
    remapFOwnd = owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                                 nbFacesArray, RANK, SIZE)
    oldFBrwd = brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, SIZE)

    def syncBrwWithRecieve(faces, facesCalcId, oldFBrwd, recievedF, mpiSize):
        tupleMap = []
        assert(len(oldFBrwd) == len(recievedF))
        for descF in oldFBrwd:
            j = 0
            while descF[1] != recievedF[j][2] and descF[2] != recievedF[j][1]:
                j += 1
            facesCalcId[descF[0]] = recievedF[j][0]

    for i in range(SIZE):
        if len(remapFOwnd[i]) != 0 or len(oldFBrwd[i]) != 0:
            if i < RANK:
                if len(remapFOwnd[i]) != 0:
                    COMM.send(remapFOwnd[i], dest=i)
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
            else:
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
                if len(remapFOwnd[i]) != 0:
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                    COMM.send(remapFOwnd[i], dest=i)

    facesMap = np.zeros((nbInnerFaces + nbSharedFaces, 3), np.int32)
    facesMap[:,0] = 3*facesCalcId[:]
    facesMap[:,1] = 3*facesCalcId[:] +1
    facesMap[:,2] = 3*facesCalcId[:] +2
    facesMap = np.reshape(facesMap, 3*(nbInnerFaces + nbSharedFaces))

    nbFacesLoc = nbFOwnd + nbFToSyncOwnd
    if verbose and RANK == 0:
        print('nbFacesLoc:', nbFacesLoc)

################################################################################
#                                           DEBUG                              #
################################################################################
if False:
    coeff = np.zeros(nbCellsLoc, np.float64)

    H = lag.createVec(nbCellsLoc, 1, 1)
    for f in FSupAirL[0]:
        idL = faces[f][2]
        idR = faces[f][3]

        coeff[idL] = 1.
        coeff[idR] = 0.5

    msh.saveMeshEData('results/SupAirL', coeff, cellsGlobId, 1, 0, RANK)

################################################################################
#                                           Bi-Domain                          #
################################################################################
sys.stdout.flush()

# Bi-Domain Picard Diamond
if False:
    if RANK == 0:
        print('---------------------------------------------')
        print('-           Bi-Domain Picard Diamond        -')
        print('---------------------------------------------')
        sys.stdout.flush()
    buildFun = mb.curlRhoCurl
    diagFunV = mb.cellsId3
    diagFun1 = mb.cellsOneOnDiag
    rotFun   = mb.edgesCurl

    # Cpt Rho
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            rule = rhoDict[physCells[i]]
            if rule[0]:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                nrmJ = J0*J0 + J1*J1
                v = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))
            else:
                v = rule[1] / mu0
            arrRho[3*i]    = v
            arrRho[3*i +1] = v
            arrRho[3*i +2] = v
        rho.setArray(arrRho)
        return rho

    # Space for coeff
    szBuff = 10000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vecs
    H        = lag.createVec(nbCellsLoc, 3, 3)
    J        = lag.createVec(nbCellsLoc, 3, 3)
    diriH    = lag.createVec(nbCellsLoc, 3, 3)
    rhsH     = lag.createVec(nbCellsLoc, 3, 3)
    tmpH     = lag.createVec(nbCellsLoc, 3, 3)
    memH     = lag.createVec(nbCellsLoc, 3, 3)
    zero     = lag.createVec(nbCellsLoc, 3, 3)

    # Ghost vec
    ghostId  = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho      = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    if True:
        it   = 0
        t    = 0.
        dt   = 5e-6
        tEnd = 50*dt

        n   = 10
        Jc  = 1e1
        Ec  = 1e-7
        mu0 = 4*np.pi * 1e-4

        rhoRules = {
                cellsSup[0]: ( True,    0),
                cellsAir[0]: (False, 1e-2),
                }

        # Static test functions
        def _H0(p):
            s = np.zeros(3)
            s[2] = 1e-4/mu0
            return s
        fun = _H0
    else:
        assert(False)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder,
                          cellsCenter, [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriH, H)
    lag.setValueVec(J, [], [], [], 0)
    # zero
    lag.setValueVec(zero, [], [], [], 0)

    ### Mat
    matOP        = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    # Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFun1)
    lag.setValueMat(matNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
    lag.setValueMat(volNotBorder, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    # matRot
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
                           facesNormal, cellsVol, nodesCellsId,
                           leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
                             facesNormal, cellsVol, nodesCellsId,
                             leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff,
                    cellsMap, cellsMap,
                    size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)

    ### Loop
    while t+dt < tEnd:
        lag.copy(H, tmpH)

        ### Save mesh
        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)

        t += dt
        it += 1

        while True:
            matOP.zeroEntries()
            lag.copy(tmpH, memH)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpH), lag.getArray(zero),
                              3, 2, np.ones(nbCellsLoc))

            # Update Rho
            computeRho(rho, nbCellsLoc, J, physCells, rhoRules)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)

            ### matOP
            size = mb.ExDiamond(row, col, coeff, FIn, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExDiamond(row, col, coeff, FSup, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FSupAirL, FSupAirR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FSupAirR, FSupAirL], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
            lag.setScaleValueMat(matOP, row, col, coeff, cellsMap, cellsMap,
                                 1./dt, size, addRule=True)
            size = mb.ExCells(row, col, coeff, CBd, cellsVol, diagFun1)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            # Solve
            lag.copy(diriH, rhsH)
            rhsH.axpby(1/dt, 1, volNotBorder*H)
            lag.solveLin(ksp, matOP, tmpH, rhsH)

            matRot.mult(tmpH, J)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memH), lag.getArray(tmpH),
                              3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()
            if nrmDiff/nrmPred < 1e-3:
                if RANK == 0 and verbose:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
        lag.copy(tmpH, H)
    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
# Bi-Domain Picard Split
if False:
    if RANK == 0:
        print('---------------------------------------------')
        print('-           Bi-Domain Picard Split          -')
        print('---------------------------------------------')
        sys.stdout.flush()
    buildFun   = mb.curlRhoCurl
    buildFunEg = mb.curlRhoCurlEps
    diagFunV   = mb.cellsId3
    diagFun1   = mb.cellsOneOnDiag
    rotFun     = mb.edgesCurl

    # Cpt Rho
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            rule = rhoDict[physCells[i]]
            if rule[0]:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                nrmJ = J0*J0 + J1*J1
                v = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))
            else:
                v = rule[1] / mu0
            arrRho[3*i]    = v
            arrRho[3*i +1] = v
            arrRho[3*i +2] = v
        rho.setArray(arrRho)
        return rho

    # Space for coeff
    szBuff = 10000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vecs
    H        = lag.createVec(nbCellsLoc, 3, 3)
    J        = lag.createVec(nbCellsLoc, 3, 3)
    diriH    = lag.createVec(nbCellsLoc, 3, 3)
    rhsH     = lag.createVec(nbCellsLoc, 3, 3)
    tmpH     = lag.createVec(nbCellsLoc, 3, 3)
    memH     = lag.createVec(nbCellsLoc, 3, 3)
    zero     = lag.createVec(nbCellsLoc, 3, 3)

    F      = lag.createVec(nbFacesLoc, 3, 3)
    rhsF   = lag.createVec(nbFacesLoc, 3, 3)
    tmpF   = lag.createVec(nbFacesLoc, 3, 3)

    # Ghost vec
    ghostId  = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho      = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    if True:
        it   = 0
        t    = 0.
        dt   = 1e-5
        tEnd = 50*dt

        n   = 10
        Jc  = 1e1
        Ec  = 1e-7
        mu0 = 4*np.pi * 1e-4

        rhoRules = {
                cellsSup[0]: ( True,    0),
                cellsAir[0]: (False, 1e-2),
                }

        # Static test functions
        def _H0(p):
            s = np.zeros(3)
            s[2] = 1e-4/mu0
            return s
        fun = _H0
    else:
        assert(False)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder,
                          cellsCenter, [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriH, H)
    lag.setValueVec(J, [], [], [], 0)
    # zero
    lag.setValueVec(zero, [], [], [], 0)

    ### Mat
    # Mat
    matOP1       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matOP2       = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matOP3       = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matOP4       = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    ### Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFun1)
    lag.setValueMat(matNotBorder, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
    lag.setValueMat(volNotBorder, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)

    ### matRot
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
                           facesNormal, cellsVol, nodesCellsId,
                           leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
                             facesNormal, cellsVol, nodesCellsId,
                             leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)

    ### Loop
    while t+dt < tEnd:
        lag.copy(H, tmpH)
        lag.copy(F, tmpF)

        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)

        t += dt
        it += 1

        while True:
            matOP1.zeroEntries()
            matOP2.zeroEntries()
            matOP3.zeroEntries()
            matOP4.zeroEntries()

            lag.copy(tmpH, memH)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpH), lag.getArray(zero),
                              3, 2, np.ones(nbCellsLoc))

            # Update Rho
            computeRho(rho, nbCellsLoc, J, physCells, rhoRules)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)

            ### matOP1
            size = mb.ExDiamond(row, col, coeff, FIn, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExDiamond(row, col, coeff, FSup, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
            lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap,
                                 cellsMap, 1./dt, size, addRule=True)
            size = mb.ExCells(row, col, coeff, CBd, cellsVol, diagFun1)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            # Mat 2
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap,
                            facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap,
                            facesMap, size, addRule=True)

            # Mat 3
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap,
                            cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap,
                            cellsMap, size, addRule=True)

            # Mat 4
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FSupAirL, FSupAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFunEg, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap,
                            facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FSupAirR, FSupAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFunEg, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap,
                            facesMap, size, addRule=True)

            # Update
            lag.copy(diriH, rhsH)
            rhsH.axpby(1/dt, 1, volNotBorder*H)

            # Nest
            matOP = lag.createNest(2, [[matOP1, matOP2], [matOP3, matOP4]])
            rhsG = lag.createNest(1, [rhsH, rhsF])
            tmpG = lag.createNest(1, [tmpH, tmpF])

            lag.solveLin(ksp, matOP, tmpG, rhsG)

            matRot.mult(tmpH, J)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memH), lag.getArray(tmpH),
                              3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()
            if nrmDiff/nrmPred < 1e-3:
                if verbose and RANK == 0:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
        lag.copy(tmpH, H)
        lag.copy(tmpF, F)
    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)

################################################################################
#                                           Faces Tri-Domain                   #
################################################################################
sys.stdout.flush()

# Faces list
if True:
    tAll = [-1, 1001, 1002, 1003, 1004]
    tIn = [-1]
    tBorder = [1001, 1002, 1003, 1004]
    cellsAll = [100001, 100002, 100003]
    cellsFil = [100001]
    cellsWir = [100002]
    cellsAir = [100003]

    ### Extract all needed faces
    CIn      = ExtractCells(cellsFTags, nbCellsLoc,      tIn)
    CBd      = ExtractCells(cellsFTags, nbCellsLoc,  tBorder)

    _FIn     = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsAir)])
    FIn      = ExtractFacesChain(_FIn, cellsFTags, [(-1, -1)])
    _FInIn   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                            [(cellsAir, cellsAir), (cellsAir, cellsWir),
                             (cellsWir, cellsAir), (cellsWir, cellsWir),
                             (cellsWir, cellsFil), (cellsFil, cellsWir),
                             (cellsFil, cellsFil)])
    FInIn    = ExtractFacesChain(_FInIn, cellsFTags, [(-1, -1)])
    FInBdL   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(-1, 1001), (-1, 1002), (-1, 1003), (-1, 1004)])
    FInBdR   = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, cellsFTags,
                            [(1001, -1), (1002, -1), (1003, -1), (1004, -1)])

    FFilWirL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsFil, cellsWir)])
    FFilWirR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsWir, cellsFil)])
    FFilFil  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsFil, cellsFil)])

    FWirAirL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsWir, cellsAir)])
    FWirAirR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsWir)])
    FWirWir  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsWir, cellsWir)])

    FAirAir  = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells, [(cellsAir, cellsAir)])

# Faces numbering
if True:
    ### Debug
    FAllL = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                         [(cellsFil, cellsWir), (cellsWir, cellsAir)])
    FAllR = ExtractFaces(faces, nbInnerFaces, nbSharedFaces, physCells,
                         [(cellsWir, cellsFil), (cellsAir, cellsWir)])

    def getMPINeig(faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLst = -np.ones(mpiSize, np.int32)
        for i in range(nbInnerFaces, nbInnerFaces+nbSharedFaces):
            v = cellsMaps[faces[i][3]]
            j = 0
            while mpiCellsNb[j] < v:
                j += 1
            neigLst[j-1] = j-1
        return neigLst
    def getFacesNb(listOfFacesList, faces):
        nbOwnd       = 0
        nbToSyncOwnd = 0
        nbToSyncBrwd = 0
        for lstF in listOfFacesList:
            nbOwnd += len(lstF[0])
            for f in lstF[1]:
                if faces[f][5] == 1:
                    nbToSyncOwnd += 1
                else:
                    nbToSyncBrwd += 1
        return nbOwnd, nbToSyncOwnd, nbToSyncBrwd
    def getFaceNeig(listOfFacesList, faces, nbInnerFaces, nbSharedFaces, mpiSize, mpiCellsNb, cellsMaps):
        neigLstOwnd = [[] for i in range(mpiSize)]
        neigLstBrwd = [[] for i in range(mpiSize)]
        for lstF in listOfFacesList:
            for f in lstF[1]:
                if f < nbInnerFaces:
                    continue

                v = cellsMaps[faces[f][3]]
                j = 0
                while mpiCellsNb[j] <= v:
                    j += 1
                if faces[f][5] == 1:
                    neigLstOwnd[j-1].append(f)
                else:
                    neigLstBrwd[j-1].append(f)

        return neigLstOwnd, neigLstBrwd

    neigLst = getMPINeig(faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)
    neigLstOwnd, neigLstBrwd = getFaceNeig([FAllL, FAllR], faces, nbInnerFaces, nbSharedFaces, SIZE, allCellsSize, cellsCalcId)

    nbFOwnd, nbFToSyncOwnd, nbFToSyncBrwd = getFacesNb([FAllL, FAllR], faces)

    nbFacesArray = np.zeros(SIZE+1, np.int32)
    nbFacesArray[RANK+1] = nbFOwnd + nbFToSyncOwnd
    nbFacesArray = COMM.allreduce(nbFacesArray, op=MPI.SUM)
    nbFacesArray = nbFacesArray.cumsum().astype(np.int32)

    facesCalcId = 1000000000 * np.ones(nbInnerFaces + nbSharedFaces, np.int32)
    def innerFacesCalcId(listOfFacesList, facesCalcId, nbFOwnd, nbFacesArray, rank):
        vId = 0

        for lstF in listOfFacesList:
            for f in lstF[0]:
                facesCalcId[f] = nbFacesArray[rank] + vId
                vId += 1

        assert(vId == nbFOwnd)
    def owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                        nbFacesArray, rank, mpiSize):
        vId = 0
        remapFOwnd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstOwnd[i]:
                facesCalcId[f] = nbFacesArray[rank] + nbFOwnd + vId
                vId += 1
                tmp = (facesCalcId[f], cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                remapFOwnd[i].append(tmp)

        assert(vId == nbFToSyncOwnd)
        return remapFOwnd
    def brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, mpiSize):
        oldFBrwd = [[] for i in range(mpiSize)]

        for i in range(mpiSize):
            for f in neigLstBrwd[i]:
                tmp = (f, cellsGlobId[faces[f][2]], cellsGlobId[faces[f][3]])
                oldFBrwd[i].append(tmp)

        return oldFBrwd

    innerFacesCalcId([FAllL, FAllR], facesCalcId, nbFOwnd, nbFacesArray, RANK)
    remapFOwnd = owndFacesCalcId(neigLstOwnd, faces, facesCalcId, cellsGlobId, nbFOwnd, nbFToSyncOwnd,
                                 nbFacesArray, RANK, SIZE)
    oldFBrwd = brwdFacesCalcId(neigLstBrwd, faces, facesCalcId, cellsGlobId, SIZE)

    def syncBrwWithRecieve(faces, facesCalcId, oldFBrwd, recievedF, mpiSize):
        tupleMap = []
        assert(len(oldFBrwd) == len(recievedF))
        for descF in oldFBrwd:
            j = 0
            while descF[1] != recievedF[j][2] and descF[2] != recievedF[j][1]:
                j += 1
            facesCalcId[descF[0]] = recievedF[j][0]

    for i in range(SIZE):
        if len(remapFOwnd[i]) != 0 or len(oldFBrwd[i]) != 0:
            if i < RANK:
                if len(remapFOwnd[i]) != 0:
                    COMM.send(remapFOwnd[i], dest=i)
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
            else:
                if len(oldFBrwd[i]) != 0:
                    val = COMM.recv(source=i)
                    # print('Recieve from ', i, 'to', RANK, ':', val)
                    syncBrwWithRecieve(faces, facesCalcId, oldFBrwd[i], val, SIZE)
                if len(remapFOwnd[i]) != 0:
                    # print('Send from ', RANK, 'to', i, ':', remapFOwnd[i])
                    COMM.send(remapFOwnd[i], dest=i)

    facesMap = np.zeros((nbInnerFaces + nbSharedFaces, 3), np.int32)
    facesMap[:,0] = 3*facesCalcId[:]
    facesMap[:,1] = 3*facesCalcId[:] +1
    facesMap[:,2] = 3*facesCalcId[:] +2
    facesMap = np.reshape(facesMap, 3*(nbInnerFaces + nbSharedFaces))

    nbFacesLoc = nbFOwnd + nbFToSyncOwnd
    if verbose and RANK == 0:
        print('nbFacesLoc:', nbFacesLoc)

################################################################################
#                                           Tri-Domain                         #
################################################################################
sys.stdout.flush()

# Tri-Domain Picard Diamond
if False:
    if RANK == 0:
        print('---------------------------------------------')
        print('-           Tri-Domain Picard Diamond       -')
        print('---------------------------------------------')
        sys.stdout.flush()
    buildFun = mb.curlRhoCurl
    diagFunV = mb.cellsId3
    diagFun1 = mb.cellsOneOnDiag
    rotFun   = mb.edgesCurl

    # Cpt Rho
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            rule = rhoDict[physCells[i]]
            if rule[0]:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                nrmJ = J0*J0 + J1*J1
                v = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))
            else:
                v = rule[1] / mu0
            arrRho[3*i]    = v
            arrRho[3*i +1] = v
            arrRho[3*i +2] = v
        rho.setArray(arrRho)
        return rho

    # Space for coeff
    szBuff = 10000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vec
    H        = lag.createVec(nbCellsLoc, 3, 3)
    J        = lag.createVec(nbCellsLoc, 3, 3)
    rhsH     = lag.createVec(nbCellsLoc, 3, 3)
    tmpH     = lag.createVec(nbCellsLoc, 3, 3)
    memH     = lag.createVec(nbCellsLoc, 3, 3)
    diriH    = lag.createVec(nbCellsLoc, 3, 3)
    zero     = lag.createVec(nbCellsLoc, 3, 3)

    # Ghost vec
    ghostId  = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho      = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    if True:
        it   = 0
        t    = 0.
        dt   = 1e-5
        tEnd = 100*dt

        n   = 10
        Jc  = 1e1
        Ec  = 1e-7
        mu0 = 4*np.pi * 1e-4

        rhoRules = {
                cellsFil[0]: ( True,    0),
                cellsWir[0]: (False, 1e-2),
                cellsAir[0]: (False, 1e0),
                }

        def funH0(p):
            s = np.zeros(3)
            s[2] = 1e-4/mu0
            return s
        fun = funH0
    else:
        assert(False)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder,
                          cellsCenter, [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriH, H)
    lag.setValueVec(J, [], [], [], 0)
    # zero
    lag.setValueVec(zero, [], [], [], 0)

    ### Mat
    matOP        = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    ### Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFun1)
    lag.setValueMat(matNotBorder, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
    lag.setValueMat(volNotBorder, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)
    ### matRot
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
                           facesNormal, cellsVol, nodesCellsId,
                           leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
                             facesNormal, cellsVol, nodesCellsId,
                             leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)

    ### Loop
    while t+dt < tEnd:
        lag.copy(H, tmpH)

        ### Save mesh
        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)

        t += dt
        it += 1

        while True:
            matOP.zeroEntries()
            lag.copy(tmpH, memH)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpH), lag.getArray(zero),
                              3, 2, np.ones(nbCellsLoc))

            # Update Rho
            computeRho(rho, nbCellsLoc, J, physCells, rhoRules)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)

            ### Update matOP
            size = mb.ExDiamond(row, col, coeff, FIn, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamond(row, col, coeff, FWirWir, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamond(row, col, coeff, FFilFil, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExDiamondLR(row, col, coeff, [FWirAirL, FWirAirR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FWirAirR, FWirAirL], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)


            size = mb.ExDiamondLR(row, col, coeff, [FFilWirL, FFilWirR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamondLR(row, col, coeff, [FFilWirR, FFilWirL], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
            lag.setScaleValueMat(matOP, row, col, coeff, cellsMap,
                                 cellsMap, 1./dt, size, addRule=True)
            size = mb.ExCells(row, col, coeff, CBd, cellsVol, diagFun1)
            lag.setValueMat(matOP, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            # # Update pb
            lag.copy(diriH, rhsH)
            rhsH.axpby(1/dt, 1, volNotBorder*H)
            lag.solveLin(ksp, matOP, tmpH, rhsH)

            matRot.mult(tmpH, J)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memH), lag.getArray(tmpH),
                              3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()

            if nrmDiff/nrmPred < 1e-3:
                if RANK == 0 and verbose:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
        lag.copy(tmpH, H)
    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
# Lin Split
if False:
    if RANK == 0:
        print('---------------------------------------------')
        print('-           Tri-Domain Picard Split         -')
        print('---------------------------------------------')
        sys.stdout.flush()
    buildFun   = mb.curlRhoCurl
    buildFunEg = mb.curlRhoCurlEps
    diagFunV   = mb.cellsId3
    diagFun1   = mb.cellsOneOnDiag
    rotFun     = mb.edgesCurl

    # Cpt Rho
    def computeRho(rho, nbCellsLoc, JLoc, physCells, rhoDict):
        sz = rho.getSizes()[0]
        arr = JLoc.getArray()
        arrRho = np.zeros(sz, np.float64)
        for i in range(nbCellsLoc):
            rule = rhoDict[physCells[i]]
            if rule[0]:
                J0 = arr[3*i] / Jc
                J1 = arr[3*i +1] / Jc
                nrmJ = J0*J0 + J1*J1
                v = (Ec / (Jc * mu0)) * np.power(nrmJ, (n-1)/2) + (Ec * 1e-3 / (mu0 * Jc))
            else:
                v = rule[1] / mu0
            arrRho[3*i]    = v
            arrRho[3*i +1] = v
            arrRho[3*i +2] = v
        rho.setArray(arrRho)
        return rho

    # Space for coeff
    szBuff = 10000000
    row   = np.zeros(szBuff,   np.int32)
    col   = np.zeros(szBuff,   np.int32)
    coeff = np.zeros(szBuff, np.float64)

    ### Vec
    # Vec
    H        = lag.createVec(nbCellsLoc, 3, 3)
    J        = lag.createVec(nbCellsLoc, 3, 3)
    rhsH     = lag.createVec(nbCellsLoc, 3, 3)
    tmpH     = lag.createVec(nbCellsLoc, 3, 3)
    memH     = lag.createVec(nbCellsLoc, 3, 3)
    diriH    = lag.createVec(nbCellsLoc, 3, 3)
    zero     = lag.createVec(nbCellsLoc, 3, 3)

    F      = lag.createVec(nbFacesLoc, 3, 3)
    rhsF   = lag.createVec(nbFacesLoc, 3, 3)
    tmpF   = lag.createVec(nbFacesLoc, 3, 3)

    # Ghost vec
    ghostId  = ut.getForeignId(cellsCalcId, nbCellsLoc, 3)
    rho      = lag.createGhostVec(nbCellsLoc, 3, 3, ghostId)

    if True:
        it   = 0
        t    = 0.
        dt   = 1e-5
        tEnd = 100*dt

        n   = 10
        Jc  = 1e1
        Ec  = 1e-7
        mu0 = 4*np.pi * 1e-4

        rhoRules = {
                cellsFil[0]: ( True,    0),
                cellsWir[0]: (False, 1e-2),
                cellsAir[0]: (False, 1e0),
                }

        def funH0(p):
            s = np.zeros(3)
            s[2] = 1e-4/mu0
            return s
        fun = funH0
    else:
        assert(False)

    ### All init
    # Init border
    sz = ut.cptVecTagsVal(coeff, row, fun, cellsFTags, tBorder,
                          cellsCenter, [1.], nbCellsLoc, 3)
    lag.setValueVec(diriH, coeff, row, cellsMap, sz)
    # Init H and J
    lag.copy(diriH, H)
    lag.setValueVec(J, [], [], [], 0)
    # zero
    lag.setValueVec(zero, [], [], [], 0)

    ### Mat
    # Mat
    matOP1       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matOP2       = lag.createMat((nbCellsLoc, nbFacesLoc), (3, 3), 3)
    matOP3       = lag.createMat((nbFacesLoc, nbCellsLoc), (3, 3), 3)
    matOP4       = lag.createMat((nbFacesLoc, nbFacesLoc), (3, 3), 3)

    matRot       = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    matNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)
    volNotBorder = lag.createMat((nbCellsLoc, nbCellsLoc), (3, 3), 3)

    ### Border
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFun1)
    lag.setValueMat(matNotBorder, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)
    size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
    lag.setValueMat(volNotBorder, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)

    ### matRot
    size = mb.ExCellsEdges(row, col, coeff, FInIn, faces,
                           facesNormal, cellsVol, nodesCellsId,
                           leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)
    size = mb.ExCellsEdgesLR(row, col, coeff, [FInBdL, FInBdR], faces,
                           facesNormal, cellsVol, nodesCellsId,
                           leastSquareCoeff, rotFun)
    lag.setValueMat(matRot, row, col, coeff, cellsMap,
                    cellsMap, size, addRule=True)

    ### Linear solver
    maxIt = 100
    ksp = lag.getLinearSolver('preonly', 'lu', 1e-8, 1e-8, maxIt)

    ### Loop
    while t+dt < tEnd:
        lag.copy(H, tmpH)
        lag.copy(F, tmpF)

        if saveMesh:
            msh.saveMeshEData('results/H', lag.getArray(H),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/J', lag.getArray(J),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)
            msh.saveMeshEData('results/rho', lag.getArray(rho),
                              cellsGlobId[:nbCellsLoc], 3, it, RANK)

        t += dt
        it += 1

        while True:
            matOP1.zeroEntries()
            matOP2.zeroEntries()
            matOP3.zeroEntries()
            matOP4.zeroEntries()

            lag.copy(tmpH, memH)

            # Diff
            nrmPred = ut.diff(COMM, lag.getArray(tmpH), lag.getArray(zero),
                              3, 2, np.ones(nbCellsLoc))

            # Update Rho
            computeRho(rho, nbCellsLoc, J, physCells, rhoRules)
            lag.syncGhostVec(rho)
            arrayRho = lag.getGhostArray(rho)

            ### Update matOP
            size = mb.ExDiamond(row, col, coeff, FIn, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamond(row, col, coeff, FWirWir, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExDiamond(row, col, coeff, FFilFil, faces,
                                facesNormal, diamNormal, diamVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FWirAirL, FWirAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FWirAirR, FWirAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 1,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExDiamondLR(row, col, coeff, [FInBdL, FInBdR], faces,
                                  facesNormal, diamNormal, diamVol, cellsVol,
                                  nodesCellsId, leastSquareCoeff,
                                  buildFun, arrayRho)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            size = mb.ExCells(row, col, coeff, CIn, cellsVol, diagFunV)
            lag.setScaleValueMat(matOP1, row, col, coeff, cellsMap,
                                 cellsMap, 1./dt, size, addRule=True)
            size = mb.ExCells(row, col, coeff, CBd, cellsVol, diagFun1)
            lag.setValueMat(matOP1, row, col, coeff, cellsMap,
                            cellsMap, size, addRule=True)

            # Mat 2
            size = mb.ExSplitLR(row, col, coeff, 2.,
                                [FWirAirL, FWirAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap,
                            facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FWirAirR, FWirAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap,
                            facesMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap,
                            facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 2,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP2, row, col, coeff, cellsMap,
                            facesMap, size, addRule=True)

            # Mat 3
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FWirAirL, FWirAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap,
                            cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FWirAirR, FWirAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap,
                            cellsMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap,
                            cellsMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 3,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFun, arrayRho)
            lag.setValueMat(matOP3, row, col, coeff, facesMap,
                            cellsMap, size, addRule=True)

            # Mat 4
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FWirAirL, FWirAirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFunEg, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap,
                            facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FWirAirR, FWirAirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFunEg, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap,
                            facesMap, size, addRule=True)

            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FFilWirL, FFilWirR], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFunEg, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap,
                            facesMap, size, addRule=True)
            size = mb.ExSplitLR(row, col, coeff, 4,
                                [FFilWirR, FFilWirL], faces,
                                facesNormal, diamNormal, splitDNormal,
                                diamVol, splitDVol, cellsVol,
                                nodesCellsId, leastSquareCoeff,
                                buildFunEg, arrayRho)
            lag.setValueMat(matOP4, row, col, coeff, facesMap,
                            facesMap, size, addRule=True)


            # Update
            lag.copy(diriH, rhsH)
            rhsH.axpby(1/dt, 1, volNotBorder*H)

            # Nest
            matOP = lag.createNest(2, [[matOP1, matOP2], [matOP3, matOP4]])
            rhsG = lag.createNest(1, [rhsH, rhsF])
            tmpG = lag.createNest(1, [tmpH, tmpF])

            lag.solveLin(ksp, matOP, tmpG, rhsG)

            matRot.mult(tmpH, J)

            # Diff
            nrmDiff = ut.diff(COMM, lag.getArray(memH), lag.getArray(tmpH),
                              3, 2, np.ones(nbCellsLoc))

            nbIt = ksp.getIterationNumber()
            if nbIt == maxIt or nbIt == 0:
                print('Error while solve, nbIt: ', nbIt)
                exit()
            if verbose and RANK == 0:
                print('Nb iteration: ', nbIt)
                print('Nrm diff: %f (%f %f)' %(nrmDiff/nrmPred, nrmDiff, nrmPred))
                sys.stdout.flush()
            if nrmDiff/nrmPred < 1e-3:
                if verbose and RANK == 0:
                    print('---------------------------------------------')
                    sys.stdout.flush()
                break
        lag.copy(tmpH, H)
        lag.copy(tmpF, F)

    ### Save mesh
    if saveMesh:
        msh.saveMeshEData('results/H', lag.getArray(H),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/J', lag.getArray(J),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
        msh.saveMeshEData('results/rho', lag.getArray(rho),
                          cellsGlobId[:nbCellsLoc], 3, it, RANK)
