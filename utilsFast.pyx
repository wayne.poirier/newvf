import cython
from libc.stdint cimport int32_t

@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing.
def coeffIJtoCSR(int32_t[:] posI, int32_t[:] posJ, double[:] coeff,
                 int32_t[:] rowCsr, int32_t[:] colCsr, double[:] valCsr,
                 int32_t[:] rowMap, int32_t[:] colMap,
                 int32_t sz, int32_t rowOffset):
    # Add coeff of an AIJ matrix coefficient to and CSR one.
    # DO NOT replace the value, add them.
    cdef int32_t pI, pJ, i, j, offset
    for i in range(sz):
        pI = rowMap[posI[i]] - rowOffset
        pJ = colMap[posJ[i]]

        j = 0
        offset = rowCsr[pI]
        while colCsr[offset+j] != pJ:
            j += 1
        valCsr[offset+j] += coeff[i]

@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing.
def coeffIJtoCSRScaled(int32_t[:] posI, int32_t[:] posJ, double[:] coeff,
                       double scale,
                       int32_t[:] rowCsr, int32_t[:] colCsr, double[:] valCsr,
                       int32_t[:] rowMap, int32_t[:] colMap,
                       int32_t sz, int32_t rowOffset):
    # Add coeff of an AIJ matrix coefficient to and CSR one.
    # DO NOT replace the value, add them.
    cdef int32_t pI, pJ, i, j, offset
    for i in range(sz):
        pI = rowMap[posI[i]] - rowOffset
        pJ = colMap[posJ[i]]

        j = 0
        offset = rowCsr[pI]
        while colCsr[offset+j] != pJ:
            j += 1
        valCsr[offset+j] += scale*coeff[i]

@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing.
def coeffIJtoCSRWeighted(int32_t[:] posI, int32_t[:] posJ, double[:] coeff,
                         double[:] weight, double scale,
                         int32_t[:] rowCsr, int32_t[:] colCsr, double[:] valCsr,
                         int32_t[:] rowMap, int32_t[:] colMap,
                         int32_t sz, int32_t rowOffset):
    # Add coeff of an AIJ matrix coefficient to and CSR one.
    # DO NOT replace the value, add them.
    cdef int32_t pI, pJ, i, j, offset
    for i in range(sz):
        pI = rowMap[posI[i]] - rowOffset
        pJ = colMap[posJ[i]]

        j = 0
        offset = rowCsr[pI]
        while colCsr[offset+j] != pJ:
            j += 1
        valCsr[offset+j] += scale * weight[posI[i]] * coeff[i]

@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing.
def scaleUpCSRRow(int32_t[:] rowB, int32_t[:] rowS,
                  int32_t dimRow, int32_t[:] rowScale):
    cdef int32_t i, j

    for i in range(len(rowB)-1):
        for j in range(dimRow):
            rowS[dimRow*i +j +1] = rowS[dimRow*i +j]\
                    + rowScale[j] * (rowB[i+1] - rowB[i])

@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing.
def scaleUpCSRCol(int32_t[:] rowB, int32_t[:] colB,
                  int32_t[:] rowS, int32_t[:] colS,
                  int32_t[:,:] colScale,
                  int32_t dimRow, int32_t dimCol):
    cdef int32_t count, begin, end, nbColScale
    cdef int32_t i, j, l, k

    count = 0
    for i in range(len(rowB)-1):
        begin = rowB[i]
        end = rowB[i +1]
        for j in range(dimRow):
            nbColScale = colScale[j][0]
            for l in range(begin, end):
                for k in range(1, nbColScale):
                    colS[count] = dimCol*colB[l] +colScale[j][k]
                    count += 1
